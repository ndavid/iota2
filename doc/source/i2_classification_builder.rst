i2_classification
#################

Landsat5_old
============

+---------------------+----------------+--------------------------------------+----------------------------------------------------+
| Parameter           | Type           | Default value                        | Description                                        |
+=====================+================+======================================+====================================================+
| additional_features | str            |                                      | OTB's bandmath expressions, separated by comma     |
+---------------------+----------------+--------------------------------------+----------------------------------------------------+
| end_date            | str            |                                      | The end date of interpolated image time series     |
+---------------------+----------------+--------------------------------------+----------------------------------------------------+
| keep_bands          | list           | ['B1', 'B2', 'B3', 'B4', 'B5', 'B6'] | The list of spectral bands used for classification |
+---------------------+----------------+--------------------------------------+----------------------------------------------------+
| start_date          | str            |                                      | The first date of interpolated image time series   |
+---------------------+----------------+--------------------------------------+----------------------------------------------------+
| temporal_resolution | int            | 16                                   | The temporal gap between two interpolation         |
+---------------------+----------------+--------------------------------------+----------------------------------------------------+


Landsat8
========

+---------------------------------------------+----------------+--------------------------------------------+---------------------------------------------------------+
| Parameter                                   | Type           | Default value                              | Description                                             |
+=============================================+================+============================================+=========================================================+
| additional_features                         | str            |                                            | OTB's bandmath expressions, separated by comma          |
+---------------------------------------------+----------------+--------------------------------------------+---------------------------------------------------------+
| end_date                                    | str            |                                            | The end date of interpolated image time series          |
+---------------------------------------------+----------------+--------------------------------------------+---------------------------------------------------------+
| keep_bands                                  | list           | ['B1', 'B2', 'B3', 'B4', 'B5', 'B6', 'B7'] | The list of spectral bands used for classification      |
+---------------------------------------------+----------------+--------------------------------------------+---------------------------------------------------------+
| start_date                                  | str            |                                            | The first date of interpolated image time series        |
+---------------------------------------------+----------------+--------------------------------------------+---------------------------------------------------------+
| temporal_resolution                         | int            | 16                                         | The temporal gap between two interpolation              |
+---------------------------------------------+----------------+--------------------------------------------+---------------------------------------------------------+
| write_reproject_resampled_input_dates_stack | bool           | True                                       | Enable the write of resampled stack image for each date |
+---------------------------------------------+----------------+--------------------------------------------+---------------------------------------------------------+


Landsat8_old
============

+---------------------------------------------+----------------+--------------------------------------+---------------------------------------------------------+
| Parameter                                   | Type           | Default value                        | Description                                             |
+=============================================+================+======================================+=========================================================+
| additional_features                         | str            |                                      | OTB's bandmath expressions, separated by comma          |
+---------------------------------------------+----------------+--------------------------------------+---------------------------------------------------------+
| end_date                                    | str            |                                      | The end date of interpolated image time series          |
+---------------------------------------------+----------------+--------------------------------------+---------------------------------------------------------+
| keep_bands                                  | list           | ['B1', 'B2', 'B3', 'B4', 'B5', 'B6'] | The list of spectral bands used for classification      |
+---------------------------------------------+----------------+--------------------------------------+---------------------------------------------------------+
| start_date                                  | str            |                                      | The first date of interpolated image time series        |
+---------------------------------------------+----------------+--------------------------------------+---------------------------------------------------------+
| temporal_resolution                         | int            | 16                                   | The temporal gap between two interpolation              |
+---------------------------------------------+----------------+--------------------------------------+---------------------------------------------------------+
| write_reproject_resampled_input_dates_stack | bool           | True                                 | Enable the write of resampled stack image for each date |
+---------------------------------------------+----------------+--------------------------------------+---------------------------------------------------------+


Sentinel_2
==========

+---------------------------------------------+----------------+-----------------------------------------------------------------+---------------------------------------------------------+
| Parameter                                   | Type           | Default value                                                   | Description                                             |
+=============================================+================+=================================================================+=========================================================+
| additional_features                         | str            |                                                                 | OTB's bandmath expressions, separated by comma          |
+---------------------------------------------+----------------+-----------------------------------------------------------------+---------------------------------------------------------+
| end_date                                    | str            |                                                                 | The end date of interpolated image time series          |
+---------------------------------------------+----------------+-----------------------------------------------------------------+---------------------------------------------------------+
| keep_bands                                  | list           | ['B2', 'B3', 'B4', 'B5', 'B6', 'B7', 'B8', 'B8A', 'B11', 'B12'] | The list of spectral bands used for classification      |
+---------------------------------------------+----------------+-----------------------------------------------------------------+---------------------------------------------------------+
| start_date                                  | str            |                                                                 | The first date of interpolated image time series        |
+---------------------------------------------+----------------+-----------------------------------------------------------------+---------------------------------------------------------+
| temporal_resolution                         | int            | 10                                                              | The temporal gap between two interpolation              |
+---------------------------------------------+----------------+-----------------------------------------------------------------+---------------------------------------------------------+
| write_reproject_resampled_input_dates_stack | bool           | True                                                            | Enable the write of resampled stack image for each date |
+---------------------------------------------+----------------+-----------------------------------------------------------------+---------------------------------------------------------+


Sentinel_2_L3A
==============

+---------------------------------------------+----------------+-----------------------------------------------------------------+---------------------------------------------------------+
| Parameter                                   | Type           | Default value                                                   | Description                                             |
+=============================================+================+=================================================================+=========================================================+
| additional_features                         | str            |                                                                 | OTB's bandmath expressions, separated by comma          |
+---------------------------------------------+----------------+-----------------------------------------------------------------+---------------------------------------------------------+
| end_date                                    | str            |                                                                 | The end date of interpolated image time series          |
+---------------------------------------------+----------------+-----------------------------------------------------------------+---------------------------------------------------------+
| keep_bands                                  | list           | ['B2', 'B3', 'B4', 'B5', 'B6', 'B7', 'B8', 'B8A', 'B11', 'B12'] | The list of spectral bands used for classification      |
+---------------------------------------------+----------------+-----------------------------------------------------------------+---------------------------------------------------------+
| start_date                                  | str            |                                                                 | The first date of interpolated image time series        |
+---------------------------------------------+----------------+-----------------------------------------------------------------+---------------------------------------------------------+
| temporal_resolution                         | int            | 16                                                              | The temporal gap between two interpolation              |
+---------------------------------------------+----------------+-----------------------------------------------------------------+---------------------------------------------------------+
| write_reproject_resampled_input_dates_stack | bool           | True                                                            | Enable the write of resampled stack image for each date |
+---------------------------------------------+----------------+-----------------------------------------------------------------+---------------------------------------------------------+


Sentinel_2_S2C
==============

+---------------------------------------------+----------------+------------------------------------------------------------------------+---------------------------------------------------------+
| Parameter                                   | Type           | Default value                                                          | Description                                             |
+=============================================+================+========================================================================+=========================================================+
| additional_features                         | str            |                                                                        | OTB's bandmath expressions, separated by comma          |
+---------------------------------------------+----------------+------------------------------------------------------------------------+---------------------------------------------------------+
| end_date                                    | str            |                                                                        | The end date of interpolated image time series          |
+---------------------------------------------+----------------+------------------------------------------------------------------------+---------------------------------------------------------+
| keep_bands                                  | list           | ['B02', 'B03', 'B04', 'B05', 'B06', 'B07', 'B08', 'B8A', 'B11', 'B12'] | The list of spectral bands used for classification      |
+---------------------------------------------+----------------+------------------------------------------------------------------------+---------------------------------------------------------+
| start_date                                  | str            |                                                                        | The first date of interpolated image time series        |
+---------------------------------------------+----------------+------------------------------------------------------------------------+---------------------------------------------------------+
| temporal_resolution                         | int            | 10                                                                     | The temporal gap between two interpolation              |
+---------------------------------------------+----------------+------------------------------------------------------------------------+---------------------------------------------------------+
| write_reproject_resampled_input_dates_stack | bool           | True                                                                   | Enable the write of resampled stack image for each date |
+---------------------------------------------+----------------+------------------------------------------------------------------------+---------------------------------------------------------+


arg_classification
==================

+-----------------------------------------------+-----------------+----------------------------------------+------------------------------------------------------------------------------------+
| Parameter                                     | Type            | Default value                          | Description                                                                        |
+===============================================+=================+========================================+====================================================================================+
| classif_mode                                  | str             | separate                               |                                                                                    |
+-----------------------------------------------+-----------------+----------------------------------------+------------------------------------------------------------------------------------+
| dempstershafer_mob                            | str             | precision                              | Choose the dempster shafer mass of belief estimation method                        |
+-----------------------------------------------+-----------------+----------------------------------------+------------------------------------------------------------------------------------+
| enable_probability_map                        | bool            | False                                  | Produce the probability map                                                        |
+-----------------------------------------------+-----------------+----------------------------------------+------------------------------------------------------------------------------------+
| fusion_options                                | str             |  -nodatalabel 0 -method majorityvoting | OTB FusionOfClassification options for voting method                               |
+-----------------------------------------------+-----------------+----------------------------------------+------------------------------------------------------------------------------------+
| fusionofclassification_all_samples_validation | bool            | False                                  | Enable the use of all reference data                                               |
+-----------------------------------------------+-----------------+----------------------------------------+------------------------------------------------------------------------------------+
| keep_runs_results                             | bool            | True                                   |                                                                                    |
+-----------------------------------------------+-----------------+----------------------------------------+------------------------------------------------------------------------------------+
| merge_final_classifications                   | bool            | False                                  | Enable the fusion of classifications mode, merging all run in a unique result.     |
+-----------------------------------------------+-----------------+----------------------------------------+------------------------------------------------------------------------------------+
| merge_final_classifications_method            | str             | majorityvoting                         | Indicate the fusion of classification method: 'majorityvoting' or 'dempstershafer' |
+-----------------------------------------------+-----------------+----------------------------------------+------------------------------------------------------------------------------------+
| merge_final_classifications_ratio             | float           | 0.1                                    | Percentage of samples to use in order to evaluate the fusion raster                |
+-----------------------------------------------+-----------------+----------------------------------------+------------------------------------------------------------------------------------+
| merge_final_classifications_undecidedlabel    | int             | 255                                    | Indicate the label for undecision case during fusion                               |
+-----------------------------------------------+-----------------+----------------------------------------+------------------------------------------------------------------------------------+
| no_label_management                           | str             | maxConfidence                          | Method for choosing a label in case of fusion                                      |
+-----------------------------------------------+-----------------+----------------------------------------+------------------------------------------------------------------------------------+


Notes
-----

dempstershafer_mob
^^^^^^^^^^^^^^^^^^

Two kind of indexes can be used:
* Global: `accuracy` or `kappa`
* Per class: `precision` or `recall`



enable_probability_map
^^^^^^^^^^^^^^^^^^^^^^

A probability map is a image with N bands , where N is the number of classes in the nomenclature file. The bands are sorted in ascending order 


fusionofclassification_all_samples_validation
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

If the fusion mode is enabled, enable the use of all reference data samples for validation


keep_runs_results
^^^^^^^^^^^^^^^^^

If in fusion mode, two final reports can be provided. One for each seed, and one for the classification fusion


arg_train
=========

+----------------------------------+-----------------+----------------------------------------------+---------------------------------------------------------------------+
| Parameter                        | Type            | Default value                                | Description                                                         |
+==================================+=================+==============================================+=====================================================================+
| a_crop_label_replacement         | list            | ['10', 'annual_crop']                        | Replace a label by a string                                         |
+----------------------------------+-----------------+----------------------------------------------+---------------------------------------------------------------------+
| annual_classes_extraction_source | str             | None                                         |                                                                     |
+----------------------------------+-----------------+----------------------------------------------+---------------------------------------------------------------------+
| annual_crop                      | list            | ['11', '12']                                 | The list of classes to be replaced by previous data                 |
+----------------------------------+-----------------+----------------------------------------------+---------------------------------------------------------------------+
| autocontext_iterations           | int             | 3                                            | Number of iterations in auto-context.                               |
+----------------------------------+-----------------+----------------------------------------------+---------------------------------------------------------------------+
| classifier                       | str             | rf                                           | Choose the classification algorithm                                 |
+----------------------------------+-----------------+----------------------------------------------+---------------------------------------------------------------------+
| crop_mix                         | bool            | False                                        | Enable crop mix option                                              |
+----------------------------------+-----------------+----------------------------------------------+---------------------------------------------------------------------+
| dempster_shafer_sar_opt_fusion   | bool            | False                                        | Enable the use of both SAR and optical data to train a model.       |
+----------------------------------+-----------------+----------------------------------------------+---------------------------------------------------------------------+
| enable_autocontext               | bool            | False                                        | Enable the auto-context processing                                  |
+----------------------------------+-----------------+----------------------------------------------+---------------------------------------------------------------------+
| features                         | list            | ['NDVI', 'NDWI', 'Brightness']               | List of additional features computed                                |
+----------------------------------+-----------------+----------------------------------------------+---------------------------------------------------------------------+
| force_standard_labels            | bool            | False                                        | Standardize labels for feature extraction                           |
+----------------------------------+-----------------+----------------------------------------------+---------------------------------------------------------------------+
| mode_outside_regionsplit         | float           | 0.1                                          | Fix the threshold for split huge model                              |
+----------------------------------+-----------------+----------------------------------------------+---------------------------------------------------------------------+
| options                          | str             |  -classifier.rf.min 5 -classifier.rf.max 25  | OTB option for classifier                                           |
+----------------------------------+-----------------+----------------------------------------------+---------------------------------------------------------------------+
| output_prev_features             | str             | None                                         | Path to previous features for crop mix                              |
+----------------------------------+-----------------+----------------------------------------------+---------------------------------------------------------------------+
| prev_features                    | str             | None                                         | Path to a configuration file used to produce previous features      |
+----------------------------------+-----------------+----------------------------------------------+---------------------------------------------------------------------+
| random_seed                      | int             | None                                         | Fix the random seed for random split of reference data              |
+----------------------------------+-----------------+----------------------------------------------+---------------------------------------------------------------------+
| ratio                            | float           | 0.5                                          | Ratio between training and validation sets                          |
+----------------------------------+-----------------+----------------------------------------------+---------------------------------------------------------------------+
| runs                             | int             | 1                                            | Number of independant runs processed.                               |
+----------------------------------+-----------------+----------------------------------------------+---------------------------------------------------------------------+
| sample_augmentation              | dict            | {'activate': False}                          | OTB parameters for sample augmentation                              |
+----------------------------------+-----------------+----------------------------------------------+---------------------------------------------------------------------+
| sample_management                | str             | None                                         | Absolute path to a CSV file containing samples transfert strategies |
+----------------------------------+-----------------+----------------------------------------------+---------------------------------------------------------------------+
| sample_selection                 | dict            | {'sampler': 'random', 'strategy': 'all'}     | OTB parameters for sample selection                                 |
+----------------------------------+-----------------+----------------------------------------------+---------------------------------------------------------------------+
| samples_classif_mix              | bool            | False                                        | Enable the second step of crop mix                                  |
+----------------------------------+-----------------+----------------------------------------------+---------------------------------------------------------------------+
| split_ground_truth               | bool            | True                                         | Enable the split of reference data                                  |
+----------------------------------+-----------------+----------------------------------------------+---------------------------------------------------------------------+
| validity_threshold               | int             | 1                                            |                                                                     |
+----------------------------------+-----------------+----------------------------------------------+---------------------------------------------------------------------+


Notes
-----

dempster_shafer_sar_opt_fusion
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Enable the use of both SAR and optical data to train a model. If disabled two models are trained.


features
^^^^^^^^

This parameter enable the computation of the three indices if available for the sensor used.There is no choice for using only one of them.


force_standard_labels
^^^^^^^^^^^^^^^^^^^^^

The chain label each features by the sensors name, the spectral band or indice and the date. If activated this parameter use the OTB default value (`value_X`)


mode_outside_regionsplit
^^^^^^^^^^^^^^^^^^^^^^^^

This parameter is available if regionPath is used and arg_classification.classif_mode is set to fusion. It represents the maximum size covered by a region. If the regions are larger than this threshold, then N models are built by randomly selecting features inside the region.


prev_features
^^^^^^^^^^^^^

This config file must be launchable by iota2 (needed for crop mix)


random_seed
^^^^^^^^^^^

Fix the random seed used for random split of reference data If set, the results must be the same for a given classifier


runs
^^^^

Number of independant runs processed. Each run has his own learning samples. Must be an integer greater than 0


sample_augmentation
^^^^^^^^^^^^^^^^^^^

In supervised classification the balance between class samples is important. There are any ways to manage class balancing in iota2, using :ref:`refSampleSelection` or the classifier's options to limit the number of samples by class.
An other approch is to generate synthetic samples. It is the purpose of thisfunctionality, which is called 'sample augmentation'.

    .. code-block:: python

        {'activate':False}

Example
-------

    .. code-block:: python

        sample_augmentation : {'target_models':['1', '2'],
                              'strategy' : 'jitter',
                              'strategy.jitter.stdfactor' : 10,
                              'strategy.smote.neighbors'  : 5,
                              'samples.strategy' : 'balance',
                              'activate' : True
                              }


iota2 implements an interface to the OTB `SampleAugmentation <https://www.orfeo-toolbox.org/CookBook/Applications/app_SampleSelection.html>`_ application.
There are three methods to generate samples : replicate, jitter and smote.The documentation :doc:`here <sampleAugmentation_explain>` explains the difference between these approaches.

 ``samples.strategy`` specifies how many samples must be created.There are 3 different strategies:    - minNumber
        To set the minimum number of samples by class required
    - balance
        balance all classes with the same number of samples as the majority one
    - byClass
        augment only some of the classes
Parameters related to ``minNumber`` and ``byClass`` strategies are    - samples.strategy.minNumber
        minimum number of samples
    - samples.strategy.byClass
        path to a CSV file containing in first column the class's label and 
        in the second column the minimum number of samples required.
In the above example, classes of models '1' and '2' will be augmented to thethe most represented class in the corresponding model using the jitter method.


sample_management
^^^^^^^^^^^^^^^^^

The CSV must contain a row per transfert
    .. code-block:: python

        >>> cat /absolute/path/myRules.csv
            1,2,4,2
Meaning :
        +--------+-------------+------------+----------+
        | source | destination | class name | quantity |
        +========+=============+============+==========+
        |   1    |      2      |      4     |     2    |
        +--------+-------------+------------+----------+



sample_selection
^^^^^^^^^^^^^^^^

This field parameters the strategy of polygon sampling. It directly refers to options of OTB’s SampleSelection application.

Example
-------

    .. code-block:: python

        sample_selection : {'sampler':'random',
                           'strategy':'percent',
                           'strategy.percent.p':0.2,
                           'per_models':[{'target_model':'4',
                                          'sampler':'periodic'}]
                           }

In the example above, all polygons will be sampled with the 20% ratio. But the polygons which belong to the model 4 will be periodically sampled, instead of the ransom sampling used for other polygons.
Notice than ``per_models`` key contains a list of strategies. Then we can imagine the following :

    .. code-block:: python

        sample_selection : {'sampler':'random',
                           'strategy':'percent',
                           'strategy.percent.p':0.2,
                           'per_models':[{'target_model':'4',
                                          'sampler':'periodic'},
                                         {'target_model':'1',
                                          'sampler':'random',
                                          'strategy', 'byclass',
                                          'strategy.byclass.in', '/path/to/myCSV.csv'
                                         }]
                           }
 where the first column of /path/to/myCSV.csv is class label (integer), second one is the required samples number (integer).


split_ground_truth
^^^^^^^^^^^^^^^^^^

 If set to False, the chain use all polygons for both training and validation


builders
========

+---------------------+----------------+-----------------------+--------------------------------------------+
| Parameter           | Type           | Default value         | Description                                |
+=====================+================+=======================+============================================+
| builders_class_name | list           | ['i2_classification'] | The name of the class defining the builder |
+---------------------+----------------+-----------------------+--------------------------------------------+
| builders_paths      | list           | None                  | The path to user builders                  |
+---------------------+----------------+-----------------------+--------------------------------------------+


Notes
-----

builders_class_name
^^^^^^^^^^^^^^^^^^^

Available builders are : 'i2_classification', 'i2_features_map' and 'i2_vectorization'


builders_paths
^^^^^^^^^^^^^^

If not indicated, the iota2 source directory is used: */iota2/sequence_builders/


chain
=====

+------------------------+----------------+---------------+-----------------------------------------------------------------------+
| Parameter              | Type           | Default value | Description                                                           |
+========================+================+===============+=======================================================================+
| check_inputs           | bool           | True          | Enable the inputs verification.                                       |
+------------------------+----------------+---------------+-----------------------------------------------------------------------+
| cloud_threshold        | int            | 0             | Threshold to consider that a pixel is valid                           |
+------------------------+----------------+---------------+-----------------------------------------------------------------------+
| color_table            | str            | None          | Absolute path to the file which link classes and their colors         |
+------------------------+----------------+---------------+-----------------------------------------------------------------------+
| data_field             | str            | None          | Field name indicating classes labels in `ground_thruth`               |
+------------------------+----------------+---------------+-----------------------------------------------------------------------+
| first_step             | str            | None          | The step group name indicating where the chain start                  |
+------------------------+----------------+---------------+-----------------------------------------------------------------------+
| ground_truth           | str            | None          | Absolute path to reference data                                       |
+------------------------+----------------+---------------+-----------------------------------------------------------------------+
| l5_path_old            | str            | None          | Absolute path to Landsat-5 images coming from old THEIA format (D*H*) |
+------------------------+----------------+---------------+-----------------------------------------------------------------------+
| l8_path                | str            | None          | Absolute path to Landsat-8 images comingfrom new tiled THEIA data     |
+------------------------+----------------+---------------+-----------------------------------------------------------------------+
| l8_path_old            | str            | None          | Absolute path to Landsat-8 images coming from old THEIA format (D*H*) |
+------------------------+----------------+---------------+-----------------------------------------------------------------------+
| last_step              | str            | None          | The step group name indicating where the chain ends                   |
+------------------------+----------------+---------------+-----------------------------------------------------------------------+
| list_tile              | str            | None          | List of tile to process, separated by space                           |
+------------------------+----------------+---------------+-----------------------------------------------------------------------+
| logger_level           | str            | INFO          | Set the logger level: NOTSET, DEBUG, INFO, WARNING, ERROR, CRITICAL   |
+------------------------+----------------+---------------+-----------------------------------------------------------------------+
| minimum_required_dates | int            | 2             | required minimum number of available dates for each sensor            |
+------------------------+----------------+---------------+-----------------------------------------------------------------------+
| nomenclature_path      | str            | None          | Absolute path to the nomenclature description file                    |
+------------------------+----------------+---------------+-----------------------------------------------------------------------+
| output_path            | str            | None          | Absolute path to the output directory.                                |
+------------------------+----------------+---------------+-----------------------------------------------------------------------+
| output_statistics      | bool           | False         | Enable the writing of PNG files containing additional statistics      |
+------------------------+----------------+---------------+-----------------------------------------------------------------------+
| proj                   | str            | EPSG:2154     | The projection wanted. Format EPSG:XXXX is mandatory                  |
+------------------------+----------------+---------------+-----------------------------------------------------------------------+
| region_field           | str            | region        | The column name for region indicator in`region_path` file             |
+------------------------+----------------+---------------+-----------------------------------------------------------------------+
| region_path            | str            | None          | Absolute path to region vector file                                   |
+------------------------+----------------+---------------+-----------------------------------------------------------------------+
| remove_output_path     | bool           | True          | Enable the removing of complete `output_path` directory               |
+------------------------+----------------+---------------+-----------------------------------------------------------------------+
| s1_path                | str            | None          | Absolute path to Sentinel-1 configuration file                        |
+------------------------+----------------+---------------+-----------------------------------------------------------------------+
| s2_l3a_output_path     | str            | None          | Absolute path to store preprocessed data in a dedicated directory.    |
+------------------------+----------------+---------------+-----------------------------------------------------------------------+
| s2_l3a_path            | str            | None          | Absolute path to Sentinel-2 L3A images (THEIA format)                 |
+------------------------+----------------+---------------+-----------------------------------------------------------------------+
| s2_output_path         | str            | None          | Absolute path to store preprocessed data in a dedicated directory.    |
+------------------------+----------------+---------------+-----------------------------------------------------------------------+
| s2_path                | str            | None          | Absolute path to Sentinel-2 images (THEIA format)                     |
+------------------------+----------------+---------------+-----------------------------------------------------------------------+
| s2_s2c_output_path     | str            | None          | Absolute path to store preprocessed data in a dedicated directory.    |
+------------------------+----------------+---------------+-----------------------------------------------------------------------+
| s2_s2c_path            | str            | None          | Absolute path to Sentinel-2 images (Sen2Cor format)                   |
+------------------------+----------------+---------------+-----------------------------------------------------------------------+
| spatial_resolution     | list           | 10            | Output spatial resolution                                             |
+------------------------+----------------+---------------+-----------------------------------------------------------------------+
| user_feat_path         | str            | None          | Absolute path to the user's features path                             |
+------------------------+----------------+---------------+-----------------------------------------------------------------------+


Notes
-----

check_inputs
^^^^^^^^^^^^

Enable the inputs verification. It can take a lot of time for large dataset. Check if region intersect reference data for instance


cloud_threshold
^^^^^^^^^^^^^^^

Indicates the threshold for a polygon to be used for learning. It use the validity count, which is incremented if a cloud, a cloud shadow or a saturated pixel is detected


output_path
^^^^^^^^^^^

Absolute path to the output directory.It is recommended to have one directory per run of the chain


output_statistics
^^^^^^^^^^^^^^^^^

Enable the writing of PNG files containing additional statisticscontaining the confidence by learning/validation pixels


remove_output_path
^^^^^^^^^^^^^^^^^^

Enable the removing of complete `output_path` directory
Only if the `first_step` is `init` and the folder name is valid


spatial_resolution
^^^^^^^^^^^^^^^^^^

The spatial resolution expected.It can be provided as integer or float,or as a list containing two values for non squared resolution


user_feat_path
^^^^^^^^^^^^^^

Absolute path to the user's features path They must be stored by tiles


coregistration
==============

+---------------+----------------+---------------+-----------------------------------------------------------------------------------------+
| Parameter     | Type           | Default value | Description                                                                             |
+===============+================+===============+=========================================================================================+
| band_ref      | int            | 1             | Number of the band of the VHR image to use for coregistration                           |
+---------------+----------------+---------------+-----------------------------------------------------------------------------------------+
| band_src      | int            | 3             | Number of the band of the src raster to use for coregistration                          |
+---------------+----------------+---------------+-----------------------------------------------------------------------------------------+
| date_src      | str            | None          | Date `YYYYMMDD` of the reference image                                                  |
+---------------+----------------+---------------+-----------------------------------------------------------------------------------------+
| date_vhr      | str            | None          | Date `YYYYMMDD` of the VHR image                                                        |
+---------------+----------------+---------------+-----------------------------------------------------------------------------------------+
| iterate       | bool           | True          | Proceed several iteration by reducing the step between geobin to find SIFT points       |
+---------------+----------------+---------------+-----------------------------------------------------------------------------------------+
| minsiftpoints | int            | 40            | Minimal number of SIFT points to find to create the new RPC model                       |
+---------------+----------------+---------------+-----------------------------------------------------------------------------------------+
| minstep       | int            | 16            | Minimal size of steps between bins in pixels                                            |
+---------------+----------------+---------------+-----------------------------------------------------------------------------------------+
| mode          | int            | 2             | Coregistration mode of the time series                                                  |
+---------------+----------------+---------------+-----------------------------------------------------------------------------------------+
| pattern       | str            | None          | Pattern of the time series files to coregister                                          |
+---------------+----------------+---------------+-----------------------------------------------------------------------------------------+
| prec          | int            | 3             | Estimated shift between source and reference raster in pixel (source raster resolution) |
+---------------+----------------+---------------+-----------------------------------------------------------------------------------------+
| resample      | bool           | True          | Resample the reference and the source rasterto the same resolution to find SIFT points  |
+---------------+----------------+---------------+-----------------------------------------------------------------------------------------+
| step          | int            | 256           | Initial size of steps between bins in pixels                                            |
+---------------+----------------+---------------+-----------------------------------------------------------------------------------------+
| vhr_path      | str            | None          | Absolute path to VHR path                                                               |
+---------------+----------------+---------------+-----------------------------------------------------------------------------------------+


Notes
-----

date_src
^^^^^^^^

If no `date_src` is mentionned, the best image will be automatically choose for coregistration


mode
^^^^

+--------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Mode   | Method                                                                                                                                                                                                                                    |
+========+===========================================================================================================================================================================================================================================+
|  1     |  single coregistration between one source image (and its masks) and the VHR image                                                                                                                                                         |
+--------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
|  2     | this mode operates a coregistration between a image of the timeseries and the VHR image, then the same RPC model is used to orthorectify every images of the timeseries                                                                   |
+--------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
|  3     | cascade mode, this mode operates a first coregistration between a source image and the VHR image, then each image of the timeseries is coregistered step by step with the closest temporal images of the timeseries already coregistered  |
+--------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+



pattern
^^^^^^^

By default the value is left to `None` and the pattern depends on the sensor used (*STACK.tif for Sentinel2, ORTHO_SURF_CORR_PENTE*.TIF)

Examples
--------
    .. code-block:: python

        pattern: '*STACK.tif'


dim_red
=======

+------------------+----------------+---------------+-----------------------------------------------------------------+
| Parameter        | Type           | Default value | Description                                                     |
+==================+================+===============+=================================================================+
| dim_red          | bool           | False         | Enable the dimensionality reduction mode                        |
+------------------+----------------+---------------+-----------------------------------------------------------------+
| reduction_mode   | str            | global        | The reduction mode                                              |
+------------------+----------------+---------------+-----------------------------------------------------------------+
| target_dimension | int            | 4             | The number of dimension required, according to `reduction_mode` |
+------------------+----------------+---------------+-----------------------------------------------------------------+


Notes
-----

reduction_mode
^^^^^^^^^^^^^^

Values authorized are: 'global' or '?'


external_features
=================

+------------------------+----------------+------------------------+--------------------------------------------------------+
| Parameter              | Type           | Default value          | Description                                            |
+========================+================+========================+========================================================+
| chunk_size_mode        | str            | split_number           | The chunk split mode                                   |
+------------------------+----------------+------------------------+--------------------------------------------------------+
| chunk_size_x           | int            | 50                     | The number if rows for chunk                           |
+------------------------+----------------+------------------------+--------------------------------------------------------+
| chunk_size_y           | int            | 50                     | The number if rows for chunk                           |
+------------------------+----------------+------------------------+--------------------------------------------------------+
| concat_mode            | bool           | True                   | Enable the use of all features                         |
+------------------------+----------------+------------------------+--------------------------------------------------------+
| external_features_flag | bool           | False                  | Enable the external features mode                      |
+------------------------+----------------+------------------------+--------------------------------------------------------+
| functions              | str            | None                   | The function list to be used to compute features       |
+------------------------+----------------+------------------------+--------------------------------------------------------+
| module                 | str            | /path/to/iota2/sources | Absolute path for user source code                     |
+------------------------+----------------+------------------------+--------------------------------------------------------+
| number_of_chunks       | int            | 50                     | The expected number of chunks                          |
+------------------------+----------------+------------------------+--------------------------------------------------------+
| output_name            | str            | None                   | Temporary chunks are written using this name as prefix |
+------------------------+----------------+------------------------+--------------------------------------------------------+


Notes
-----

concat_mode
^^^^^^^^^^^

If disabled, only external features are used in the whole processing


iota2_feature_extraction
========================

+-----------------+----------------+---------------+-------------+
| Parameter       | Type           | Default value | Description |
+=================+================+===============+=============+
| acor_feat       | bool           | False         |             |
+-----------------+----------------+---------------+-------------+
| copy_input      | bool           | True          |             |
+-----------------+----------------+---------------+-------------+
| extract_bands   | bool           | False         |             |
+-----------------+----------------+---------------+-------------+
| keep_duplicates | bool           | True          |             |
+-----------------+----------------+---------------+-------------+
| rel_refl        | bool           | False         |             |
+-----------------+----------------+---------------+-------------+


scikit_models_parameters
========================

+-----------------------------+----------------+---------------+----------------------------------+
| Parameter                   | Type           | Default value | Description                      |
+=============================+================+===============+==================================+
| cross_validation_folds      | int            | 5             | The number of k-folds            |
+-----------------------------+----------------+---------------+----------------------------------+
| cross_validation_grouped    | bool           | False         |                                  |
+-----------------------------+----------------+---------------+----------------------------------+
| cross_validation_parameters | dict           | {}            |                                  |
+-----------------------------+----------------+---------------+----------------------------------+
| model_type                  | str            | None          | machine learning algorthm’s name |
+-----------------------------+----------------+---------------+----------------------------------+
| standardization             | bool           | False         |                                  |
+-----------------------------+----------------+---------------+----------------------------------+


Notes
-----

model_type
^^^^^^^^^^

Models comming from scikit-learn are use if scikit_models_parameters.model_type is different from None. More informations about how to use scikit-learn is available at iota2 and scikit-learn machine learning algorithms.


sensors_data_interpolation
==========================

+-------------------------+----------------+---------------+-----------------------------------------------+
| Parameter               | Type           | Default value | Description                                   |
+=========================+================+===============+===============================================+
| auto_date               | bool           | True          | Enable the use of `start_date` and `end_date` |
+-------------------------+----------------+---------------+-----------------------------------------------+
| use_additional_features | bool           | False         | Enable the use of additional features         |
+-------------------------+----------------+---------------+-----------------------------------------------+
| use_gapfilling          | bool           | True          | Enable the use of gapfilling                  |
+-------------------------+----------------+---------------+-----------------------------------------------+
| write_outputs           | bool           | False         | Write temporary files                         |
+-------------------------+----------------+---------------+-----------------------------------------------+


Notes
-----

auto_date
^^^^^^^^^

If True, iota2 will automatically guess the first and the last interpolation date. Else, `start_date` and `end_date` of each sensors will be used


write_outputs
^^^^^^^^^^^^^

Write the time series before and after gapfilling, the mask time series, and also the feature time series. This option required a large amount of free disk space.


task_retry_limits
=================

+---------------+---------------+---------------+-------------------------------------------+
| Parameter     | Type          | Default value | Description                               |
+===============+===============+===============+===========================================+
| allowed_retry | int           | 0             | Allow dask to retry a failed job N times. |
+---------------+---------------+---------------+-------------------------------------------+
| maximum_cpu   | int           | 4             | The maximum number of CPU available       |
+---------------+---------------+---------------+-------------------------------------------+
| maximum_ram   | int           | 16            | The maximum amount of RAM available. (gB) |
+---------------+---------------+---------------+-------------------------------------------+


Notes
-----

maximum_cpu
^^^^^^^^^^^

the amout of cpu will be doubled if the task is killed due to ram overconsumption until maximum_cpu or allowed_retry are reach


maximum_ram
^^^^^^^^^^^

the amout of RAM will be doubled if the task is killed due to ram overconsumption until maximum_ram or allowed_retry are reach


userFeat
========

+-----------+---------------+---------------+--------------------------------------+
| Parameter | Type          | Default value | Description                          |
+===========+===============+===============+======================================+
| arbo      | str           | /*            | The input folder hierarchy           |
+-----------+---------------+---------------+--------------------------------------+
| patterns  | str           | ALT,ASP,SLP   | key name for detect the input images |
+-----------+---------------+---------------+--------------------------------------+


