Access develop branch
#####################

Release planning
****************

A release is a long time process, which requires suffisant contribution to be a relevant improvement. Otherwise, the new features must be tested before, then they can be stay in development for several months.

For help development or to use a new feature not yet released, it can be useful to access the current development branch and use it as a full operational conda enviromnent.

Access via conda env
********************

On anaconda cloud, a package named `iota2_develop <https://anaconda.org/iota2/iota2_develop>`_ is available. This package can be updated on demand, and contains the state of develop branch corresponding to his creation.

This environment is fully operationnal but not correspond to the current development branch (bug fix or recent add). But it permits to install easily most of the dependencies.

Real-time access to develop
***************************

Once you have a conda environment (here `iota_dev`) installed you can access to the source of iota2.

First, clone iota2 project somewhere:

.. code-block:: console

	cd /home/doc/somewhere/
	git clone https://framagit.org/iota2-project/iota2.git

Then go in environment location:

.. code-block:: console

   cd /home/doc/miniconda3/envs/iota_dev/lib/python3.6/site-packages

Keep the source for security, and create a symbolic link to the cloned project

.. code-block:: console

   mv iota2 iota2_backup
   ln -s /home/doc/somewhere/iota2/iota2 iota2

.. warning::

   Please note that iota2 is twice in path. The first path must contains code folders like `Common`, `Steps`, ...


Once done, simply use this environment like all other:

.. code-block:: console

   conda activate iota_dev
   Iota2.py ...

Then you can update the code locate in `/home/doc/somewhere/iota2`, with git operation (pull, checkout, branch) or create your own branch and modify codes. The conda environment always use the current state of your iota2 source directory.
