i2_vectorization
################

builders
========

+---------------------+----------------+-----------------------+--------------------------------------------+
| Parameter           | Type           | Default value         | Description                                |
+=====================+================+=======================+============================================+
| builders_class_name | list           | ['i2_classification'] | The name of the class defining the builder |
+---------------------+----------------+-----------------------+--------------------------------------------+
| builders_paths      | list           | None                  | The path to user builders                  |
+---------------------+----------------+-----------------------+--------------------------------------------+


Notes
-----

builders_class_name
^^^^^^^^^^^^^^^^^^^

Available builders are : 'i2_classification', 'i2_features_map' and 'i2_vectorization'


builders_paths
^^^^^^^^^^^^^^

If not indicated, the iota2 source directory is used: */iota2/sequence_builders/


chain
=====

+-------------+---------------+---------------+------------------------------------------------------+
| Parameter   | Type          | Default value | Description                                          |
+=============+===============+===============+======================================================+
| output_path | str           | None          | Absolute path to the output directory.               |
+-------------+---------------+---------------+------------------------------------------------------+
| proj        | str           | EPSG:2154     | The projection wanted. Format EPSG:XXXX is mandatory |
+-------------+---------------+---------------+------------------------------------------------------+


Notes
-----

output_path
^^^^^^^^^^^

Absolute path to the output directory.It is recommended to have one directory per run of the chain


simplification
==============

+-------------------------------------+----------------+-------------------------------------------+---------------------------------------------------------------------------------------+
| Parameter                           | Type           | Default value                             | Description                                                                           |
+=====================================+================+===========================================+=======================================================================================+
| angle                               | bool           | True                                      | If True, smoothing corners of pixels (45°)                                            |
+-------------------------------------+----------------+-------------------------------------------+---------------------------------------------------------------------------------------+
| bingdal                             | str            | None                                      | path to GDAL binaries                                                                 |
+-------------------------------------+----------------+-------------------------------------------+---------------------------------------------------------------------------------------+
| blocksize                           | int            | 2000                                      | block size to split raster to prevent Numpy memory error                              |
+-------------------------------------+----------------+-------------------------------------------+---------------------------------------------------------------------------------------+
| chunk                               | int            | 10                                        | Number of chunks for statistics computing                                             |
+-------------------------------------+----------------+-------------------------------------------+---------------------------------------------------------------------------------------+
| classification                      | str            | None                                      | Input raster of classification                                                        |
+-------------------------------------+----------------+-------------------------------------------+---------------------------------------------------------------------------------------+
| clipfield                           | str            | None                                      | field to identify distinct areas                                                      |
+-------------------------------------+----------------+-------------------------------------------+---------------------------------------------------------------------------------------+
| clipfile                            | str            | None                                      | vector-based file to clip output vector-based classification                          |
+-------------------------------------+----------------+-------------------------------------------+---------------------------------------------------------------------------------------+
| clipvalue                           | int            | None                                      | value of field which identify distinct areas                                          |
+-------------------------------------+----------------+-------------------------------------------+---------------------------------------------------------------------------------------+
| confidence                          | str            | None                                      | Input raster of confidence                                                            |
+-------------------------------------+----------------+-------------------------------------------+---------------------------------------------------------------------------------------+
| douglas                             | int            | 10                                        | Douglas-Peucker tolerance for vector-based generalization                             |
+-------------------------------------+----------------+-------------------------------------------+---------------------------------------------------------------------------------------+
| dozip                               | bool           | True                                      | Zip output vector-based classification (OSO-like production)                          |
+-------------------------------------+----------------+-------------------------------------------+---------------------------------------------------------------------------------------+
| grasslib                            | str            | None                                      | path to grasslib                                                                      |
+-------------------------------------+----------------+-------------------------------------------+---------------------------------------------------------------------------------------+
| gridsize                            | int            | None                                      | Number of lines and columns of serialization process                                  |
+-------------------------------------+----------------+-------------------------------------------+---------------------------------------------------------------------------------------+
| hermite                             | int            | 10                                        | Hermite Interpolation threshold for vector-based smoothing                            |
+-------------------------------------+----------------+-------------------------------------------+---------------------------------------------------------------------------------------+
| inland                              | str            | None                                      | Inland water limit shapefile                                                          |
+-------------------------------------+----------------+-------------------------------------------+---------------------------------------------------------------------------------------+
| lcfield                             | str            | Class                                     | Name of the field to store landcover class in vector-based classification             |
+-------------------------------------+----------------+-------------------------------------------+---------------------------------------------------------------------------------------+
| lib64bit                            | str            | None                                      | Path of BandMath and Concatenate OTB executables returning 64-bits float pixel values |
+-------------------------------------+----------------+-------------------------------------------+---------------------------------------------------------------------------------------+
| mmu                                 | int            | 1000                                      | MMU of output vector-based classification (projection unit),(Default : 0.1 ha)        |
+-------------------------------------+----------------+-------------------------------------------+---------------------------------------------------------------------------------------+
| nomenclature                        | str            | None                                      | configuration file which describe nomenclature                                        |
+-------------------------------------+----------------+-------------------------------------------+---------------------------------------------------------------------------------------+
| outprefix                           | str            | dept                                      | Prefix to use for naming of vector-based classifications                              |
+-------------------------------------+----------------+-------------------------------------------+---------------------------------------------------------------------------------------+
| rssize                              | int            | 20                                        | Resampling size of input classification raster (projection unit)                      |
+-------------------------------------+----------------+-------------------------------------------+---------------------------------------------------------------------------------------+
| seed                                | int            | 1                                         | Seed of input raster classification                                                   |
+-------------------------------------+----------------+-------------------------------------------+---------------------------------------------------------------------------------------+
| statslist                           | dict           | {1: 'rate', 2: 'statsmaj', 3: 'statsmaj'} | dictionnary of requested landcover statistics                                         |
+-------------------------------------+----------------+-------------------------------------------+---------------------------------------------------------------------------------------+
| systemcall                          | bool           | False                                     | If True, use yours gdal lib (cf. bingdal)                                             |
+-------------------------------------+----------------+-------------------------------------------+---------------------------------------------------------------------------------------+
| umc1                                | int            | None                                      | MMU for first regularization                                                          |
+-------------------------------------+----------------+-------------------------------------------+---------------------------------------------------------------------------------------+
| umc2                                | int            | None                                      | MMU for second regularization                                                         |
+-------------------------------------+----------------+-------------------------------------------+---------------------------------------------------------------------------------------+
| validity                            | str            | None                                      | Input raster of validity                                                              |
+-------------------------------------+----------------+-------------------------------------------+---------------------------------------------------------------------------------------+
| vectorize_fusion_of_classifications | bool           | False                                     | flag to inform iota2 to vectorize the fusion of classifications                       |
+-------------------------------------+----------------+-------------------------------------------+---------------------------------------------------------------------------------------+


Notes
-----

bingdal
^^^^^^^

Some GDAL lib versions (automatically set up with iota²) are not efficient to handle topology errors, use yours !


blocksize
^^^^^^^^^

Numpy memory error may occur for large areas during serialization process. Split in sub-rasters prevents memory error


chunk
^^^^^

Number of chunks (groups of vector-based features) for parallel computing landcover statistics


classification
^^^^^^^^^^^^^^

This parameter is automatically set if the configuration file use the classification and vectorization builders


clipfield
^^^^^^^^^

field to identify distinct geographical/administrativeareas (cf. "clipfile" parameter)


clipfile
^^^^^^^^

vector-based file can contain more than one feature (geographical/administrative areas). An output vector-based classification is produced for each feature (cf. 'clipfield' parameter).


clipvalue
^^^^^^^^^

output vector-based classification is only produced on the specific area (clipfield=clipvalue in clipfile) (cf. 'clipfield' parameter). If None, all areas are produced.


confidence
^^^^^^^^^^

This parameter is automatically set if the configuration file use the classification and vectorization builders


grasslib
^^^^^^^^

Some functions of GRASS GIS software are used to vectorize, simplify and smooth vector layer. This path corresponds to GRASS install folder


gridsize
^^^^^^^^

This parameter is useful only for large areas for which vectorization process can not be executed (memory limitation). By 'serialization', we mean parallel vectorization processes. If not None, regularized classification raster is splitted in gridsize x gridsize rasters


inland
^^^^^^

to vectorize only inland waters, and not unnecessary sea water areas


lib64bit
^^^^^^^^

Band math and concatenate OTB executables with 64 bits capabilities (only for large areas where clumps number > 2²³ bits for mantisse)


nomenclature
^^^^^^^^^^^^

This configuration file includes code, color, description and vector field alias of each class
Classes:
{
        Level1:
        {
                "Urbain":
                {
                code:100
                alias:"Urbain"
                color:"#b106b1"
                }
                ...
        }
        Level2:
        {
               "Urbain dense":
               {
               code:1
               alias:"UrbainDens"
               color:"#ff00ff"
               parent:100
               }
               ...
        }
}



outprefix
^^^^^^^^^

Naming of vector-based classifications is as following : prefix_clipvalue


rssize
^^^^^^

OSO-like vectorization requires a resampling step in order to regularize and decrease raster polygons number, If None, classification is not resampled


seed
^^^^

This parameter is usefull to vectorize one specific output classification seed


statslist
^^^^^^^^^

Different landcover statistics can be computed for vector-based classification file. A python-like dictionnary must be provided. This is the OSO-like statistics : {1: "rate", 2: "statsmaj", 3: "statsmaj"}1: "rate" :      rates of classification classes are computed      for each polygon
2: "statsmaj" :      descriptive stats of classifier confidence are computed      for each polygon by using only majority class pixels
3: "statsmaj" :      descriptive stats of sensor validity are computed      for each polygon by using only majority class pixels

list of available statistics : 
    - stats : mean_b, std_b, max_b, min_b
    - statsmaj : meanmaj, stdmaj, maxmaj, minmaj of maj. class
    - rate : rate of each pixel value (classe names)
    - stats_cl : mean_cl, std_cl, max_cl, min_cl of one class



umc1
^^^^

It is an interface of parameter '-st' of gdal_sieve.py function. If None, classification is not regularized


umc2
^^^^

OSO-like vectorization process requires 2 successive regularization, if you need a single regularization, let this parameter to None


validity
^^^^^^^^

This parameter is automatically set if the configuration file use the classification and vectorization builders


vectorize_fusion_of_classifications
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This flag is only useful if the vectorization is chained with classification workflow


task_retry_limits
=================

+---------------+---------------+---------------+-------------------------------------------+
| Parameter     | Type          | Default value | Description                               |
+===============+===============+===============+===========================================+
| allowed_retry | int           | 0             | Allow dask to retry a failed job N times. |
+---------------+---------------+---------------+-------------------------------------------+
| maximum_cpu   | int           | 4             | The maximum number of CPU available       |
+---------------+---------------+---------------+-------------------------------------------+
| maximum_ram   | int           | 16            | The maximum amount of RAM available. (gB) |
+---------------+---------------+---------------+-------------------------------------------+


Notes
-----

maximum_cpu
^^^^^^^^^^^

the amout of cpu will be doubled if the task is killed due to ram overconsumption until maximum_cpu or allowed_retry are reach


maximum_ram
^^^^^^^^^^^

the amout of RAM will be doubled if the task is killed due to ram overconsumption until maximum_ram or allowed_retry are reach


