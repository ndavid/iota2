#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
""" Provide examples for documentation"""


class example_class:
    """ Empty class"""
    def __init__(self):
        """ Do nothing"""
        pass

    def meth1(self):
        pass


def example_method(arg1: int, arg2: int) -> int:
    """ 
    This method sum the two args

    Parameters
    ----------
    
    arg1:
        a integer value
    arg2:
        a integer value

    Notes
    -----
        The output value is the sum of the two inputs
    """
    return arg1 + arg2
