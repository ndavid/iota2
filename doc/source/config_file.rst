.. _config-file-main-page:

Configuration file parameters
#############################

.. toctree::
   :maxdepth: 2
			  
   param_generated/all_parameters
   param_generated/i2_classification_builder
   param_generated/i2_features_map_builder


..
   .. include:: ./param_generated/all_parameters.rst
   .. include:: ./param_generated/i2_classification_builder.rst
   .. include:: ./param_generated/i2_features_map_builder.rst

