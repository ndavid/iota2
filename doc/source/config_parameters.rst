.. _config-file-main-page:

Configuration file parameters
#############################

.. toctree::

    all_parameters
    i2_classification_builder
    i2_features_map_builder
    i2_vectorization_builder
