How to get iota2 ?
==================

Licence
-------

:abbr:`iota2 (Infrastructure pour l'Occupation des sols par Traitement Automatique Incorporant les Orfeo Toolbox Applications)`
is a free software under the GNU Affero General Public License v3.0. See `here <http://www.gnu.org/licenses/agpl.html>`_ 
for details.

How to install iota2 ?
----------------------

iota2 is only frequently tested on some Linux distributions (Ubuntu and CentOS), although others are known to work (Debian).
It is distributed through the Conda package management system which install and manage all its depency.

In this section, steps to install iota² from scratch will be detailed.

**Step 1 :** download and install Miniconda

If Miniconda or Anaconda is already installed on your system, you can skip this step. 
Otherwise, you can download and install Miniconda thanks to the 
`bash installer <https://conda.io/en/latest/miniconda.html>`_  even if 
your are not the system administrator.

.. Note:: If the installation is inside a virtual machine, do not use a shared directory with the host system as iota² output directory.

**Step 2 :** add iota² channel

Add the iota² channel to inform Conda where iota² packages are. This information 
has to be added in the ``.condarc`` file. In order to locate the file, please enter the following 
command :

.. code-block:: console

    conda info

then a list of informations are printed, especially :

.. code-block:: console

    active environment : None
            shell level : 0
       user config file : /absolute/path/to/.condarc
 populated config files : /absolute/path/to/.condarc
          conda version : 4.7.10
    conda-build version : 3.18.2
    ...

.. Note:: Maintain conda up to date using the ``conda update conda`` command

Once the ``.condarc`` file is located (create it at the place ``user config file`` if not exists), you must add the following in it.

.. code-block:: console

    channel_priority: true
    channels:
        - iota2
        - conda-forge
        - defaults


**Step 3 :** get the iota² package and install it

.. WARNING:: To avoid some conflict or wrong compatibility always install iota2 in a fresh environment. Also always install iota2 first before another custom package

   
.. code-block:: console

    # create an empty conda environment : iota2-env
    conda create --name iota2-env

    # install iota2 in iota2-env (this may take a while)
    conda install -c iota2 iota2 -n iota2-env
	
.. Note:: Download all iota2 dependencies, from a fresh miniconda installation, requires at least 5go of free disk space. Each instaled environment containing only iota2 and dependencies requires 3.7go of free disk space.

.. WARNING:: Installing iota2 may be quite long according to the user configuration computer and network (proxy, internet speed). Sometime the solving environment can take a very long time before ends. 

**Step 3 bis:** Use mamba get the iota² package and install it


.. Note:: If the installation using conda is too slow or not ending consider the following workaround

.. code-block:: console

   # create an empty conda environment: iota2-env
   conda create --name iota2-env
   # enable it
   conda activate iota2-env
   # install recent python 3.6
   conda install python=3.6
   # install mamba
   conda install mamba
   # install iota2
   mamba install iota2 -c iota2
   # disable environment and reload it
   conda deactivate
   conda activate iota2-env
   # test if the installation ends well
   Iota2.py -h


How to test the installation ?
------------------------------

You can test the installation by following the :doc:`tutorial <i2_classification_tutorial>`.

.. Note:: More informations about iota2 launching parameters are available :doc:`here <going_further_i2_launching_params>`
