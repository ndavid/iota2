iota2 feature maps
##################

Assuming iota2 is :doc:`fully operational <HowToGetIOTA2>`, this chapter presents an usage of iota2: the production of features maps using satelitte images time series.


Introduction to data
********************

iota2 handles several sensors images :

* Landsat 5 and 8 (old and new THEIA format)
* Sentinel 1, Sentinel 2 L2A(THEIA and Sen2cor), Sentinel L3A (Theia Format)
* Various other images already processed, with the ```userFeat`` sensor

In this chapter, only the use of Sentinel 2 L2A will be illustrated.
To use other sensor, it is necessary to adapt the inputs parameters.

To produce spectral indices, an user can use all spectral indices already computed by iota "link to spectral features in iota2 (NDVI...)" ad provide python code to produce custom features.
 
Get the data-set
================

TODO: adapt dataset, remove unused files ?

Two data-set are available, containing minimal data required to produce a land cover map:

* A entire Sentinel 2 tile, with two dates (8.8 Go)

* An extraction of Sentinel 2 data, with three dates over different eco-climatic region (soon)


 
The first archive contains:


.. code-block:: console

    ├── colorFile.txt
    ├── IOTA2_Example.cfg
    ├── IOTA2_Outputs
    │   └── Results
    │       └── #IOTA2's output (according to the configuration file)
    ├── nomenclature.txt
    ├── sensor_data
    │   ├── T31TCJ
    │   │   ├── SENTINEL2A_20180511-105804-037_L2A_T31TCJ_D_V1-7
    │   │   │   ├── MASKS
    │   │   │   │   └── *.tif
    │   │   │   └── *.tif
    │   │   └── SENTINEL2A_20180521-105702-711_L2A_T31TCJ_D_V1-7
    │   │       ├── MASKS
    │   │       │   └── *.tif
    │   │       └── *.tif
    │   └── T31TDJ
    │       └── #empty
    └── vector_data
        ├── EcoRegion.shp
        └── groundTruth.shp

		

``sensor_data`` : the directory which contains Sentinel-2 data. These data **must** be stored by tiles as in the archive.

``IOTA2_Example.cfg`` : the file used to set iota2's parameters such as inputs/outputs paths, classifier parameters etc.

Understand the configuration file
=================================

iota2 exploits hundreds of parameters, native or coming from other libraries such as scikit-learn or OTB.

These combinations of parameters will make it possible to choose the treatments to be carried out, the choice of the algorithms used or which output to write. A documentation of all these parameters is available `here <>`.

To simplify the use, iota2 read a configuration file which is a simple text file containing sections and fields.

The minimal configuration file contains all required fields to produce a land cover map.

.. include:: examples/config_tuto_features_maps.cfg
    :literal:


For an end user, launching iota2 requires to fill correctly the configuration file.

In the above example, replace the ``XXXX`` by the path where the archive has been extracted.

Running the chain
=================

iota2 launch
************

To launch the chain, simply use the command line

.. code-block:: console

    Iota2.py -config /XXXX/IOTA2_TESTS_DATA/config_tuto_classif.cfg

First, the chain displays the list of all steps activated by the configuration file


"todo Ajout d'un fichier qui contient le print summarize de la chaine mis à jour avec les pre-push ?"

.. include:: examples/steps_features_maps.txt
    :literal:

Once the processing start, a large amount of information will be printed, most of them concerning the dask-scheduler.

Did it all go well?
===================

iota2 displays a lot of information during execution.
It becomes difficult to identify if everything went well or if an error occurred.

To this end, in the ``output_path`` a log directory is available : ``output_path/logs``.

The logs directory is ordered as:

.. code-block:: console

   draw tree todo

Each step has its own log directory.
In these directories two kind of log can be found ``*_out.log`` and ``*_err.log``.
The error are compiled in "err" file and the standard output in "out" file.
With the scheduler dask, iota2 go as far as possible while the data required for the next steps is available.
To simplify the error identification, an interactive graph is produced in a html page.
Nodes in graph can have three colors (red: error, blue: done, white: not yielded).
By clicking on graph node, the corresponding log file is openned.

If despite all this information, the errors can not be identifiyed or solved, the iota2 can help all users.
The simplest way to ask help is to create an issue on framagit ""lien framagit"" by adding the archive available in log directory.

				
Output tree structure
=====================

In this section, all output folders are described.

.. code-block:: console

    draw tree


	
customF
-------

Temporary data used for reassemble maps for each tiles.
Remove after use ?


envelope
--------

Contains shapefiles, one for each tile.
Used to ensure tile priority, with no overlap.


final
-----

This folder contains the final products of iota2.


features
--------

For each tiles, contains useful information:

- ``nbView.tif`` : number of a pixel is non masked in the whole time series

- ``CloudThreshold.shp`` : mask  the pixels if nbView is too not

- ``tmp/MaskComminSL`` : the mask of all sensors for this tile

- ``tmp/Sentinel2L3A_T31TCJ_reference.tif`` : the image used for reprojecting data

- ``tmp/Sentinel2L3A_T31TCJ_input_dates.txt`` : the list of date detected in ``s2_path``

Final products
==============

Features map
------------



To go further
=============


.. toctree::
   :maxdepth: 1

	External features <external_features>


