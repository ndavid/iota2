iota2-v20210203 - Changes since version v20200309
-------------------------------------------------

Features added
**************

*      Be able to change raster's resolution (issue 212)
            Using the parameter "spatial_resolution" users are able to set iota2 working resolution
*      Enable custom features, defined by a python function
            Allow users to provide a python function to create new features (which will be used for learning and classification stages)
*      Code style
            Use yapf to standardize python scripts
*      Remove configuration file from every iota2 function (API).
            Most iota2 functions are usable without using a configuration file
*      New scheduling workflow and builder design
            Use Dask to schedule tasks
*      New documentation architecture and host
            Migration from readthedocs.io to http://osr-cesbio.ups-tlse.fr
*      Refactoring of the configuration file
            Standardization and reorganization of parameters between sections. Old configuration files are automatically detected and parsed to the new shape.
*      Re-encoding of labels
            Iota2 is able to deal with string labels
*      Check if enough dates are available before running iota2
            New parameter "minimum_required_dates"
*      Launch iota2 only on sensor data already processed by iota2 (i.e : s2_output_path becomes s2_path)


Bugs fixed
**********

*     BUG FIX: alter user database (convert the datafield to lowercase)
*     BUG FIX: issue #213 random segfault when ExportImage
*     BUG FIX: data augmentation step gets wrong files (issue #215)
*     BUG FIX : CSV file separator (issue 216)
*     BUG FIX: Sentinel L3A usage (issue 259)


iota2-v20200309 - Changes since version v0.5.1
----------------------------------------------

Features added
**************

* check iota2 inputs
* iota2 can use autoContext workflow
* iota2 can use scikit-learn machine-learning algorithms
* iota2 can use cross-validation algorithm on scikit-learn models
* installation thanks to conda
* generate probability map
* fusion post classification of optical and SAR data
* fusion of classifications
* available sensors :
    - Sentinel-1
    - Sentinel-2
        L2A format, THEIA + PEPS
        L3A format
    - Landsat-5 old format
    - Landsat-5 new format 
    - Landsat-8 old format
    - Landsat-8 new format
    - User features  
* post classification tools
    - regularisation
        - majority
        - adaptative
    - vectorisation / simplification /smooth
    - zonal statistics

Bugs fixed
**********

