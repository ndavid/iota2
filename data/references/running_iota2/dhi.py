import numpy as np


def get_cumulative_productivity(self):
    print("cumul")
    coef = np.sum(self.get_Sentinel2_NDVI() / 1000.0, axis=2)
    labels = ["cumul_prod"]
    return coef, labels


def get_minimum_productivity(self):
    print("min")
    coef = np.min(self.get_Sentinel2_NDVI() / 1000.0, axis=2)
    labels = ["min_prod"]
    return coef, labels


def get_seasonal_variation(self):
    print("var")
    coef = (np.std(self.get_Sentinel2_NDVI() / 1000.0, axis=2) /
            (np.mean(self.get_Sentinel2_NDVI() / 1000.0, axis=2) + 1E-6))

    labels = ["var_prod"]
    return coef, labels
