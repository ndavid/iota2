#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   vector tools
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================

import os
import sys
import math
import time
import osgeo
import shutil
import random
import logging
import warnings
import argparse
import numpy as np
from shutil import copyfile
from osgeo import ogr, gdal
from typing import Set
from typing import List
from typing import Dict
from typing import Union
from typing import Tuple
from typing import Optional

from iota2.Common.Utils import run

LOGGER = logging.getLogger("distributed.worker")


def re_encode_field(input_db: str,
                    in_field: str,
                    new_field: str,
                    new_field_type,
                    new_field_size: int,
                    dict_rules: Dict,
                    logger=LOGGER) -> None:
    """modify inplace the input database. Add a new field according the the 'in_field' 
    values and the dictionary rules

    Parameters
    ----------
    input_db
        input database
    in_field
        field to perform conversion rules
    new_field
        field to create
    new_field_type
        new field type
    new_field_size
        new field size
    dict_rules:
        dict_rules[in_field] = new_value
    """

    ds = ogr.Open(input_db)
    driver = ds.GetDriver()
    dataset = driver.Open(input_db, 1)
    layer = dataset.GetLayer()

    all_fields = []
    inLayerDefn = layer.GetLayerDefn()
    for i in range(inLayerDefn.GetFieldCount()):
        field = inLayerDefn.GetFieldDefn(i).GetName()
        all_fields.append(field)

    if new_field not in all_fields:
        field_name = ogr.FieldDefn(new_field, new_field_type)
        field_name.SetWidth(new_field_size)
        layer.CreateField(field_name)
        for feat in layer:
            feat.SetField(new_field, dict_rules[feat.GetField(in_field)])
            layer.SetFeature(feat)
    else:
        logger.warning(
            f"field {new_field} already exists in {input_db}, skipping adding it"
        )


def get_re_encoding_labels_dic(
        ref_data: str, ref_data_field: str) -> Dict[Union[str, int], int]:
    """
    Parameters
    ----------
    ref_data
        input reference database
    ref_data_field
        column containing labels database
    Notes
    -----
    return the labels conversion dictionary dic[old_label] = new_label
    """
    ref_field_type = getFieldType(ref_data, ref_data_field)
    label_type = None
    if int == ref_field_type:
        label_type = "int"
    elif str == ref_field_type:
        label_type = "str"
    else:
        raise ValueError(
            f"Input reference database labels must be string or integer format."
        )
    all_labels = sorted(
        getFieldElement(ref_data,
                        field=ref_data_field,
                        mode="unique",
                        elemType=label_type))
    lut_labels = {}
    for new_label, old_label in zip(range(1, len(all_labels) + 1), all_labels):
        lut_labels[old_label] = new_label
    return lut_labels


def get_vector_proj(vector_file: str,
                    driver: Optional[str] = "ESRI Shapefile"):
    """get vector projection
    """
    driver = ogr.GetDriverByName(driver)
    dataset = driver.Open(vector_file)
    layer = dataset.GetLayer()
    spatialRef = layer.GetSpatialRef()
    return spatialRef.GetAttrValue("AUTHORITY", 1)


def get_geom_column_name(vector_file: str,
                         driver: Optional[str] = "ESRI Shapefile"):
    """
    get field's name containing features' geometries

    Parameters
    ----------
    vector_file :
        input vector file
    driver :
        ogr driver's name

    """
    column_name = None
    if os.path.exists(vector_file):
        driver = ogr.GetDriverByName(driver)
        dataSource = driver.Open(vector_file, 0)
        layer = dataSource.GetLayer()
        column_name = layer.GetGeometryColumn()
    else:
        raise Exception("File not found : {}".format(vector_file))

    if not column_name:
        column_name = "GEOMETRY"

    return column_name


def openToRead(shapefile: str, driver: Optional[str] = "ESRI Shapefile"):
    """
    Opens a shapefile to read it and returns the datasource in read mode

    Parameters
    ----------
    shapefile:
        the input vector file to open
    driver:
        Optional driver name for vector file
    """
    driver = ogr.GetDriverByName(driver)
    if driver.Open(shapefile, 0):
        data_source = driver.Open(shapefile, 0)
    else:
        print("Not possible to open the file " + shapefile)
        sys.exit(1)

    return data_source


# TODO: UNUSED
# def mergeFeatures(shapefile, field="", value=""):

#     ds = openToWrite(shapefile)
#     layer = ds.GetLayer()
#     newGeometry = None
#     for feature in layer:
#         geometry = feature.GetGeometryRef()
#         if newGeometry is None:
#             newGeometry = geometry.Clone()
#         else:
#             newGeometry = newGeometry.Union(geometry)

#     return newGeometry


def openToWrite(shapefile, driver="ESRI Shapefile"):
    """ 
   Opens a shapefile to read it and returns the datasource in write mode
   """
    driver = ogr.GetDriverByName(driver)
    if driver.Open(shapefile, 1):
        dataSource = driver.Open(shapefile, 1)
    else:
        print("Not possible to open the file " + shapefile)
        sys.exit(1)

    return dataSource


def getNbFeat(shapefile, driver="ESRI Shapefile"):
    """
   Return the number of features of a shapefile
   """
    ds = openToRead(shapefile, driver)
    layer = ds.GetLayer()
    featureCount = layer.GetFeatureCount()
    return int(featureCount)


def getGeomType(shapefile, driver="ESRI Shapefile"):
    """
   Return the type of geometry of the file (WKBGeometryType)
   """
    ds = openToRead(shapefile, driver)
    layer = ds.GetLayer()
    return layer.GetGeomType()


def getGeomTypeFromFeat(shapefile, driver="ESRI Shapefile"):
    """
   Return the type of geometry of the file
   """

    # get the data layer
    ds = openToRead(shapefile, driver)
    layer = ds.GetLayer()

    # get the first feature
    feature = layer.GetNextFeature()

    geometry = feature.GetGeometryRef()

    return geometry.GetGeometryName()


def getFIDSpatialFilter(shapefile,
                        shapefileToIntesect,
                        field=None,
                        driver="ESRI Shapefile"):

    ds = openToRead(shapefile, driver)
    lyr = ds.GetLayer()
    dsinter = openToRead(shapefileToIntesect, driver)
    lyrinter = dsinter.GetLayer()

    lyr.SetSpatialFilterRect(lyrinter.GetExtent()[0],
                             lyrinter.GetExtent()[2],
                             lyrinter.GetExtent()[1],
                             lyrinter.GetExtent()[3])
    fidlist = []
    for feat in lyr:
        if field is None:
            fidlist.append(feat.GetFID())
        else:
            fidlist.append(feat.GetField(field))

    return fidlist


def spatialFilter(vect,
                  clipzone,
                  clipfield,
                  clipvalue,
                  outvect,
                  driverclip="ESRI Shapefile",
                  drivervect="ESRI Shapefile",
                  driverout="ESRI Shapefile"):
    """
   Return features of a vector file  which are intersected by a feature of another vector file
   """

    dsclip = openToRead(clipzone, driverclip)
    dsvect = openToRead(vect, drivervect)
    lyrclip = dsclip.GetLayer()
    lyrvect = dsvect.GetLayer()

    fields = getFields(clipzone, driverclip)
    layerDfnClip = lyrclip.GetLayerDefn()
    fieldTypeCode = layerDfnClip.GetFieldDefn(
        fields.index(clipfield)).GetType()

    if fieldTypeCode == 4:
        lyrclip.SetAttributeFilter(clipfield + " = \'" + str(clipvalue) + "\'")
    else:
        lyrclip.SetAttributeFilter(clipfield + " = " + str(clipvalue))

    featclip = lyrclip.GetNextFeature()
    geomclip = featclip.GetGeometryRef()
    lyrvect.SetSpatialFilter(geomclip)

    if lyrvect.GetFeatureCount() != 0:
        drv = ogr.GetDriverByName(driverout)
        outds = drv.CreateDataSource(outvect)
        layerNameOut = os.path.splitext(os.path.basename(outvect))[0]
        outlyr = outds.CopyLayer(lyrvect, layerNameOut)
        del outlyr, outds, lyrclip, lyrvect, dsvect, dsclip
    else:
        print("No intersection between the two vector files")
        del lyrclip, lyrvect, dsvect, dsclip


def getLayerName(shapefile, driver="ESRI Shapefile", layernb=0):
    """
    Return the name of the nth layer of a vector file
    """
    ds = openToRead(shapefile, driver)
    layer = ds.GetLayer(layernb)
    return layer.GetName()


# TODO : UNUSED
# def random_shp_points(shapefile, nbpoints, opath, driver="ESRI Shapefile"):
#     """
#     Takes an initial shapefile of points and randomly select an input nb of wanted points .
#     Returns the name of the ouput file
#     """
#     layer = openToWrite(shapefile)
#     FID = []
#     for f in layer:
#         FID.append(f.GetFID())
#     pointsToSelect = random.sample(FID, nbpoints)
#     expr = ""
#     for p in range(0, len(pointsToSelect) - 1):
#         expr = "FID = " + str(pointsToSelect[p]) + " OR " + expr
#     expr = expr + " FID = " + str(pointsToSelect[-1])
#     layer.SetAttributeFilter(expr)
#     outname = opath + "/" + str(nbpoints) + "points.shp"
#     CreateNewLayerPoint(layer, outname)
#     return outname


def intersect(f1, fid1, f2, fid2):
    """
   This function checks two features in a file to see if they intersect.
   It takes 4 arguments, f1 for the first file, fid1 for the index of the
   first file's feature, f2 for the second file, fid2 for the index of the
   second file's feature. Returns whether the intersection is True or False.
   """
    test = False
    ds1 = openToRead(f1)
    layer1 = ds1.GetLayer()
    feat1 = layer1.GetFeature(fid1)
    geom1 = feat1.GetGeometryRef()
    ds2 = openToRead(f2)
    layer2 = ds2.GetLayer()
    feat2 = layer2.GetFeature(fid2)
    geom2 = feat2.GetGeometryRef()
    if geom1.Intersect(geom2) == 1:
        print("INTERSECTION IS TRUE")
        test = True
    else:
        print("INTERSECTION IS FALSE")
        test = False
    return test


def getFields(shp, driver="ESRI Shapefile"):
    """
   Returns the list of fields of a vector file
   """
    if not isinstance(shp, osgeo.ogr.Layer):
        ds = openToRead(shp, driver)
        lyr = ds.GetLayer()
    else:
        lyr = shp

    inLayerDefn = lyr.GetLayerDefn()
    field_name_list = []
    for i in range(inLayerDefn.GetFieldCount()):
        field = inLayerDefn.GetFieldDefn(i).GetName()
        field_name_list.append(field)
    return field_name_list


# TODO: duplicate name with iota2.Common.Tools.ExtractROIRaster different outputs
def getFieldType(shp, field):

    ds = openToRead(shp)
    layer = ds.GetLayer()
    layerDefinition = layer.GetLayerDefn()
    dico = {"String": str, "Real": float, "Integer": int, "Integer64": int}
    for i in range(layerDefinition.GetFieldCount()):
        if layerDefinition.GetFieldDefn(i).GetName() == field:
            fieldTypeCode = layerDefinition.GetFieldDefn(i).GetType()
            fieldType = layerDefinition.GetFieldDefn(i).GetFieldTypeName(
                fieldTypeCode)

    return dico[fieldType]


def getFirstLayer(shp):

    ds = openToRead(shp)
    lyr = ds.GetLayer()
    layer = ds.GetLayerByIndex(0)
    layerName = layer.GetName()

    return layerName


def ListValueFields(shp, field):
    """
   Returns the list of fields of a shapefile
   """
    if not isinstance(shp, osgeo.ogr.Layer):
        ds = openToRead(shp)
        lyr = ds.GetLayer()
    else:
        lyr = shp

    values = []
    for feat in lyr:
        if not feat.GetField(field) in values:
            values.append(feat.GetField(field))

    return sorted(values)


def copyShapefile(shape, outshape):

    folderout = os.path.dirname(os.path.realpath(outshape))
    basefileout = os.path.splitext(os.path.basename(outshape))[0]
    folder = os.path.dirname(os.path.realpath(shape))
    basefile = os.path.splitext(os.path.basename(shape))[0]
    for root, dirs, files in os.walk(folder):
        for name in files:
            if os.path.splitext(name)[0] == basefile:
                copyfile(
                    folder + '/' + name, folderout + '/' + basefileout +
                    os.path.splitext(name)[1].lower())


def copyShp(shp, keyname):
    """
   Creates an empty new layer based on the properties and attributs of an input file
   """
    outShapefile = shp.split('.')[0] + '-' + keyname + '.shp'
    print(outShapefile)
    ds = openToRead(shp)
    layer = ds.GetLayer()
    inLayerDefn = layer.GetLayerDefn()
    field_name_target = getFields(shp)
    outDriver = ogr.GetDriverByName("ESRI Shapefile")
    #if file already exists, delete it
    if os.path.exists(outShapefile):
        outDriver.DeleteDataSource(outShapefile)
    outDataSource = outDriver.CreateDataSource(outShapefile)
    out_lyr_name = os.path.splitext(os.path.split(outShapefile)[1])[0]
    #Get the spatial reference of the input layer
    srsObj = layer.GetSpatialRef()
    #Creates the spatial reference of the output layer
    outLayer = outDataSource.CreateLayer(out_lyr_name,
                                         srsObj,
                                         geom_type=getGeomType(shp))
    # Add input Layer Fields to the output Layer if it is the one we want
    for i in range(0, inLayerDefn.GetFieldCount()):
        fieldDefn = inLayerDefn.GetFieldDefn(i)
        fieldName = fieldDefn.GetName()
        if fieldName not in field_name_target:
            continue
        outLayer.CreateField(fieldDefn)
    # Get the output Layer's Feature Definition
    outLayerDefn = outLayer.GetLayerDefn()
    #print layer.GetFeatureCount()
    print("New file created : %s" % (outShapefile))
    return outShapefile


def CreateNewLayer(layer: str,
                   outShapefile: str,
                   outformat: Optional[str] = "ESRI Shapefile"):
    """
    This function creates a new shapefile with a layer as input
    Parameters
    ----------
    layer : 
        the input layer
    outShapefile :
        the name of the output shapefile
    
      """
    inLayerDefn = layer.GetLayerDefn()
    field_name_target = []
    for i in range(inLayerDefn.GetFieldCount()):
        field = inLayerDefn.GetFieldDefn(i).GetName()
        field_name_target.append(field)

    outDriver = ogr.GetDriverByName(outformat)
    #if file already exists, delete it
    if os.path.exists(outShapefile):
        outDriver.DeleteDataSource(outShapefile)

    outDataSource = outDriver.CreateDataSource(outShapefile)
    out_lyr_name = os.path.splitext(os.path.split(outShapefile)[1])[0]
    #Get the spatial reference of the input layer
    srsObj = layer.GetSpatialRef()
    #Creates the spatial reference of the output layer
    outLayer = outDataSource.CreateLayer(out_lyr_name,
                                         srsObj,
                                         geom_type=layer.GetGeomType())
    # Add input Layer Fields to the output Layer if it is the one we want

    for i in range(0, inLayerDefn.GetFieldCount()):
        fieldDefn = inLayerDefn.GetFieldDefn(i)
        fieldName = fieldDefn.GetName()
        if fieldName not in field_name_target:
            continue
        outLayer.CreateField(fieldDefn)
    # Get the output Layer's Feature Definition
    outLayerDefn = outLayer.GetLayerDefn()

    # Add features to the ouput Layer
    for inFeature in layer:
        # Create output Feature
        outFeature = ogr.Feature(outLayerDefn)

        # Add field values from input Layer
        for i in range(0, outLayerDefn.GetFieldCount()):
            fieldDefn = outLayerDefn.GetFieldDefn(i)
            fieldName = fieldDefn.GetName()
            if fieldName not in field_name_target:
                continue

            outFeature.SetField(
                outLayerDefn.GetFieldDefn(i).GetNameRef(),
                inFeature.GetField(i))
        # Set geometry as centroid
        geom = inFeature.GetGeometryRef()
        outFeature.SetGeometry(geom.Clone())
        # Add new feature to output Layer
        outLayer.CreateFeature(outFeature)
    return outShapefile


def copyFeatInShp(inFeat, shp):
    """
   Copy a feature into a new file verifyng thad does not exist in the new file
   """
    ds = openToWrite(shp)
    layer = ds.GetLayer()
    outLayer_defn = layer.GetLayerDefn()
    inGeom = inFeat.GetGeometryRef()
    layer.SetSpatialFilter(inGeom)

    if layer.GetFeatureCount() == 0:
        layer.SetSpatialFilter(None)
        field_name_list = getFields(shp)
        outFeat = ogr.Feature(outLayer_defn)
        for field in field_name_list:
            inValue = inFeat.GetField(field)
            outFeat.SetField(field, inValue)
        geom = inFeat.GetGeometryRef()
        outFeat.SetGeometry(geom)
        layer.CreateFeature(outFeat)
        layer.SetFeature(outFeat)
        print("Feature copied")
        ds.ExecuteSQL('REPACK ' + layer.GetName())
    elif layer.GetFeatureCount() >= 1:
        layer2 = ds.GetLayer()
        layer2.SetSpatialFilter(inGeom)
        v = 0
        for k in layer2:
            geom2 = k.GetGeometryRef()
            if geom2:
                if inGeom.Equal(geom2) is True:
                    v += 1
                elif inGeom.Equal(geom2) is False:
                    v += 0
            if v == 0:
                field_name_list = getFields(shp)
                outFeat = ogr.Feature(outLayer_defn)
                for field in field_name_list:
                    inValue = inFeat.GetField(field)
                    outFeat.SetField(field, inValue)
                geom = inFeat.GetGeometryRef()
                outFeat.SetGeometry(geom)
                layer.CreateFeature(outFeat)
                layer.SetFeature(outFeat)
                print("Feature copied")
                ds.ExecuteSQL('REPACK ' + layer.GetName())


def copyFeatInShp2(inFeat, shp):
    """
   Copy a feature into a new file verifyng thad does not exist in the new file
   """
    ds = openToWrite(shp)
    layer = ds.GetLayer()
    outLayer_defn = layer.GetLayerDefn()
    inGeom = inFeat.GetGeometryRef()
    layer.SetSpatialFilter(inGeom)
    field_name_list = getFields(shp)
    outFeat = ogr.Feature(outLayer_defn)
    for field in field_name_list:
        inValue = inFeat.GetField(field)
        outFeat.SetField(field, inValue)
    geom = inFeat.GetGeometryRef()
    outFeat.SetGeometry(geom)
    layer.CreateFeature(outFeat)
    layer.SetFeature(outFeat)
    #print "Feature copied"
    ds.ExecuteSQL('REPACK ' + layer.GetName())


def deleteInvalidGeom(shp):
    """
   Delete the invalide geometries in a file.
   """
    print("Verifying geometries validity")
    ds = openToWrite(shp)
    layer = ds.GetLayer()
    nbfeat = getNbFeat(shp)
    count = 0
    corr = 0
    fidl = []
    #for i in range(0,nbfeat):
    for feat in layer:
        #feat = layer.GetFeature(i)
        fid = feat.GetFID()
        if feat.GetGeometryRef() is None:
            #print fid
            geom = feat.GetGeometryRef()
            if geom is None:
                fidl.append(fid)
            else:
                valid = geom.IsValid()
                ring = geom.IsRing()
                simple = geom.IsSimple()
                if valid == False:
                    fidl.append(fid)
    listFid = []
    if len(fidl) != 0:
        for f in fidl:
            listFid.append("FID!=" + str(f))
        chain = []
        for f in listFid:
            chain.append(f)
            chain.append(' AND ')
        chain.pop()
        fchain = ''.join(chain)
        layer.SetAttributeFilter(fchain)
        CreateNewLayer(layer, "valide_entities.shp")
        print("New file: valide_entities.shp was created")
    else:
        print("All geometries are valid. No file created")

    return 0


def checkValidGeom(shp,
                   outformat="ESRI shapefile",
                   display=True,
                   do_corrections=True):
    """Check the validity of geometries in a file. If geometry is not valid then
    apply buffer 0 to correct. Works for files with polygons

    Parameters
    ----------
    shp : string
        input shapeFile
    Return
    ------
    tuple
        (output_shape, count, corr) where count is the number of invalid features
        and corr the number of invalid features corrected
    """
    if display:
        print("Verifying geometries validity")

    ds = openToWrite(shp, outformat)
    layer = ds.GetLayer()
    nbfeat = getNbFeat(shp, outformat)
    count = 0
    corr = 0
    fidl = []

    for feat in layer:
        #feat = layer.GetFeature(i)
        fid = feat.GetFID()
        if feat.GetGeometryRef() is None:
            if display:
                print(fid)
            if not do_corrections:
                continue
            count += 1
            layer.DeleteFeature(fid)
            ds.ExecuteSQL('REPACK ' + layer.GetName())
            layer.ResetReading()
        else:
            geom = feat.GetGeometryRef()
            valid = geom.IsValid()
            ring = geom.IsRing()
            simple = geom.IsSimple()
            if valid == False and do_corrections:
                fidl.append(fid)
                buffer_test = feat.SetGeometry(geom.Buffer(0))
                layer.SetFeature(feat)
                if buffer_test == 0:
                    if display:
                        print("Feature %d has been corrected" % feat.GetFID())
                    corr += 1
                else:
                    if display:
                        print("Feature %d could not be corrected" %
                              feat.GetFID())
                count += 1
    if display:
        print("From %d invalid features, %d were corrected" % (count, corr))

    if outformat == "ESRI shapefile":
        ds.ExecuteSQL('REPACK %s' % (layer.GetName()))
    elif outformat == "SQlite":
        ds = layer = None
    else:
        pass

    return shp, count, corr


def checkEmptyGeom(shp,
                   informat="ESRI shapefile",
                   do_corrections=True,
                   output_file=None):
    """Check if a geometry is empty, then if it does it will not be copied in output shapeFile

    Parameters
    ----------
    input_shape : string
        input shapeFile
    do_correction : bool
        flag to remove empty geometries
    output_shape : string
        output shapeFile, if set to None output_shape = input_shape
    Return
    ------
    tuple
        (output_shape, invalid_geom_number) where invalid_geom_number is the number of empty geometries
    """
    ds = openToRead(shp, informat)
    layer = ds.GetLayer()
    allFID = []
    count = 0
    for feat in layer:
        fid = feat.GetFID()
        geom = feat.GetGeometryRef()
        if geom is not None:
            empty = geom.IsEmpty()
            if empty is False:
                allFID.append(fid)
            elif empty is True:
                count += 1
    if do_corrections:
        if count == 0:
            print("No empty geometries")
            outShapefile = shp
        else:
            for fid in allFID:
                allFID.append("FID=" + str(fid))
            #Add the word OR
            allList = []
            for item in allFID:
                allList.append(item)
                allList.append(' OR ')
            allList.pop()

            ch = ' '.join(allList)
            layer.SetAttributeFilter(ch)
            outShapefile = output_file if output_file is not None else shp.split(
                '.')[0] + "-NoEmpty.shp"
            CreateNewLayer(layer, outShapefile)
            print("%d empty geometries were deleted") % (len(allFID))
    return (output_file, count)


def checkIsRingGeom(shp):
    """
   Check if all the geometries in the shapefile are closed rings
   """
    ds = openToWrite(shp)
    layer = ds.GetLayer()
    nbfeat = getNbFeat(shp)


def explain_validity(shp):

    from shapely.wkt import loads
    from shapely.geos import lgeos
    """
   Explains the validity reason of each feature in a shapefile
   """
    ds = openToRead(shp)
    layer = ds.GetLayer()
    nbfeat = getNbFeat(shp)
    for feat in layer:
        geom = feat.GetGeometryRef()
        ob = loads(geom.ExportToWkt())
        print(lgeos.GEOSisValidReason(ob._geom))
    return 0


def checkIntersect(shp, distance, fieldin):
    """
   Check if each feature intersects another feature in the same file. If True compute the difference. The common part is deleted
   """
    print("Verifying intersection")
    ds = openToWrite(shp)
    layer = ds.GetLayer()
    nbfeat = getNbFeat(shp)
    outShp = copyShp(shp, 'nointersct')
    layerDef = layer.GetLayerDefn()
    fields = getFields(shp)
    for i in range(0, nbfeat):
        print(i)
        feat1 = layer.GetFeature(i)
        geom1 = feat1.GetGeometryRef()
        centroid = geom1.Centroid()
        x = centroid.GetX()
        y = centroid.GetY()
        minX = x - float(distance)
        minY = y - float(distance)
        maxX = x + float(distance)
        maxY = y + float(distance)
        layer.SetSpatialFilterRect(float(minX), float(minY), float(maxX),
                                   float(maxY))
        nbfeat2 = layer.GetFeatureCount()
        intersection = False
        listFID = []
        listID = []
        for j in range(0, nbfeat2):
            feat2 = layer.GetFeature(j)
            #print feat1.GetFID()
            #print feat2.GetFID()
            geom1 = feat1.GetGeometryRef()
            geom2 = feat2.GetGeometryRef()
            if geom1.Intersects(geom2) == True and not geom1.Equal(geom2):
                listFID.append(feat2.GetFID())
                listID.append(feat2.GetField('id'))

        if len(listFID) == 0:
            outds = openToRead(outShp)
            outlayer = outds.GetLayer()
            if VerifyGeom(geom1, outlayer) == False:
                copyFeatInShp(feat1, outShp)
        elif len(listFID) == 1:
            for f in listFID:
                feat2 = layer.GetFeature(f)
                geom2 = feat2.GetGeometryRef()
                if feat1.GetFieldAsString(fieldin) == feat2.GetFieldAsString(
                        fieldin):
                    print(
                        feat1.GetFieldAsString('id') + " " +
                        feat2.GetFieldAsString('id'))
                    newgeom = union(geom1, geom2)
                else:
                    newgeom = Difference(geom1, geom2)
                #newgeom =  Difference(geom1, geom2)
                newgeom2 = ogr.CreateGeometryFromWkb(newgeom.wkb)
                newFeature = ogr.Feature(layerDef)
                newFeature.SetGeometry(newgeom2)
                for field in fields:
                    newFeature.SetField(field, feat1.GetField(field))
                copyFeatInShp(newFeature, outShp)
        elif len(listFID) > 1:
            for f in listFID:
                feat2 = layer.GetFeature(f)
                geom2 = feat2.GetGeometryRef()
                if feat1.GetFieldAsString(fieldin) == feat2.GetFieldAsString(
                        fieldin):
                    newgeom = union(geom1, geom2)
                else:
                    newgeom = Difference(geom1, geom2)
                #newgeom =  Difference(geom1, geom2)
                newgeom2 = ogr.CreateGeometryFromWkb(newgeom.wkb)
                newFeature = ogr.Feature(layerDef)
                newFeature.SetGeometry(newgeom2)
                geom1 = newFeature.GetGeometryRef()
                for field in fields:
                    newFeature.SetField(field, feat1.GetField(field))
                copyFeatInShp(newFeature, outShp)

    return outShp


def checkIntersect2(shp, fieldin, fieldinID):
    """
   Check if each feature intersects another feature in the same file. If True compute the difference. The common part is deleted
   """
    print("Verifying intersection")
    ds = openToWrite(shp)
    layer = ds.GetLayer()
    nbfeat = getNbFeat(shp)
    outShp = copyShp(shp, 'nointersct')
    layerDef = layer.GetLayerDefn()
    fields = getFields(shp)
    for i in range(0, nbfeat):
        print(i)
        feat1 = layer.GetFeature(i)
        geom1 = feat1.GetGeometryRef()
        centroid = geom1.Centroid()
        layer.SetSpatialFilter(geom1)
        nbfeat2 = layer.GetFeatureCount()
        intersection = False
        listFID = []
        listID = []
        for j in range(0, nbfeat2):
            feat2 = layer.GetFeature(j)
            #print feat1.GetFID()
            #print feat2.GetFID()
            geom1 = feat1.GetGeometryRef()
            geom2 = feat2.GetGeometryRef()
            if geom1.Intersects(geom2) == True and not geom1.Equal(geom2):
                listFID.append(feat2.GetFID())
                listID.append(feat2.GetField(fieldinID))
        if len(listFID) == 0:
            outds = openToRead(outShp)
            outlayer = outds.GetLayer()
            if VerifyGeom(geom1, outlayer) is False:
                copyFeatInShp2(feat1, outShp)
        elif len(listFID) == 1:
            for f in listFID:
                feat2 = layer.GetFeature(f)
                geom2 = feat2.GetGeometryRef()
                if feat1.GetFieldAsString(fieldin) == feat2.GetFieldAsString(
                        fieldin):
                    print(
                        feat1.GetFieldAsString(fieldinID) + " " +
                        feat2.GetFieldAsString(fieldinID))
                    newgeom = union(geom1, geom2)
                else:
                    newgeom = Difference(geom1, geom2)
                #newgeom =  Difference(geom1, geom2)
                newgeom2 = ogr.CreateGeometryFromWkb(newgeom.wkb)
                newFeature = ogr.Feature(layerDef)
                newFeature.SetGeometry(newgeom2)
                for field in fields:
                    newFeature.SetField(field, feat1.GetField(field))
                copyFeatInShp2(newFeature, outShp)
        elif len(listFID) > 1:
            for f in listFID:
                feat2 = layer.GetFeature(f)
                geom2 = feat2.GetGeometryRef()
                if feat1.GetFieldAsString(fieldin) == feat2.GetFieldAsString(
                        fieldin):
                    newgeom = union(geom1, geom2)
                else:
                    newgeom = Difference(geom1, geom2)
                #newgeom =  Difference(geom1, geom2)
                newgeom2 = ogr.CreateGeometryFromWkb(newgeom.wkb)
                newFeature = ogr.Feature(layerDef)
                newFeature.SetGeometry(newgeom2)
                geom1 = newFeature.GetGeometryRef()
                for field in fields:
                    newFeature.SetField(field, feat1.GetField(field))
                copyFeatInShp2(newFeature, outShp)

    return outShp


def checkIntersect3(shp, fieldin, fieldinID):
    """
   Check if each feature intersects another feature in the same file. If True compute the difference. The common part is deleted
   """
    print("Verifying intersection")
    ds = openToWrite(shp)
    layer = ds.GetLayer()
    nbfeat = getNbFeat(shp)
    layerDef = layer.GetLayerDefn()
    fields = getFields(shp)
    listFID = []
    for i in range(0, nbfeat):
        feat1 = layer.GetFeature(i)
        geom1 = feat1.GetGeometryRef()
        centroid = geom1.Centroid()
        layer2 = ds.GetLayer()
        layer2.SetSpatialFilter(None)
        layer2.SetSpatialFilter(geom1)
        nbfeat2 = layer2.GetFeatureCount()
        intersection = False
        listID = []
        for feat2 in layer2:
            geom2 = feat2.GetGeometryRef()
            if geom1.Intersects(geom2) == True and not geom1.Equal(geom2):
                listFID.append(feat2.GetFID())
                if feat1.GetFieldAsString(fieldin) == feat2.GetFieldAsString(
                        fieldin):
                    newgeom = union(geom1, geom2)
                    newgeom2 = ogr.CreateGeometryFromWkb(newgeom.wkb)
                    newFeature = ogr.Feature(layerDef)
                    newFeature.SetGeometry(newgeom2)
                    geom1 = newFeature.GetGeometryRef()
                    for field in fields:
                        newFeature.SetField(field, feat1.GetField(field))
                    layer.CreateFeature(newFeature)
                    layer.SetFeature(newFeature)
                else:
                    newgeom = Difference(geom1, geom2)
                    newgeom2 = ogr.CreateGeometryFromWkb(newgeom.wkb)
                    newFeature = ogr.Feature(layerDef)
                    newFeature.SetGeometry(newgeom2)
                    geom1 = newFeature.GetGeometryRef()
                    for field in fields:
                        newFeature.SetField(field, feat1.GetField(field))
                    layer.CreateFeature(newFeature)
                    layer.SetFeature(newFeature)

    for fid in range(0, len(listFID)):
        layer.DeleteFeature(listFID[fid])

    ds.ExecuteSQL('REPACK ' + layer.GetName())


def VerifyGeom(geom, layer):
    verif = False
    for feat in layer:
        geom2 = feat.GetGeometryRef()
        if geom and geom2:
            if geom.Equal(geom2):
                verif = True

    return verif


def Difference(geom1, geom2):

    from shapely.wkt import loads
    """
   Returns the difference of 2 geometries
   """
    obj1 = loads(geom1.ExportToWkt())
    obj2 = loads(geom2.ExportToWkt())
    return obj1.difference(obj2)


def union(geom1, geom2):

    from shapely.wkt import loads
    """
   Returns the difference of 2 geometries
   """
    obj1 = loads(geom1.ExportToWkt())
    obj2 = loads(geom2.ExportToWkt())
    print(obj1.union(obj2))
    return obj1.union(obj2)


def CheckDoubleGeomTwofiles(shp1, shp2):
    """Priority to file No. 1 """
    distance = 5000
    ds1 = openToRead(shp1)
    lyr1 = ds1.GetLayer()
    for feat1 in lyr1:
        if feat1.GetGeometryRef():
            geom1 = feat1.GetGeometryRef()
            centroid = geom1.Centroid()
            x = centroid.GetX()
            y = centroid.GetY()
            minX = x - float(distance)
            minY = y - float(distance)
            maxX = x + float(distance)
            maxY = y + float(distance)
            ds2 = openToWrite(shp2)
            lyr2 = ds2.GetLayer()
            lyr2.SetSpatialFilter(geom1)
            if lyr2.GetFeatureCount() == 0:
                lyr1.GetNextFeature()
            else:
                print(lyr2.GetFeatureCount())
                for feat2 in lyr2:
                    fid = feat2.GetFID()
                    geom2 = feat2.GetGeometryRef()
                    if geom1.Equal(geom2) == True:
                        lyr2.DeleteFeature(fid)
                        ds2.ExecuteSQL('REPACK ' + lyr2.GetName())
            lyr2.ResetReading()


# TODO: Duplicate with KeepCommonFeature
# Function not working variable field in parameter and field1 in code
def CheckDoubleGeomTwofilesCopy(shp1, shp2, field):
    """Priority to file No. 1 """

    ds1 = openToRead(shp1)
    lyr1 = ds1.GetLayer()
    ds2 = openToRead(shp2)
    lyr2 = ds2.GetLayer()
    newshp = copyShp(shp1, "commonshape")
    dict1 = dict()
    dict2 = dict()
    dict3 = dict()
    for feat in lyr1:
        values = []
        ge = feat.GetGeometryRef()
        f = feat.GetFID()
        code = feat.GetField(field1)
        values.append(ge.ExportToWkt())
        values.append(code)
        dict1[f] = values
    for feat in lyr2:
        values = []
        ge = feat.GetGeometryRef()
        f = feat.GetFID()
        code = feat.GetField(field1)
        values.append(ge.ExportToWkt())
        values.append(code)
        dict2[f] = values
    for k1, v1 in dict1:
        for k2, v2 in dict2:
            new_feat = lyr1.GetFeat(k1)
            copyFeatInShp2(new_feat, newshp)


# TODO: unused
# def getFieldNames(shp_in):
#     """
#     Get list of field names
#     ARGs:
#     INPUT:
#     - shp_in: input shapefile
#     OUTPUT:
#     - list_field_names: list of field names
#     (Other solution(?):
#     ogrinfo -so shp_in short_shp_in
#     """
#     list_field_names = []
#     driver = ogr.GetDriverByName("ESRI Shapefile")
#     dataSource = driver.Open(shp_in, 0)
#     layer = dataSource.GetLayer()
#     layerDefn = layer.GetLayerDefn()
#     for i in range(layerDefn.GetFieldCount()):
#         list_field_names.append(layerDefn.GetFieldDefn(i).GetName())
#     return list_field_names


def multiPolyToPoly(shpMulti, shpSingle):
    """
    IN:
    shpMulti [string] : path to an input vector
    shpSingle [string] : output vector

    OUT:
    convert all multipolygon to polygons. Add all single polygon into shpSingle
    """
    def addPolygon(feat, simplePolygon, in_lyr, out_lyr):
        """
        add polygon
        """
        featureDefn = in_lyr.GetLayerDefn()
        polygon = ogr.CreateGeometryFromWkb(simplePolygon)
        out_feat = ogr.Feature(featureDefn)
        for field in field_name_list:
            inValue = feat.GetField(field)
            out_feat.SetField(field, inValue)
        out_feat.SetGeometry(polygon)
        out_lyr.CreateFeature(out_feat)
        out_lyr.SetFeature(out_feat)

    def multipoly2poly(in_lyr, out_lyr):
        for in_feat in in_lyr:
            geom = in_feat.GetGeometryRef()
            if geom.GetGeometryName() == 'MULTIPOLYGON':
                for geom_part in geom:
                    addPolygon(in_feat, geom_part.ExportToWkb(), in_lyr,
                               out_lyr)
            else:
                addPolygon(in_feat, geom.ExportToWkb(), in_lyr, out_lyr)

    gdal.UseExceptions()
    driver = ogr.GetDriverByName('ESRI Shapefile')
    field_name_list = get_all_fields_in_shape(shpMulti)
    in_ds = driver.Open(shpMulti, 0)
    in_lyr = in_ds.GetLayer()
    inLayerDefn = in_lyr.GetLayerDefn()
    srsObj = in_lyr.GetSpatialRef()
    if os.path.exists(shpSingle):
        driver.DeleteDataSource(shpSingle)
    out_ds = driver.CreateDataSource(shpSingle)
    out_lyr = out_ds.CreateLayer('poly', srsObj, geom_type=ogr.wkbPolygon)
    for i in range(0, len(field_name_list)):
        fieldDefn = inLayerDefn.GetFieldDefn(i)
        fieldName = fieldDefn.GetName()
        if fieldName not in field_name_list:
            continue
        out_lyr.CreateField(fieldDefn)
    multipoly2poly(in_lyr, out_lyr)


def mergeSQLite(outname, opath, files):
    filefusion = opath + "/" + outname + ".sqlite"
    if os.path.exists(filefusion):
        os.remove(filefusion)
    if len(files) > 1:
        first = files[0]
        cmd = 'ogr2ogr -f SQLite ' + filefusion + ' ' + first
        run(cmd)
        if len(files) > 1:
            for f in range(1, len(files)):
                fusion = ('ogr2ogr -f SQLite -update -append ' + filefusion +
                          ' ' + files[f])
                print(fusion)
                run(fusion)
    else:
        shutil.copy(files[0], filefusion)


def getFieldElement(shape: str,
                    driverName: Optional[str] = "ESRI Shapefile",
                    field: Optional[str] = "CODE",
                    mode: Optional[str] = "all",
                    elemType: Optional[str] = "int"):
    """
    PARAMETERS
    ----------
    shape [string] : shape to compute
    driverName [string] : ogr driver to read the shape
    field [string] : data's field
    mode [string] : "all" or "unique"

    OUT :
    [list] containing all/unique element in shape's field

    Example :
        getFieldElement("./MyShape.sqlite","SQLite","CODE",mode = "all")
        >> [1,2,2,2,2,3,4]
        getFieldElement("./MyShape.sqlite","SQLite","CODE",mode = "unique")
        >> [1,2,3,4]
    """
    def getElem(elem, elemType):

        retourElem = None
        if elemType == "int":
            retourElem = int(elem)
        elif elemType == "str":
            retourElem = str(elem)
        else:
            raise Exception("elemType must be 'int' or 'str'")
        return retourElem

    driver = ogr.GetDriverByName(driverName)

    if driverName.lower() == "sqlite":
        max_try = 6
        for _ in range(max_try):
            try:
                dataSource = driver.Open(shape, 0)
                layer = dataSource.GetLayer()
                break
            except:
                print(f"failed to open database {shape}, waiting...")
                time.sleep(random.randint(1, 10))
    else:
        dataSource = driver.Open(shape, 0)
        layer = dataSource.GetLayer()

    retourMode = None
    if mode == "all":
        retourMode = [
            getElem(currentFeat.GetField(field), elemType)
            for currentFeat in layer
        ]
    elif mode == "unique":
        retourMode = list(
            set([
                getElem(currentFeat.GetField(field), elemType)
                for currentFeat in layer
            ]))
    else:
        raise Exception("mode parameter must be 'all' or 'unique'")
    return retourMode


def getVectorFeatures(ground_truth: str, region_field: str,
                      InputShape: str) -> List[str]:
    """
    PARAMETERS
    ----------
    ground_truth :
        original ground truth file
    region_field :
        the region name
    InputShape :
        path to a vector (otbcli_SampleExtraction output)

    Notes
    -----
    AllFeat : [lsit of string] : list of all feature fought in InputShape. This vector must
    contains field with pattern 'value_N' N:[0,int(someInt)]
    """
    input_fields = get_all_fields_in_shape(ground_truth) + [
        region_field, "originfid", "tile_o"
    ]

    dataSource = ogr.Open(InputShape)
    daLayer = dataSource.GetLayer(0)
    layerDefinition = daLayer.GetLayerDefn()

    AllFeat = []
    for i in range(layerDefinition.GetFieldCount()):
        field_name = layerDefinition.GetFieldDefn(i).GetName()
        if field_name not in input_fields and field_name not in [
                input_field.lower() for input_field in input_fields
        ]:
            AllFeat.append(layerDefinition.GetFieldDefn(i).GetName())
    return AllFeat


def get_all_fields_in_shape(vector, driver='ESRI Shapefile'):
    """
    Return all fields in vector

    Parameters
    ----------
    vector :
        path to vector file
    driver :
        gdal driver

    
    """
    driver = ogr.GetDriverByName(driver)
    data_source = driver.Open(vector, 0)
    if data_source is None:
        raise Exception("Could not open " + vector)
    layer = data_source.GetLayer()
    layer_definition = layer.GetLayerDefn()
    return [
        layer_definition.GetFieldDefn(i).GetName()
        for i in range(layer_definition.GetFieldCount())
    ]


def keepBiggestArea(shpin, shpout):
    """
    usage : from shpin, keep biggest polygon and save it in shpout
    logger = logging.getLogger("distributed.worker")
    logger.debug("Processing {}".format(shpin))
    """
    def addPolygon(feat, simplePolygon, in_lyr, out_lyr):
        """
        usage : add polygon
        """
        featureDefn = in_lyr.GetLayerDefn()
        polygon = ogr.CreateGeometryFromWkb(simplePolygon)
        out_feat = ogr.Feature(featureDefn)
        for field in field_name_list:
            inValue = feat.GetField(field)
            out_feat.SetField(field, inValue)
        out_feat.SetGeometry(polygon)
        out_lyr.CreateFeature(out_feat)
        out_lyr.SetFeature(out_feat)

    gdal.UseExceptions()
    driver = ogr.GetDriverByName('ESRI Shapefile')
    field_name_list = get_all_fields_in_shape(shpin)
    in_ds = driver.Open(shpin, 0)
    in_lyr = in_ds.GetLayer()
    inLayerDefn = in_lyr.GetLayerDefn()
    srsObj = in_lyr.GetSpatialRef()
    if os.path.exists(shpout):
        driver.DeleteDataSource(shpout)
    out_ds = driver.CreateDataSource(shpout)
    out_lyr = out_ds.CreateLayer('poly', srsObj, geom_type=ogr.wkbPolygon)
    for i in range(0, len(field_name_list)):
        fieldDefn = inLayerDefn.GetFieldDefn(i)
        fieldName = fieldDefn.GetName()
        if fieldName not in field_name_list:
            continue
        out_lyr.CreateField(fieldDefn)

    area = []
    allGeom = []
    for in_feat in in_lyr:
        geom = in_feat.GetGeometryRef()
        area.append(geom.GetArea())
        allGeom.append(geom.ExportToWkb())

    indexMax = int(np.argmax(np.array(area)))
    addPolygon(in_lyr[indexMax], allGeom[indexMax], in_lyr, out_lyr)


def erodeOrDilateShapeFile(infile: str, outfile: str, buffdist: float) -> bool:
    """
    dilate or erode all features in the shapeFile In
    Return True if succeed
    Parameters
    ----------
    infile :
        the shape file
        ex : /xxx/x/x/x/x/yyy.shp
    outfile :
        the resulting shapefile
        ex : /x/x/x/x/x.shp
    buffdist :
        the distance of dilatation or erosion
        ex : -10 for erosion
             +10 for dilatation

    OUT :
    - the shapeFile outfile
    """

    retour = True
    try:
        ds = ogr.Open(infile)
        drv = ds.GetDriver()
        if os.path.exists(outfile):
            drv.DeleteDataSource(outfile)
        drv.CopyDataSource(ds, outfile)
        ds.Destroy()

        ds = ogr.Open(outfile, 1)
        lyr = ds.GetLayer(0)
        for i in range(0, lyr.GetFeatureCount()):
            feat = lyr.GetFeature(i)
            lyr.DeleteFeature(i)
            geom = feat.GetGeometryRef()
            feat.SetGeometry(geom.Buffer(float(buffdist)))
            lyr.CreateFeature(feat)
        ds.Destroy()
    except:
        retour = False
    return retour


def erodeShapeFile(infile, outfile, buffdist):
    return erodeOrDilateShapeFile(infile, outfile, -math.fabs(buffdist))


# TODO: duplicate name with iota2.Sampling.TileEnvelope, outputs are diferent
def getShapeExtent(shape_in):
    """
        Get shape extent of shape_in. The shape must have only one geometry
    """

    driver = ogr.GetDriverByName("ESRI Shapefile")
    dataSource = driver.Open(shape_in, 0)
    layer = dataSource.GetLayer()

    for feat in layer:
        geom = feat.GetGeometryRef()
    env = geom.GetEnvelope()
    return env[0], env[2], env[1], env[3]


def removeShape(shapePath, extensions):
    """
    IN:
        shapePath : path to the shapeFile without extension.
            ex : /path/to/myShape where /path/to/myShape.* exists
        extensions : all extensions to delete
            ex : extensions = [".prj",".shp",".dbf",".shx"]
    """
    for ext in extensions:
        if os.path.exists(shapePath + ext):
            os.remove(shapePath + ext)


def cpShapeFile(inpath, outpath, extensions, spe=False):

    for ext in extensions:
        if not spe:
            shutil.copy(inpath + ext, outpath + ext)
        else:
            shutil.copy(inpath + ext, outpath)


def renameShapefile(inpath, filename, old_suffix, new_suffix, outpath=None):
    if not outpath:
        outpath = inpath
    run("cp " + inpath + "/" + filename + old_suffix + ".shp " + outpath +
        "/" + filename + new_suffix + ".shp")
    run("cp " + inpath + "/" + filename + old_suffix + ".shx " + outpath +
        "/" + filename + new_suffix + ".shx")
    run("cp " + inpath + "/" + filename + old_suffix + ".dbf " + outpath +
        "/" + filename + new_suffix + ".dbf")
    run("cp " + inpath + "/" + filename + old_suffix + ".prj " + outpath +
        "/" + filename + new_suffix + ".prj")
    return outpath + "/" + filename + new_suffix + ".shp"


def ClipVectorData(vectorFile, cutFile, opath, nameOut=None):
    """
    Cuts a shapefile with another shapefile
    ARGs:
       INPUT:
            -vectorFile: the shapefile to be cut
            -shpMask: the other shapefile
       OUTPUT:
            -the vector file clipped
    """
    if not nameOut:
        nameVF = vectorFile.split("/")[-1].split(".")[0]
        nameCF = cutFile.split("/")[-1].split(".")[0]
        outname = opath + "/" + nameVF + "_" + nameCF + ".shp"
    else:
        outname = opath + "/" + nameOut + ".shp"

    if os.path.exists(outname):
        os.remove(outname)

    Clip = ("ogr2ogr -clipsrc " + cutFile + " " + outname + " " + vectorFile +
            " -progress")
    run(Clip)
    return outname


valid = False
empty = False
intersect = False
explain = False
delete = False
none = False

if __name__ == "__main__":
    if len(sys.argv) == 1:

        prog = os.path.basename(sys.argv[0])
        print('      ' + sys.argv[0] + ' [options]')
        print("     Help : ", prog, " --help")
        print("        or : ", prog, " -h")
        sys.exit(-1)
    else:
        usage = "usage: %prog [options] "
        parser = argparse.ArgumentParser(
            description="Verify shapefile geometries."
            "You have to choose only one option")
        parser.add_argument("-s",
                            dest="shapefile",
                            action="store",
                            help="Input shapefile",
                            required=True)
        parser.add_argument(
            "-v",
            action='store_true',
            help="Check the validity of geometries of input file."
            "If geometry is not valid then buffer 0 to correct",
            default=False)
        parser.add_argument(
            "-e",
            action='store_true',
            help=
            "Check if a geometry is empty and create a new file with no empty geometries",
            default=False)
        parser.add_argument(
            "-i",
            action='store_true',
            help=
            "Check if each feature intersects another feature in the same file."
            "If True compute the difference. The common part is deleted",
            default=False)
        parser.add_argument(
            "-ev",
            action='store_true',
            help="Explains the validity reason of each feature in a shapefile",
            default=False)
        parser.add_argument("-d",
                            action='store_true',
                            help="Delete the invalide geometries in a file",
                            default=False)

        args = parser.parse_args()

        if os.path.splitext(args.shapefile)[1] == ".shp":
            outformat = "ESRI shapefile"
        elif os.path.splitext(args.shapefile)[1] == ".sqlite":
            outformat = "SQlite"
        else:
            print("Output format not managed")
            sys.exit()

        if args.v or args.e or args.i or args.ev or args.d:
            if args.v:
                valid = True
            if args.e:
                empty = True
            if args.i:
                intersect = True
            if args.ev:
                explain = True
            if args.d:
                delete = True
        else:
            none = True

        if valid:
            checkValidGeom(args.shapefile, outformat)

        if empty:
            checkEmptyGeom(args.shapefile, outformat)

        if intersect:
            checkIntersect3(args.shapefile, 'CODE', 'ID')

        if explain:
            explain_validity(args.shapefile)

        if delete:
            deleteInvalidGeom(args.shapefile)

        if none:
            prog = os.path.basename(sys.argv[0])
            print('      ' + sys.argv[0] + ' [options]')
            print("     Help : ", prog, " --help")
            print("        or : ", prog, " -h")
            sys.exit(-1)
