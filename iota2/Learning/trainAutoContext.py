#!/usr/bin/env python3
#-*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================

import os
import logging
from typing import List, Dict, Union, Optional

LOGGER = logging.getLogger("distributed.worker")
LOGGER.addHandler(logging.NullHandler())

SENSORS_PARAMS = Dict[str, Union[str, List[str], int]]
Param = Dict[str, Union[str, List[str], int]]


def train_autoContext(parameter_dict: Param,
                      data_field: str,
                      output_path: str,
                      features_list_name: List[str],
                      superpix_data_field: Optional[str] = "superpix",
                      iterations: Optional[int] = 3,
                      RAM: Optional[int] = 128,
                      WORKING_DIR: Optional[str] = None,
                      logger: Optional[logging.Logger] = LOGGER):
    """launch autoContext training

    Parameters
    ----------
    parameter_dict : dict
        dictionnary containing autoContext's input parameters
        {"model_name": string,
         "seed": integer,
         "list_learning_samples": list,
         "list_tiles": list,
         "list_slic": list,
         "list_superPixel_samples": list}
    output_path : sting
        path to a directory where will be stored models
        in a directory 'model'
    data_field : string
        class field name in learning database samples files
    superpix_data_field : string
        field in database discriminating superpixels labels
    iterations : int
        number of auto-context iterations
    RAM : integer
        available ram
    WORKING_DIR : string
        path to store temporary data
    logger : logging
        root logger
    """
    import shutil
    from iota2.Common.OtbAppBank import CreateTrainAutoContext
    from iota2.Common.FileUtils import ensure_dir

    model_name = parameter_dict["model_name"]
    seed_num = parameter_dict["seed"]
    data_ref = parameter_dict["list_learning_samples"]
    data_segmented = parameter_dict["list_superPixel_samples"]

    field = data_field.lower()

    models_path = os.path.join(output_path, "model")
    model_path = os.path.join(models_path,
                              "model_{}_seed_{}".format(model_name, seed_num))
    ensure_dir(model_path)

    if WORKING_DIR is None:
        tmp_dir = os.path.join(model_path, "tmp")
    else:
        tmp_dir = os.path.join(
            WORKING_DIR, "model_{}_seed_{}_tmp".format(model_name, seed_num))
    ensure_dir(tmp_dir)

    feat_labels = [elem.lower() for elem in features_list_name]

    train_autoContext_app = CreateTrainAutoContext({
        "refdata":
        data_ref,
        "reffield":
        field,
        "superpixdata":
        data_segmented,
        "superpixdatafield":
        superpix_data_field,
        "feat":
        feat_labels,
        "nit":
        iterations,
        "out":
        "{}/".format(model_path)
    })
    logger.info("Start training autoContext, produce model {}, seed {}".format(
        model_name, seed_num))
    train_autoContext_app.ExecuteAndWriteOutput()
    logger.info("training autoContext DONE")
    shutil.rmtree(tmp_dir)
