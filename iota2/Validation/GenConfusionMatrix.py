#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
""" Generate Confusion Matrix"""
import os
import ogr
import logging
import argparse
from logging import Logger
from typing import List, Tuple, Optional, Dict

from iota2.Common import OtbAppBank
from iota2.Common import FileUtils as fu
from iota2.VectorTools import vector_functions as vf

LOGGER = logging.getLogger("distributed.worker")


def gen_conf_matrix(in_classif: str,
                    out_csv: str,
                    data_field: str,
                    ref_vector: str,
                    ram: float,
                    labels_table: Dict,
                    logger=LOGGER) -> None:
    """generate an otb confusion matrix

    Parameters
    ----------
    in_classif : string
        input classification file
    out_csv : string
        output csv file
    data_field : string
        data field in the ground truth database
    ref_vector : string
        reference vector file (containing polygons)
    ram : float
        available ram in Mo
    labels_table:
        dictionary to convert labels labels_table[old_label] = new_label
    """
    if os.path.exists(ref_vector):
        all_castable = []
        for _, user_label in labels_table.items():
            try:
                __ = int(user_label)
                all_castable.append(True)
            except ValueError:
                all_castable.append(False)
        re_encode_labels = all(all_castable)
        if re_encode_labels:
            user_label_data_field = "reencodedlabels"
            vf.re_encode_field(ref_vector,
                               data_field.lower(),
                               user_label_data_field,
                               ogr.OFTInteger,
                               100,
                               labels_table,
                               logger=logger)
            data_field = user_label_data_field

        confusion_app = OtbAppBank.CreateConfusionMatrix({
            "in":
            in_classif,
            "out":
            out_csv,
            "ref":
            "vector",
            "ref.vector.field":
            data_field.lower(),
            "ref.vector.in":
            ref_vector,
            "ram":
            ram
        })
        confusion_app.ExecuteAndWriteOutput()
    else:
        logger.warning(
            f"{ref_vector} does not exists, maybe there is no validation samples available in the tile"
        )


def confusion_sar_optical_parameter(iota2_dir: str,
                                    logger: Optional[Logger] = LOGGER):
    """
    return a list of tuple containing the classification and the associated
    shapeFile to compute a confusion matrix
    """
    ref_vectors_dir = os.path.join(iota2_dir, "dataAppVal", "bymodels")
    classifications_dir = os.path.join(iota2_dir, "classif")

    vector_seed_pos = 4
    vector_tile_pos = 0
    vector_model_pos = 2
    classif_seed_pos = 5
    classif_tile_pos = 1
    classif_model_pos = 3

    vectors = fu.FileSearch_AND(ref_vectors_dir, True, ".shp")
    classifications = fu.FileSearch_AND(classifications_dir, True, "Classif",
                                        "model", "seed", ".tif")

    group = []
    for vector in vectors:
        vec_name = os.path.basename(vector)
        seed = vec_name.split("_")[vector_seed_pos]
        tile = vec_name.split("_")[vector_tile_pos]
        model = vec_name.split("_")[vector_model_pos]
        key = (seed, tile, model)
        fields = vf.get_all_fields_in_shape(vector)
        if len(
                vf.getFieldElement(vector,
                                   driverName="ESRI Shapefile",
                                   field=fields[0],
                                   mode="all",
                                   elemType="str")) != 0:
            group.append((key, vector))
    for classif in classifications:
        classif_name = os.path.basename(classif)
        seed = classif_name.split("_")[classif_seed_pos].split(".tif")[0]
        tile = classif_name.split("_")[classif_tile_pos]
        model = classif_name.split("_")[classif_model_pos]
        key = (seed, tile, model)
        group.append((key, classif))
    # group by keys
    groups_param_buff = [param for key, param in fu.sortByFirstElem(group)]
    groups_param = []
    # check if all parameter to find are found.
    for group in groups_param_buff:
        if len(group) != 3:
            logger.debug(f"all parameter to use Dempster-Shafer fusion, "
                         f"not found : {group}")
        else:
            groups_param.append(group)

    # output
    output_parameters = []
    for param in groups_param:
        for sub_param in param:
            if ".shp" in sub_param:
                ref_vector = sub_param
            elif "SAR.tif" in sub_param:
                classif_sar = sub_param
            elif ".tif" in sub_param and "SAR.tif" not in sub_param:
                classif_opt = sub_param
        output_parameters.append((ref_vector, classif_opt))
        output_parameters.append((ref_vector, classif_sar))

    return output_parameters


def confusion_sar_optical(ref_vector: Tuple[str, str],
                          data_field: str,
                          ram: Optional[int] = 128,
                          logger: Optional[Logger] = LOGGER):
    """
    function use to compute a confusion matrix dedicated to the D-S
    classification fusion.

    Parameter
    ---------
    ref_vector : tuple
        tuple containing (reference vector, classification raster)
    dataField : string
        labels fields in reference vector
    ram : int
        ram dedicated to produce the confusion matrix (OTB's pipeline size)
    LOGGER : logging
        root logger
    """
    from iota2.Common import OtbAppBank

    ref_vector, classification = ref_vector
    csv_out = ref_vector.replace(".shp", ".csv")
    if "SAR.tif" in classification:
        csv_out = csv_out.replace(".csv", "_SAR.csv")
    if os.path.exists(csv_out):
        os.remove(csv_out)

    confusion_parameters = {
        "in": classification,
        "out": csv_out,
        "ref": "vector",
        "ref.vector.in": ref_vector,
        "ref.vector.field": data_field.lower(),
        "ram": str(0.8 * ram)
    }

    confusion_matrix = OtbAppBank.CreateComputeConfusionMatrixApplication(
        confusion_parameters)

    logger.info(f"Launch : {csv_out}")
    confusion_matrix.ExecuteAndWriteOutput()
    logger.debug(f"{csv_out} done")


if __name__ == "__main__":
    PARSER = argparse.ArgumentParser(
        description="this function create a confusion matrix")
    PARSER.add_argument(
        "-path.classif",
        help=("path to the folder which contains classification "
              "images (mandatory)"),
        dest="pathClassif",
        required=True)
    PARSER.add_argument(
        "-path.valid",
        help=("path to the folder which contains validation samples"
              " (with priority) (mandatory)"),
        dest="path_valid",
        required=True)
    PARSER.add_argument("-N",
                        dest="N",
                        help="number of random sample(mandatory)",
                        required=True,
                        type=int)
    PARSER.add_argument("-data.field",
                        dest="dataField",
                        help="data's field into data shape (mandatory)",
                        required=True)
    PARSER.add_argument(
        "-confusion.out.cmd",
        dest="pathToCmdConfusion",
        help=("path where all confusion cmd will be stored in a text file"
              "(mandatory)"),
        required=True)
    PARSER.add_argument("--wd",
                        dest="path_wd",
                        help="path to the working directory",
                        default=None,
                        required=False)
    PARSER.add_argument("-output_path",
                        help="path to the output directory (mandatory)",
                        dest="output_path",
                        required=True)
    PARSER.add_argument("-spatial_res",
                        help="the spatial resolution (mandatory)",
                        dest="spatial_res",
                        required=True)
    PARSER.add_argument(
        "-list_tiles",
        help="the list of tiles separated by space (mandatory)",
        dest="list_tiles",
        required=True)
    PARSER.add_argument("-cross_val",
                        help="activate cross validation ",
                        dest="cross_val",
                        default=False,
                        required=False)
    ARGS = PARSER.parse_args()

    gen_conf_matrix(ARGS.pathClassif,
                    ARGS.path_valid,
                    ARGS.N,
                    ARGS.dataField,
                    ARGS.pathToCmdConfusion,
                    ARGS.path_wd,
                    ARGS.output_path,
                    ARGS.spatial_res,
                    ARGS.list_tiles,
                    ARGS.cross_val,
                    logger=LOGGER)
