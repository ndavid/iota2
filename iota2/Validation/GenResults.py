#!/usr/bin/env python3
#-*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================

import os
import logging
import argparse
import numpy as np
from typing import Dict

from iota2.Common import FileUtils as fu

LOGGER = logging.getLogger("distributed.worker")


def genResults(pathRes: str, pathNom: str, labels_conversion_table: Dict):
    """
    generate IOTA² final report
    """
    from iota2.Validation import ResultsUtils as resU

    all_castable = []
    for _, user_label in labels_conversion_table.items():
        try:
            __ = int(user_label)
            all_castable.append(True)
        except ValueError:
            all_castable.append(False)
    re_encode_labels = all(all_castable)
    if re_encode_labels:
        labels_conversion_table = None

    all_csv = fu.FileSearch_AND(pathRes + "/TMP", True, "Classif", ".csv")

    resU.stats_report(all_csv,
                      pathNom,
                      os.path.join(pathRes, "RESULTS.txt"),
                      labels_table=labels_conversion_table)

    for seed_csv in all_csv:
        name, ext = os.path.splitext(os.path.basename(seed_csv))
        out_png = os.path.join(pathRes, "Confusion_Matrix_{}.png".format(name))
        resU.gen_confusion_matrix_fig(seed_csv,
                                      out_png,
                                      pathNom,
                                      undecidedlabel=None,
                                      dpi=200,
                                      write_conf_score=True,
                                      grid_conf=True,
                                      conf_score="count_sci",
                                      threshold=0.1,
                                      labels_table=labels_conversion_table)


if __name__ == "__main__":

    parser = argparse.ArgumentParser(
        description=
        "This function shape classifications (fake fusion and tiles priority)")
    parser.add_argument(
        "-path.res",
        help=
        "path to the folder which contains classification's results (mandatory)",
        dest="pathRes",
        required=True)
    parser.add_argument("-path.nomenclature",
                        help="path to the nomenclature (mandatory)",
                        dest="pathNom",
                        required=True)
    args = parser.parse_args()

    genResults(args.pathRes, args.pathNom)
