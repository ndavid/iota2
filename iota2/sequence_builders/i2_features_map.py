#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
import os
import shutil
import logging
from typing import Optional
from functools import partial
from collections import OrderedDict

from iota2.Steps.IOTA2Step import Step
import iota2.Common.i2_constants as i2_const
from iota2.Common import ServiceError as sErr
from iota2.Steps.IOTA2Step import StepContainer
from iota2.configuration_files import read_config_file as rcf
from iota2.sequence_builders.i2_sequence_builder import i2_builder
from iota2.configuration_files import check_config_parameters as ccp
from iota2.Common.IOTA2Directory import generate_features_maps_directories

LOGGER = logging.getLogger("distributed.worker")
I2_CONST = i2_const.iota2_constants()


class i2_features_map(i2_builder):
    """
    class use to describe steps sequence and variable to use at
    each step (config)
    """
    def __init__(self,
                 cfg: str,
                 config_resources: str,
                 schduler_type: str,
                 restart: Optional[bool] = False,
                 tasks_states_file: Optional[str] = None,
                 hpc_working_directory: Optional[str] = "TMPDIR"):
        # ajouts nouveaux parametres pour dask
        super().__init__(cfg, config_resources, schduler_type, restart,
                         tasks_states_file, hpc_working_directory)

        # steps definitions
        self.config_resources = config_resources
        # self.steps_group = OrderedDict()
        self.steps_group["init"] = OrderedDict()
        self.steps_group["writing"] = OrderedDict()

        # build steps
        self.sort_step()

        # control variable
        self.control_var = self.init_dict_control_variables()

        # pickle's path
        # self.iota2_pickle = os.path.join(
        #     rcf.read_config_file(self.cfg).getParam("chain", "output_path"),
        #     "logs", "iota2.txt")

    def get_dir(self):
        """
        usage : return iota2_directories
        """
        directories = ['final', "features", "customF"]

        iota2_outputs_dir = rcf.read_config_file(self.cfg).getParam(
            'chain', 'output_path')

        return [os.path.join(iota2_outputs_dir, d) for d in directories]

    def build_steps(self, cfg, config_ressources=None):
        """
        build steps
        """
        from iota2.Steps import CommonMasks
        from iota2.Steps import sensorsPreprocess
        from iota2.Steps import write_features_map
        from iota2.Steps import merge_features_maps

        # Init self.tiles for step communication
        Step.set_models_spatial_information(self.tiles, {})
        # will contains all IOTA² steps
        s_container = StepContainer("featuresbuilder")
        s_container.append(
            partial(sensorsPreprocess.sensorsPreprocess, self.cfg,
                    self.config_resources, self.workingDirectory), "init")
        s_container.append(
            partial(CommonMasks.CommonMasks, self.cfg, self.config_resources),
            "init")

        s_container.append(
            partial(write_features_map.write_features_map, self.cfg,
                    self.config_resources), "writing")

        s_container.append(
            partial(merge_features_maps.merge_features_maps, self.cfg,
                    self.config_resources), "writing")

        return [s_container]

    def pre_check(self):
        self.check_config_parameters()

    def check_config_parameters(self):
        config_content = rcf.read_config_file(self.cfg)
        try:
            ccp.test_var_config_file(config_content.cfg, "chain", "first_step",
                                     str, ["init", "writing"])
            ccp.test_var_config_file(config_content.cfg, "chain", "last_step",
                                     str, ["init", "writing"])

            # ensure that all tiles required are in folder
            tiles = config_content.cfg.chain.list_tile.split(" ")
            path_to_test = []
            if config_content.cfg.chain.s2_path.lower:
                path_to_test.append(config_content.cfg.chain.s2_path)
            if config_content.cfg.chain.s1_path:
                path_to_test.append(config_content.cfg.chain.s1_path)
            if config_content.cfg.chain.s2_l3a_path:
                path_to_test.append(config_content.cfg.chain.s2_l3a_path)
            if config_content.cfg.chain.l8_path:
                path_to_test.append(config_content.cfg.chain.l8_path)
            if config_content.cfg.chain.l5_path_old:
                path_to_test.append(config_content.cfg.chain.l5_path_old)
            if config_content.cfg.chain.l8_path_old:
                path_to_test.append(config_content.cfg.chain.l8_path_old)
            if config_content.cfg.chain.s2_s2c_path:
                path_to_test.append(config_content.cfg.chain.s2_s2c_path)

            for path in path_to_test:
                for tile in tiles:

                    ccp.test_ifexists(os.path.join(path, tile))
        except sErr.configFileError as err:
            raise err

    def init_dict_control_variables(self):
        # control variable
        config_content = rcf.read_config_file(self.cfg)
        control_var = {}
        control_var["iota2_outputs_dir"] = config_content.getParam(
            'chain', 'output_path')
        control_var["remove_output_path"] = config_content.getParam(
            "chain", "remove_output_path")
        return control_var

    def generate_output_directories(self,
                                    first_step_index: int,
                                    restart: bool,
                                    builder_index: Optional[int] = 0):
        i2_output_dir = self.control_var["iota2_outputs_dir"]
        rm_if_exists = self.control_var["remove_output_path"]

        task_status_file = os.path.join(i2_output_dir,
                                        I2_CONST.i2_tasks_status_filename)

        restart = restart and os.path.exists(task_status_file)
        rm_if_exists = rm_if_exists and os.path.exists(i2_output_dir)

        if rm_if_exists and first_step_index != 0 or restart:
            pass
        elif rm_if_exists and builder_index == 0:
            shutil.rmtree(i2_output_dir)

        if restart:
            pass
        elif (first_step_index == 0
              and not restart) or not os.path.exists(i2_output_dir):
            generate_features_maps_directories(i2_output_dir)
        else:
            pass
