#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
""" Module for parse config file"""
import os
import sys
import logging
from collections import Counter
from itertools import zip_longest
from typing import Dict, Union, List
from inspect import getmembers, isclass, isfunction
from config import Config, Sequence, Mapping

from iota2.Common.FileUtils import FileSearch_AND
from iota2.Common.FileUtils import get_iota2_project_dir
from iota2.sequence_builders.i2_sequence_builder import i2_builder
from iota2.Common.Tools import generate_default_config_file as gdcf
from iota2.configuration_files import check_config_parameters as ccp
from iota2.configuration_files import default_config_parameters as dcp
LOGGER = logging.getLogger("distributed.worker")

# this is a pointer to the module object instance itself.
this = sys.modules[__name__]

# declaration of path_conf and cfg variables
this.path_conf = None
this.cfg = None


def clearConfig():
    if this.path_conf is not None:
        # also in local function scope. no scope specifier
        # like global is needed
        this.path_conf = None
        this.cfg = None


class read_config_file:
    """
    The class serviceConfigFile defines all methods to access to the
    configuration file and to check the variables.
    """
    def __init__(self, path_conf, iota_config=True):
        """
            Init class serviceConfigFile
            :param path_conf: string path of the config file
            iota_config: if not activated it allow to load any kind of config
                         file
        """
        self.path_conf = path_conf
        self.cfg = Config(open(path_conf))
        self.conversion_param = {}
        if iota_config:
            # create dictionnary of sections readed
            # usefull to know if a section has been readed in config file

            self.backlog_params = {}
            self.backlog_params["readed"] = []
            self.backlog_params["default"] = []
            self.backlog_params["cfg_raw"] = Config(open(path_conf))
            self.load_params()
            self.check_params()

    def load_params(self):

        function_list = [
            fun for fun in getmembers(dcp)
            if isclass(fun[1]) and fun[0].startswith("init")
        ]

        for fun in function_list:
            param = fun[1]()
            # build a dictionary of checks
            self.conversion_param[param.name] = param.ensure_param_type
            # Checking type
            # param.ensure_param_type(param.value)
            block_name, default_params = param.get_init_values()
            if self.is_section_available(block_name):
                self.backlog_params["readed"].append(block_name)
            else:
                self.backlog_params["default"].append(block_name)
            self.init_section(block_name, default_params)

    def check_params(self):

        for bloc in self.cfg.keys():
            for param in self.cfg[bloc].keys():
                if param in self.conversion_param.keys():
                    # may raise exceptions about typing
                    self.conversion_param[param](self.cfg[bloc][param])
                else:
                    print(f"WARNING: Unknow {param} unable to check type")

        function_list = [
            fun for fun in getmembers(ccp)
            if isfunction(fun[1]) and fun[0].startswith("check")
        ]
        for fun in function_list:
            # may raise exceptions about parameters consistency
            fun[1](self.cfg, self.path_conf)

    def init_section(self, sectionName, sectionDefault):
        """use to initialize a full configuration file section

        Parameters
        ----------
        sectionName : string
            section's name
        sectionDefault : dict
            default values are store in a python dictionnary
        """
        if not hasattr(self.cfg, sectionName):
            section_default = self.init_dicoMapping(sectionDefault)
            self.cfg.addMapping(sectionName, section_default, "")
        for key, value in list(sectionDefault.items()):
            self.addParam(sectionName, key, value)

    def init_dicoMapping(self, myDict):
        """use to init a mapping object from a dict
        """
        new_map = Mapping()
        for key, value in list(myDict.items()):
            new_map.addMapping(key, value, "")
        return new_map

    def init_listSequence(self, myList):
        """use to init a Sequence object from a list
        """
        new_seq = Sequence()
        for elem in myList:
            new_seq.append(elem, "#comment")
        return new_seq

    def __str__(self):
        return gdcf.display_complete_config_file(self.path_conf)

    def __repr__(self):
        return "Configuration file : " + self.path_conf

    def getAvailableSections(self):
        """
        Return all sections in the configuration file
        :return: list of available section
        """
        return [section for section in list(self.cfg.keys())]

    def is_section_available(self, section):
        """
        """
        if not hasattr(self.cfg, section):
            return False
        return True

    def getSection(self, section):
        """
        """
        if not hasattr(self.cfg, section):
            # not an osoError class because it should NEVER happened
            raise Exception(
                "Section {} is not in the configuration file ".format(section))
        return getattr(self.cfg, section)

    def getParam(self, section, variable):
        """
            Return the value of variable in the section from config
            file define in the init phase of the class.
            :param section: string name of the section
            :param variable: string name of the variable
            :return: the value of variable
        """

        if not hasattr(self.cfg, section):
            # not an osoError class because it should NEVER happened
            raise Exception("Section is not in the configuration file: " +
                            str(section))

        objSection = getattr(self.cfg, section)
        if not hasattr(objSection, variable):
            # not an osoError class because it should NEVER happened
            raise Exception("Variable is not in the configuration file: " +
                            str(variable))

        tmpVar = getattr(objSection, variable)

        if variable in self.conversion_param.keys():
            param_value = self.conversion_param[variable](tmpVar)

        else:
            print(f"Key not found for ensuring type {variable}")
            param_value = tmpVar
        return param_value

    def setParam(self, section, variable, value):
        """
            Set the value of variable in the section from config
            file define in the init phase of the class.
            Mainly used in Unitary test in order to force a value
            :param section: string name of the section
            :param variable: string name of the variable
            :param value: value to set
        """

        if not hasattr(self.cfg, section):
            # not an osoError class because it should NEVER happened
            raise Exception("Section is not in the configuration file: " +
                            str(section))

        objSection = getattr(self.cfg, section)

        if not hasattr(objSection, variable):
            # not an osoError class because it should NEVER happened
            raise Exception("Variable is not in the configuration file: " +
                            str(variable))

        setattr(objSection, variable, value)

    def addParam(self, section, variable, value):
        """
            ADD and set a parameter in an existing section in the config
            file define in the init phase of the class.
            Do nothing if the parameter exist.
            :param section: string name of the section
            :param variable: string name of the variable
            :param value: value to set
        """
        if not hasattr(self.cfg, section):
            raise Exception(
                f"Section is not in the configuration file: {section}")

        objSection = getattr(self.cfg, section)
        if not hasattr(objSection, variable):
            setattr(objSection, variable, value)

    def forceParam(self, section, variable, value):
        """
            ADD a parameter in an existing section in the config
            file define in the init phase of the class if the parameter
            doesn't exist.
            FORCE the value if the parameter exist.
            Mainly used in Unitary test in order to force a value
            :param section: string name of the section
            :param variable: string name of the variable
            :param value: value to set
        """

        if not hasattr(self.cfg, section):
            # not an osoError class because it should NEVER happened
            raise Exception(
                f"Section is not in the configuration file: {section}")

        objSection = getattr(self.cfg, section)

        if not hasattr(objSection, variable):
            # It's normal because the parameter should not already exist
            # creation of attribute
            setattr(objSection, variable, value)

        else:
            # It already exist !!
            # setParam instead !!
            self.setParam(section, variable, value)

    def get_builders(self):
        """
        This function return a builder module
        if all conditions are satisfied
        """
        def check_code_path(code_path):

            if code_path is None:
                return False
            if code_path.lower() == "none":
                return False
            if len(code_path) < 1:
                return False
            if not os.path.isfile(code_path):
                raise ValueError(f"Error: {code_path} is not a correct path")
            return True

        def check_import(module_path):
            import importlib

            spec = importlib.util.spec_from_file_location(
                module_path.split(os.sep)[-1].split('.')[0], module_path)
            module = importlib.util.module_from_spec(spec)
            spec.loader.exec_module(module)
            return module

        def check_function_in_module(module, builder_name):

            try:
                builder_init = getattr(module, builder_name)
                return builder_init
            except AttributeError:
                raise AttributeError(
                    f"{module.__name__} has no class {builder_name}")

        user_mod_paths = self.getParam("builders", "builders_paths")

        # will contains iota2 builder module then user builder modules
        modules_path = []

        i2_mod_path = os.path.join(get_iota2_project_dir(), "iota2",
                                   "sequence_builders")
        i2_modules = FileSearch_AND(i2_mod_path, True, ".py")
        modules_path = list(
            filter(lambda x: ".pyc" not in x and "__init__" not in x,
                   i2_modules))

        if user_mod_paths:
            user_modules = []
            for user_mod_path in user_mod_paths:
                user_modules += FileSearch_AND(user_mod_path, True, ".py")
            modules_path += list(
                filter(
                    lambda x: ".pyc" not in x and "__init__" not in x and
                    not ".py~" in x, user_modules))

        class_names = self.getParam("builders", "builders_class_name")
        builders = []
        for class_name in class_names:
            class_found = []
            for module_path in modules_path:
                module_path_valid = check_code_path(module_path)
                module = None
                init_builder = None
                if module_path_valid:
                    module = check_import(module_path)
                    try:
                        init_builder = check_function_in_module(
                            module, class_name)
                        class_found.append(True)
                    except AttributeError:
                        class_found.append(False)
                        continue
                builders.append(init_builder)
                break
            if not any(class_found):
                raise ValueError(
                    f"builder class '{class_name}' can't be found in modules '{' '.join(modules_path)}'"
                )
        self.builders_compatibility(builders)

        return builders

    def builders_compatibility(self, builders_list: List[i2_builder]) -> None:
        """check builders compatibilty

        Parameters
        ----------
        builders_list
            Ordered list of builder to launch

        Notes
        -----

        raise exceptions if there no compatibilty
        """

        named_builders_list = [builder.__name__ for builder in builders_list]

        # common rules
        # A builder can't be instanciate twice
        for counter_module, occurs in Counter(named_builders_list).items():
            if occurs != 1:
                raise ValueError(
                    f"module '{counter_module}' cannot be used twice or more. Please check builders in iota2 configuration file"
                )
        # vectorization after classification
        if "i2_vectorization" in named_builders_list and "i2_classification" in named_builders_list:
            index_vecto = named_builders_list.index("i2_vectorization")
            index_classif = named_builders_list.index("i2_classification")
            if index_vecto < index_classif:
                raise ValueError(
                    "vectorization can't be perform before classification. Please check builders order in iota2 configuration file"
                )
        # about feature's map
        if "i2_features_map" in named_builders_list and "i2_classification" in named_builders_list:
            raise ValueError(
                "i2_features_map and i2_classification are incompatible builders. Please check builders in iota2 configuration file"
            )
        if "i2_vectorization" in named_builders_list and "i2_features_map" in named_builders_list:
            index_vecto = named_builders_list.index("i2_vectorization")
            index_f_map = named_builders_list.index("i2_features_map")
            if index_vecto < index_f_map:
                raise ValueError(
                    "vectorization can't be perform before features map generation. Please check builders order in iota2 configuration file"
                )


class iota2_parameters:
    """
    describe iota2 parameters as usual python dictionary
    """
    def __init__(self, configuration_file: str):
        """
        """
        self.__config = read_config_file(configuration_file)
        self.projection = int(
            self.__config.getParam('chain', 'proj').split(":")[-1])
        self.all_tiles = self.__config.getParam('chain', 'list_tile')
        self.i2_output_path = self.__config.getParam('chain', 'output_path')
        self.extract_bands_flag = self.__config.getParam(
            'iota2_feature_extraction', 'extract_bands')
        self.auto_date = self.__config.getParam('sensors_data_interpolation',
                                                'auto_date')
        self.write_outputs_flag = self.__config.getParam(
            'sensors_data_interpolation', 'write_outputs')
        self.features_list = self.__config.getParam('arg_train', 'features')
        self.enable_gapfilling = self.__config.getParam(
            'sensors_data_interpolation', 'use_gapfilling')
        self.hand_features_flag = self.__config.getParam(
            'sensors_data_interpolation', 'use_additional_features')
        self.copy_input = self.__config.getParam('iota2_feature_extraction',
                                                 'copy_input')
        self.rel_refl = self.__config.getParam('iota2_feature_extraction',
                                               'rel_refl')
        self.keep_dupl = self.__config.getParam('iota2_feature_extraction',
                                                'keep_duplicates')
        self.vhr_path = self.__config.getParam('coregistration', 'vhr_path')
        self.acor_feat = self.__config.getParam('iota2_feature_extraction',
                                                'acor_feat')
        self.user_patterns = self.__config.getParam('userFeat',
                                                    'patterns').split(",")

        self.available_sensors_section = [
            "Sentinel_2", "Sentinel_2_S2C", "Sentinel_2_L3A", "Sentinel_1",
            "Landsat8", "Landsat8_old", "Landsat5_old", "userFeat"
        ]
        self.working_resolution = self.__config.getParam(
            'chain', 'spatial_resolution')

    def get_sensors_parameters(
            self, tile_name: str
    ) -> Dict[str, Dict[str, Union[str, List[str], int]]]:
        """get enabled sensors parameters
        """
        sensors_parameters = {}
        for sensor_section_name in self.available_sensors_section:
            sensor_parameter = self.build_sensor_dict(tile_name,
                                                      sensor_section_name)
            if sensor_parameter:
                sensors_parameters[sensor_section_name] = sensor_parameter
        return sensors_parameters

    def build_sensor_dict(
            self, tile_name: str,
            sensor_section_name: str) -> Dict[str, Union[str, List[str], int]]:
        """get sensor parameters
        """
        sensor_dict = {}
        sensor_output_target_dir = None
        sensor_data_param_name = ""
        keep_bands = None
        if sensor_section_name == "Sentinel_2":
            sensor_data_param_name = "s2_path"
            sensor_output_target_dir = self.__config.getParam(
                "chain", "s2_output_path")
            keep_bands = self.__config.getParam("Sentinel_2", "keep_bands")
        elif sensor_section_name == "Sentinel_1":
            sensor_data_param_name = "s1_path"
        elif sensor_section_name == "Landsat8":
            sensor_data_param_name = "l8_path"
            keep_bands = self.__config.getParam("Landsat8", "keep_bands")

        elif sensor_section_name == "Landsat8_old":
            sensor_data_param_name = "l8_path_old"
            keep_bands = self.__config.getParam("Landsat8_old", "keep_bands")

        elif sensor_section_name == "Landsat5_old":
            sensor_data_param_name = "l5_path_old"
            keep_bands = self.__config.getParam("Landsat5_old", "keep_bands")

        elif sensor_section_name == "Sentinel_2_S2C":
            sensor_data_param_name = "s2_s2c_path"
            keep_bands = self.__config.getParam("Sentinel_2_S2C", "keep_bands")

            sensor_output_target_dir = self.__config.getParam(
                "chain", "s2_s2c_output_path")
        elif sensor_section_name == "Sentinel_2_L3A":
            sensor_data_param_name = "s2_l3a_path"
            sensor_output_target_dir = self.__config.getParam(
                "chain", "s2_l3a_output_path")
            keep_bands = self.__config.getParam("Sentinel_2_L3A", "keep_bands")

        elif sensor_section_name == "userFeat":
            sensor_data_param_name = "user_feat_path"
        else:
            raise ValueError(f"unknown section : {sensor_section_name}")
        sensor_data_path = self.__config.getParam("chain",
                                                  sensor_data_param_name)
        if not sensor_data_path:
            return sensor_dict
        if sensor_section_name == "Sentinel_1":
            sensor_section = {}
        else:
            sensor_section = self.__config.getSection(sensor_section_name)

        sensor_dict["tile_name"] = tile_name
        sensor_dict["target_proj"] = self.projection
        sensor_dict["all_tiles"] = self.all_tiles
        sensor_dict["image_directory"] = sensor_data_path
        sensor_dict["write_dates_stack"] = sensor_section.get(
            "write_reproject_resampled_input_dates_stack", None)
        sensor_dict["extract_bands_flag"] = self.extract_bands_flag
        sensor_dict["output_target_dir"] = sensor_output_target_dir
        sensor_dict["keep_bands"] = keep_bands
        sensor_dict["i2_output_path"] = self.i2_output_path
        sensor_dict["temporal_res"] = sensor_section.get(
            "temporal_resolution", None)
        sensor_dict["auto_date_flag"] = self.auto_date
        sensor_dict["date_interp_min_user"] = sensor_section.get(
            "start_date", None)
        sensor_dict["date_interp_max_user"] = sensor_section.get(
            "end_date", None)
        sensor_dict["write_outputs_flag"] = self.write_outputs_flag
        sensor_dict["features"] = self.features_list
        sensor_dict["enable_gapfilling"] = self.enable_gapfilling
        sensor_dict["hand_features_flag"] = self.hand_features_flag
        sensor_dict["hand_features"] = sensor_section.get(
            "additional_features", None)
        sensor_dict["copy_input"] = self.copy_input
        sensor_dict["rel_refl"] = self.rel_refl
        sensor_dict["keep_dupl"] = self.keep_dupl
        sensor_dict["vhr_path"] = self.vhr_path
        sensor_dict["acor_feat"] = self.acor_feat
        sensor_dict["patterns"] = self.user_patterns
        sensor_dict["working_resolution"] = self.working_resolution
        return sensor_dict
