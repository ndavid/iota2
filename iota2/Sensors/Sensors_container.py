#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""
This class manage sensor's data by tile, providing services needed in whole
IOTA² library
"""
import os
import logging
from typing import Optional, List, TypeVar, Tuple
from iota2.Sensors.Sentinel_1 import sentinel_1
from iota2.Sensors.Sentinel_2 import sentinel_2
from iota2.Sensors.Sentinel_2_S2C import sentinel_2_s2c
from iota2.Sensors.Sentinel_2_L3A import sentinel_2_l3a
from iota2.Sensors.Landsat_5_old import landsat_5_old
from iota2.Sensors.Landsat_8 import landsat_8
from iota2.Sensors.Landsat_8_old import landsat_8_old
from iota2.Sensors.User_features import user_features
from iota2.Common.ServiceError import configError
sensor_type = TypeVar("Sensor_class_object")
otbapp_type = TypeVar("OtbApplication")

LOGGER = logging.getLogger("distributed.worker")


class sensors_container():
    """
    The sensors container class
    """
    def __init__(self, tile_name: str, working_dir: str, output_path: str,
                 **kwargs) -> None:
        """
        Build the sensors container class

        Parameters
        ----------
        tile_name :
            tile to consider : "T31TCJ"
        working_dir :
            absolute path to a working directory dedicated to compute temporary
            data
        output_path :
            the final output path
        sensors_description :
            :class:`iota2.Common.ServiceConfigFile.iota2_parameters`

        """
        self.tile_name = tile_name
        self.working_dir = working_dir
        self.sensors_description = kwargs
        self.enabled_sensors = self.get_enabled_sensors()
        self.common_mask_name = "MaskCommunSL.tif"
        self.features_dir = os.path.join(output_path, "features", tile_name)
        self.common_mask_dir = os.path.join(self.features_dir, "tmp")

        self.logger = kwargs.get("logger", LOGGER)

    def __str__(self):
        """return enabled sensors and the current tile
        """
        return "tile's name : {}, available sensors : {}".format(
            self.tile_name, ", ".join(self.print_enabled_sensors_name()))

    def __repr__(self):
        """return enabled sensors and the current tile
        """
        return self.__str__()

    def get_iota2_sensors_names(self) -> List[str]:
        """
        Define the list of supported sensors in iota2

        """
        available_sensors_name = [
            landsat_5_old.name, landsat_8.name, landsat_8_old.name,
            sentinel_1.name, sentinel_2.name, sentinel_2_s2c.name,
            sentinel_2_l3a.name, user_features.name
        ]
        return available_sensors_name

    def print_enabled_sensors_name(self) -> List[str]:
        """
        Create a list of name of enabled sensors
        """
        l5_old = self.sensors_description.get("Landsat5_old", None)
        land8 = self.sensors_description.get('Landsat8', None)
        l8_old = self.sensors_description.get('Landsat8_old', None)
        sen1 = self.sensors_description.get("Sentinel_1", None)
        sen2 = self.sensors_description.get("Sentinel_2", None)
        s2_s2c = self.sensors_description.get("Sentinel_2_S2C", None)
        s2_l3a = self.sensors_description.get("Sentinel_2_L3A", None)
        user_feat = self.sensors_description.get("userFeat", None)

        enabled_sensors = []
        if l5_old is not None:
            enabled_sensors.append(landsat_5_old.name)
        if land8 is not None:
            enabled_sensors.append(landsat_8.name)
        if l8_old is not None:
            enabled_sensors.append(landsat_8_old.name)
        if sen1 is not None:
            enabled_sensors.append(sentinel_1.name)
        if sen2 is not None:
            enabled_sensors.append(sentinel_2.name)
        if s2_s2c is not None:
            enabled_sensors.append(sentinel_2_s2c.name)
        if s2_l3a is not None:
            enabled_sensors.append(sentinel_2_l3a.name)
        if user_feat is not None:
            enabled_sensors.append(user_features.name)
        return enabled_sensors

    def get_sensor(self, sensor_name: str) -> Optional[sensor_type]:
        """

        Return a sensor class object found by it's name

        Return None if not found

        Parameters
        ----------
        sensor_name :
            sensor's name

        """
        sensor_found = []
        for sensor in self.enabled_sensors:
            if sensor_name == sensor.__class__.name:
                sensor_found.append(sensor)
        if len(sensor_found) > 1:
            raise Exception(
                "Too many sensors found with the name {}".format(sensor_name))
        return sensor_found[0] if sensor_found else None

    def remove_sensor(self, sensor_name: str) -> None:
        """
        Remove a sensor in the available sensor list

        Parameters
        ----------
        sensor_name :
            the sensor's name to remove

        """
        for index, sensor in enumerate(self.enabled_sensors):
            if sensor_name == sensor.__class__.name:
                self.enabled_sensors.pop(index)

    def get_enabled_sensors(self) -> List[sensor_type]:
        """
        Build enabled sensor list

        This function define sensors to use in IOTA2 run. It is where sensor's
        order is defined :

        1. Landsat5_old
        2. Landsat8
        3. Landsat8_old
        4. Sentinel_1
        5. Sentinel_2
        6. Sentinel_2_S2C
        7. Sentinel_2_L3A
        8. userFeat
        """
        l5_old = self.sensors_description.get("Landsat5_old", None)
        land8 = self.sensors_description.get('Landsat8', None)
        l8_old = self.sensors_description.get('Landsat8_old', None)
        sen1 = self.sensors_description.get("Sentinel_1", None)
        sen2 = self.sensors_description.get("Sentinel_2", None)
        s2_s2c = self.sensors_description.get("Sentinel_2_S2C", None)
        s2_l3a = self.sensors_description.get("Sentinel_2_L3A", None)
        user_feat = self.sensors_description.get("userFeat", None)

        enabled_sensors = []
        if l5_old is not None:
            enabled_sensors.append(landsat_5_old(**l5_old))
        if land8 is not None:
            enabled_sensors.append(landsat_8(**land8))
        if l8_old is not None:
            enabled_sensors.append(landsat_8_old(**l8_old))
        if sen1 is not None:
            enabled_sensors.append(sentinel_1(**sen1))
        if sen2 is not None:
            enabled_sensors.append(sentinel_2(**sen2))
        if s2_s2c is not None:
            enabled_sensors.append(sentinel_2_s2c(**s2_s2c))
        if s2_l3a is not None:
            enabled_sensors.append(sentinel_2_l3a(**s2_l3a))
        if user_feat is not None:
            enabled_sensors.append(user_features(**user_feat))
        if len(enabled_sensors) == 0:
            raise configError(
                "No active sensors. Check your configuration file.")
        return enabled_sensors

    def get_enabled_sensors_path(self) -> List[str]:
        """
        Return the enabled sensors path to images
        """
        # self.enabled_sensors

        l5_old = self.sensors_description.get("Landsat5_old", None)
        land8 = self.sensors_description.get('Landsat8', None)
        l8_old = self.sensors_description.get('Landsat8_old', None)
        sen1 = self.sensors_description.get("Sentinel_1", None)
        sen2 = self.sensors_description.get("Sentinel_2", None)
        s2_s2c = self.sensors_description.get("Sentinel_2_S2C", None)
        s2_l3a = self.sensors_description.get("Sentinel_2_L3A", None)
        user_feat = self.sensors_description.get("userFeat", None)

        paths = []
        for sensor in self.enabled_sensors:
            if landsat_5_old.name == sensor.__class__.name:
                paths.append(l5_old["image_directory"])
            elif landsat_8.name == sensor.__class__.name:
                paths.append(land8["image_directory"])
            elif landsat_8_old.name == sensor.__class__.name:
                paths.append(l8_old["image_directory"])
            elif sentinel_1.name == sensor.__class__.name:
                paths.append(sen1["image_directory"])
            elif sentinel_2.name == sensor.__class__.name:
                paths.append(sen2["image_directory"])
            elif sentinel_2_s2c.name == sensor.__class__.name:
                paths.append(s2_s2c["image_directory"])
            elif sentinel_2_l3a.name == sensor.__class__.name:
                paths.append(s2_l3a["image_directory"])
            elif user_features.name == sensor.__class__.name:
                paths.append(user_feat["image_directory"])
        return paths

    def sensors_preprocess(self,
                           available_ram: Optional[int] = 128,
                           logger=LOGGER) -> None:
        """preprocessing every enabled sensors

        Parameters
        ----------
        available_ram :
            RAM, usefull to many OTB's applications.

        """
        for sensor in self.enabled_sensors:
            self.sensor_preprocess(sensor, self.working_dir, available_ram,
                                   logger)

    def sensor_preprocess(self,
                          sensor: sensor_type,
                          working_dir: str,
                          available_ram: int,
                          logger=LOGGER) -> otbapp_type:
        """
        Sensor preprocessing

        Parameters
        ----------
        sensor :
            A sensor object, see :meth:`get_sensor`
        working_dir :
            absolute path to a working directory dedicated
            to compute temporary data
        available_ram :
            RAM, usefull to many OTB's applications.

        Notes
        -----
        object
            return the object returned from sensor.preprocess. Nowadays,
            nothing is done with it, no object's type are imposed
            but it could change.
        """
        sensor_prepro_app = None
        if "preprocess" in dir(sensor):
            sensor_prepro_app = sensor.preprocess(working_dir=working_dir,
                                                  ram=available_ram,
                                                  logger=logger)
        return sensor_prepro_app

    def sensors_dates(self) -> List[Tuple[str, List[str]]]:
        """
        Get available dates by sensors

        if exists get_available_dates's sensor method is called. The method
        must return a list of sorted available dates.

        Notes
        -------
        The list of tuple returned contains:
            (sensor's name, [list of available dates])

        """
        sensors_dates = []
        for sensor in self.enabled_sensors:
            dates_list = []
            # if "get_available_dates" in dir(sensor):
            if "get_dates_directories" in dir(sensor):
                dates_list = sensor.get_dates_directories()
            sensors_dates.append((sensor.__class__.name, dates_list))
        return sensors_dates

    def get_sensors_footprint(
            self,
            available_ram: Optional[int] = 128
    ) -> List[Tuple[str, otbapp_type]]:
        """
        Get sensor's footprint

        It provide the list of otb's application dedicated to generate sensor's
        footprint. Each otb's application must be dedicated to the generation
        of a binary raster, 0 meaning 'NODATA'

        Parameters
        ----------
        available_ram :
            RAM, usefull to many OTB's applications.

        Notes
        -----
        The returned list is a list of tuple containing
        (sensor's name, otbApplication)

        """
        sensors_footprint = []
        for sensor in self.enabled_sensors:
            sensors_footprint.append(
                (sensor.__class__.name, sensor.footprint(available_ram)))
        return sensors_footprint

    def get_common_sensors_footprint(
        self,
        available_ram: Optional[int] = 128
    ) -> Tuple[otbapp_type, List[otbapp_type]]:
        """
        Get common sensor's footprint

        provide an otbApplication ready to be 'ExecuteAndWriteOutput'
        generating a binary raster which is the intersection of all sensor's
        footprint

        Parameters
        ----------
        available_ram :
            RAM, usefull to many OTB's applications.

        Notes
        -----
        The output tuple is given by (otbApplication, dependencies)
        where otbApplication is the application and dependencies is a list of
        otbApplication for avoid pipeline issues

        """
        from iota2.Common.OtbAppBank import CreateBandMathApplication
        sensors_footprint = []
        all_dep = []
        for sensor in self.enabled_sensors:
            footprint, _ = sensor.footprint(available_ram)
            footprint.Execute()
            sensors_footprint.append(footprint)
            all_dep.append(_)
            all_dep.append(footprint)

        expr = "+".join("im{}b1".format(i + 1)
                        for i in range(len(sensors_footprint)))
        expr = "{}=={}?1:0".format(expr, len(sensors_footprint))
        common_mask_out = os.path.join(self.common_mask_dir,
                                       self.common_mask_name)
        common_mask = CreateBandMathApplication({
            "il": sensors_footprint,
            "exp": expr,
            "out": common_mask_out,
            "pixType": "uint8",
            "ram": str(available_ram)
        })
        return common_mask, all_dep

    def get_sensors_time_series(
        self,
        available_ram: Optional[int] = 128
    ) -> List[Tuple[str, Tuple[otbapp_type, List[otbapp_type]], List[str]]]:
        """
        Get time series to each enabled sensors

        Parameters
        ----------
        available_ram :
            RAM, usefull to many OTB's applications.

        Notes
        -----
        The returned list is a list of tuple :
            (sensor's name, (otbApplication, dependencies), labels)
        where:

        * Sensors name is the string returned by
          :attr:`iota2.Sensors.Sentinel_2.name` for instance
        * (otbApplication, dependencies) is a tuple containing the
          aplication to be executed and dependencies is a list of all
          application needed to avoid pipeline issue
        * labels are sorted feature's name

        """
        sensors_time_series = []
        for sensor in self.enabled_sensors:
            if "get_time_series" in dir(sensor):
                sensors_time_series.append(
                    (sensor.__class__.name,
                     sensor.get_time_series(available_ram)))
        return sensors_time_series

    def get_sensors_time_series_masks(
        self,
        available_ram: Optional[int] = 128
    ) -> List[Tuple[str, Tuple[otbapp_type, List[otbapp_type]], int]]:
        """get time series masks to each enabled sensors

        Parameters
        ----------
        available_ram :
            RAM, usefull to many OTB's applications.

        Notes
        -----
        The returned list is a list of tuple :
            (sensor's name, (otbApplication, dependencies), number of masks)
        where:

        * Sensors name is the string returned by
          :attr:`iota2.Sensors.Sentinel_2.name` for instance
        * (otbApplication, dependencies) is a tuple containing the
          aplication to be executed and dependencies is a list of all
          application needed to avoid pipeline issue
        * number of mask is an integer

        """

        sensors_time_series_masks = []
        for sensor in self.enabled_sensors:
            if "get_time_series_masks" in dir(sensor):
                sensors_time_series_masks.append(
                    (sensor.__class__.name,
                     sensor.get_time_series_masks(available_ram)))
        return sensors_time_series_masks

    def get_sensors_time_series_gapfilling(
        self,
        available_ram: Optional[int] = 128
    ) -> List[Tuple[str, Tuple[otbapp_type, List[otbapp_type]], List[str]]]:
        """
        Get time series gapfilled to each enabled sensors

        Parameters
        ----------
        available_ram :
            RAM, usefull to many OTB's applications.

        Notes
        -----
        The returned list is a list of tuple :
            (sensor's name, (otbApplication, dependencies), labels)
        where:

        * Sensors name is the string returned by
          :attr:`iota2.Sensors.Sentinel_2.name` for instance
        * (otbApplication, dependencies) is a tuple containing the
          aplication to be executed and dependencies is a list of all
          application needed to avoid pipeline issue
        * labels are sorted feature's name

        """
        sensors_time_series = []
        for sensor in self.enabled_sensors:
            if "get_time_series_gapfilling" in dir(sensor):
                sensors_time_series.append(
                    (sensor.__class__.name,
                     sensor.get_time_series_gapfilling(available_ram)))
        return sensors_time_series

    def get_sensors_features(
        self,
        available_ram: Optional[int] = 128,
        logger=LOGGER
    ) -> List[Tuple[str, Tuple[otbapp_type, List[otbapp_type]], List[str]]]:
        """
        Get features to each enabled sensors

        Parameters
        ----------
        available_ram :
            RAM, usefull to many OTB's applications.

        Notes
        -----
        The returned list of tuple is composed by
        (sensor's name, ((otbApplication, dependencies), labels))
        where:

        * Sensors name is the string returned by
          :attr:`iota2.Sensors.Sentinel_2.name` for instance
        * (otbApplication, dependencies) is a tuple containing the
          aplication to be executed and dependencies is a list of all
          application needed to avoid pipeline issue
        * labels are sorted feature's name

        """
        sensors_features = []
        for sensor in self.enabled_sensors:
            sensors_features.append((sensor.__class__.name,
                                     sensor.get_features(available_ram,
                                                         logger=logger)))
        return sensors_features
