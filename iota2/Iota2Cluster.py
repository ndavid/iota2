#!/usr/bin/env python3
#-*- coding: utf-8 -*-
# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Iota2Cluster.py allow user to deploy iota2 on HPC architecture

check 'Iota2Cluster.py -h' for options
"""

import os
import logging
import argparse
from typing import Union, List
from subprocess import Popen, PIPE

from iota2.Common.FileUtils import ensure_dir
from iota2.Common.FileUtils import get_iota2_project_dir
from iota2.configuration_files import read_config_file as rcf
from iota2.sequence_builders.i2_sequence_builder_merger import workflow_merger

LOGGER = logging.getLogger("distributed.worker")


def get_RAM(ram: str):
    """
        usage return ram in gb
        ram [param] [str]
        
        out [ram] [str] : ram in gb
    """

    ram = ram.lower().replace(" ", "")
    if "gb" in ram:
        ram = float(ram.split("gb")[0])
    elif "mb" in ram:
        ram = float(ram.split("mb")[0]) / 1024
    return ram


def get_qsub_cmd(cfg: str, config_ressources: Union[str,
                                                    None], starting_step: int,
                 ending_step: int, restart: bool, restart_from: Union[str,
                                                                      None],
                 nb_parallel_tasks: int, scheduler_type: str,
                 execution_graph_files: List[str], tmp_directory: str):
    """
    build qsub cmd to launch iota2 on HPC
    """
    from Steps import PBS_scheduler

    log_dir = os.path.join(cfg.getParam("chain", "output_path"), "logs")
    job_dir = log_dir
    ensure_dir(job_dir)

    config_path = cfg.path_conf
    iota2_main = os.path.join(job_dir, "iota2.pbs")

    config_ressources_path = None
    if config_ressources:
        config_ressources_path = config_ressources

    scheduler = PBS_scheduler.PBS_scheduler(config_ressources_path)

    chain_name = scheduler.name
    walltime = scheduler.walltime
    cpu = scheduler.cpu
    ram = scheduler.RAM

    log_err = os.path.join(log_dir, "iota2_err.log")
    log_out = os.path.join(log_dir, "iota2_out.log")

    if os.path.exists(iota2_main):
        os.remove(iota2_main)

    nb_cpu = nb_parallel_tasks if nb_parallel_tasks > cpu else cpu
    ressources = ("#!/bin/bash\n"
                  "#PBS -N {}\n"
                  "#PBS -l select=1"
                  ":ncpus={}"
                  ":mem={}\n"
                  "#PBS -l walltime={}\n"
                  "#PBS -o {}\n"
                  "#PBS -e {}\n").format(chain_name, nb_cpu, ram, walltime,
                                         log_out, log_err)

    py_path = os.environ.get('PYTHONPATH')
    path = os.environ.get('PATH')
    ld_lib_path = os.environ.get('LD_LIBRARY_PATH')
    otb_app_path = os.environ.get('OTB_APPLICATION_PATH')
    gdal_data = os.environ.get('GDAL_DATA')
    geotiff_csv = os.environ.get('GEOTIFF_CSV')
    grass = os.environ.get('GRASSDIR')

    modules = ("\nexport PYTHONPATH={}\n"
               "export PATH={}\n"
               "export LD_LIBRARY_PATH={}\n"
               "export OTB_APPLICATION_PATH={}\n"
               "export GDAL_DATA={}\n"
               "export GRASSDIR={}\n"
               "export GEOTIFF_CSV={}\n").format(py_path, path, ld_lib_path,
                                                 otb_app_path, gdal_data,
                                                 grass, geotiff_csv)
    scripts = os.path.join(get_iota2_project_dir(), "iota2")

    # mandatory parameters
    exe = (f"python {scripts}/Iota2.py "
           f"-config {config_path} "
           f"-starting_step {starting_step} "
           f"-ending_step {ending_step} "
           f"-nb_parallel_tasks {nb_parallel_tasks} "
           f"-scheduler_type {scheduler_type} "
           f"-tmp_directory_env {tmp_directory} ")

    if restart_from is not None:
        exe = f"{exe} -restart_from {restart_from}"
    if restart:
        exe = f"{exe} -restart"
    if config_ressources_path:
        exe = f"{exe} -config_ressources {config_ressources_path}"
    if config_ressources_path:
        exe = f"{exe} -config_ressources {config_ressources_path}"
    if execution_graph_files:
        exe = f"{exe} -execution_graph_files {' '.join(execution_graph_files)}"
    pbs = ressources + modules + exe

    with open(iota2_main, "w") as iota2_f:
        iota2_f.write(pbs)

    qsub = f"qsub {iota2_main}"
    return qsub


def run_cluster(parsed_args: argparse.ArgumentParser):
    """launch iota2 on HPC
    """
    # #########################################################################
    # Temporarly check of config file
    # #########################################################################
    import os
    from config import Config
    from iota2.Common.Tools import convert_configuration_files as ccf

    b_cfg = Config(open(parsed_args.config_path))
    if "outputPath" in b_cfg["chain"]:
        print("\033[31m" + "*" * 80)
        print("* Old configuration file detected. Conversion will be done.")
        print("*" * 80 + "\033[0m")
        print("\n" * 2)
        converter = ccf.conversion_config_file()
        out_name = os.path.splitext(parsed_args.config_path)[0] + "_conv.cfg"
        converter.convert(parsed_args.config_path, out_name)
        print("\n" * 2)
        print("*" * 80)
        print(f"Configuration conversion done.\nPlease check {out_name}.")
        print(f"{out_name} can be used to launch again the chain")
        return 0
    # #########################################################################
    if parsed_args.restart and parsed_args.restart_from:
        raise ValueError(
            "parameters 'restart' and 'restart_from' are not compatible")
    cfg = rcf.read_config_file(parsed_args.config_path)
    chains = cfg.get_builders()
    if len(chains) > 1:
        chain_to_process = workflow_merger(chains, parsed_args.config_path,
                                           parsed_args.config_ressources,
                                           parsed_args.scheduler_type,
                                           parsed_args.restart,
                                           parsed_args.restart_from)
    else:
        chain_to_process = chains[0](parsed_args.config_path,
                                     parsed_args.config_ressources,
                                     parsed_args.scheduler_type,
                                     parsed_args.restart,
                                     parsed_args.restart_from)
    if parsed_args.start == parsed_args.end == 0:
        all_steps = chain_to_process.get_steps_number()
        parsed_args.start = all_steps[0]
        parsed_args.end = all_steps[-1]

    first_step_index = parsed_args.start - 1
    last_step_index = parsed_args.end - 1

    final_graphs = chain_to_process.get_final_i2_exec_graph(
        first_step_index, last_step_index, parsed_args.graph_figures)

    print(
        chain_to_process.print_step_summarize(
            parsed_args.start, parsed_args.end, parsed_args.config_ressources
            is not None))

    if not parsed_args.only_summary:
        qsub_cmd = get_qsub_cmd(
            cfg, parsed_args.config_ressources, parsed_args.start,
            parsed_args.end, parsed_args.restart, parsed_args.restart_from,
            parsed_args.nb_parallel_tasks, parsed_args.scheduler_type,
            parsed_args.graph_figures, parsed_args.tmp_directory)
        process = Popen(qsub_cmd, shell=True, stdout=PIPE, stderr=PIPE)


if __name__ == "__main__":

    from iota2.Iota2 import iota2_arguments
    PARSER = iota2_arguments()

    # if we use a cluster, we must have a working directory
    PARSER.set_defaults(tmp_directory="TMPDIR")

    ARGS = PARSER.parse_args()
    CLUSTER_SCHEDULERS = ("PBS", "cluster")
    if ARGS.scheduler_type in CLUSTER_SCHEDULERS:
        run_cluster(ARGS)
    else:
        print(
            f"'Iota2Cluster.py -scheduler_type' must be in {CLUSTER_SCHEDULERS} "
        )
