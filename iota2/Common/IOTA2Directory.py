#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""Generate iota² output tree"""
import os
import shutil
import logging
from typing import Optional, List

from iota2.Common.verifyInputs import check_iota2_inputs

LOGGER = logging.getLogger("distributed.worker")


def generate_directories(root: str, merge_final_classifications: bool) -> None:
    """
    generate iota2 classification workflow output directories

    Parameters
    ----------
    root:
        iota2 output path
    merge_final_classifications
        flag to generate the directory dedicated to receive the
        fusion of final classifications

    Notes
    -----
    the removal of directories is managed in iota.sequence_builders.i2_classification.generate_output_directories
    """
    if not os.path.exists(root):
        os.mkdir(root)

    if not os.path.exists(root + "/samplesSelection"):
        os.mkdir(root + "/samplesSelection")
    if not os.path.exists(root + "/model"):
        os.mkdir(root + "/model")
    if not os.path.exists(root + "/formattingVectors"):
        os.mkdir(root + "/formattingVectors")
    if not os.path.exists(root + "/config_model"):
        os.mkdir(root + "/config_model")
    if not os.path.exists(root + "/envelope"):
        os.mkdir(root + "/envelope")
    if not os.path.exists(root + "/classif"):
        os.mkdir(root + "/classif")
    if not os.path.exists(root + "/shapeRegion"):
        os.mkdir(root + "/shapeRegion")
    if not os.path.exists(root + "/final"):
        os.mkdir(root + "/final")
    if not os.path.exists(root + "/features"):
        os.mkdir(root + "/features")
    if not os.path.exists(root + "/dataRegion"):
        os.mkdir(root + "/dataRegion")
    if not os.path.exists(root + "/learningSamples"):
        os.mkdir(root + "/learningSamples")
    if not os.path.exists(root + "/dataAppVal"):
        os.mkdir(root + "/dataAppVal")
    if not os.path.exists(root + "/stats"):
        os.mkdir(root + "/stats")

    if not os.path.exists(root + "/cmd"):
        os.mkdir(root + "/cmd")
        os.mkdir(root + "/cmd/stats")
        os.mkdir(root + "/cmd/train")
        os.mkdir(root + "/cmd/cla")
        os.mkdir(root + "/cmd/confusion")
        os.mkdir(root + "/cmd/features")
        os.mkdir(root + "/cmd/fusion")
        os.mkdir(root + "/cmd/splitShape")

    if merge_final_classifications:
        if not os.path.exists(root + "/final/merge_final_classifications"):
            os.mkdir(root + "/final/merge_final_classifications")


def generate_features_maps_directories(root: str) -> None:
    """
    generate output directories for write features maps

    Parameters
    ----------
    root : str
        iota2 output path
    """
    from iota2.Common.FileUtils import ensure_dir

    ensure_dir(root)
    if os.path.exists(root + "/final"):
        shutil.rmtree(root + "/final")
    os.mkdir(root + "/final")
    if os.path.exists(root + "/features"):
        shutil.rmtree(root + "/features")
    os.mkdir(root + "/features")
    if os.path.exists(root + "/customF"):
        shutil.rmtree(root + "/customF")
    os.mkdir(root + "/customF")


def generate_vectorization_directories(root: str) -> None:
    """
    generate vectorization needed directories

    Parameters
    ----------
    root :
        iota2 output path
    """
    from iota2.Common.FileUtils import ensure_dir
    ensure_dir(root)
    ensure_dir(os.path.join(root, "vectors"))
    ensure_dir(os.path.join(root, "simplification"))
    ensure_dir(os.path.join(root, "simplification", "tiles"))
    ensure_dir(os.path.join(root, "simplification", "vectors"))
    ensure_dir(os.path.join(root, "simplification", "mosaic"))
    ensure_dir(os.path.join(root, "simplification", "tmp"))
