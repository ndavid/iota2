import os
import logging
from typing import Dict
from pathlib import Path
from sphinx.cmd.build import main as sphinx_main
from iota2.Common.FileUtils import FileSearch_AND

LOGGER = logging.getLogger("distributed.worker")


class DisplayablePath(object):
    display_filename_prefix_middle = '├──'
    display_filename_prefix_last = '└──'
    display_parent_prefix_middle = '    '
    display_parent_prefix_last = '│   '

    def __init__(self, path, parent_path, is_last):
        self.path = Path(str(path))
        self.parent = parent_path
        self.is_last = is_last
        if self.parent:
            self.depth = self.parent.depth + 1
        else:
            self.depth = 0

    @classmethod
    def make_tree(cls, root, parent=None, is_last=False, criteria=None):
        root = Path(str(root))
        criteria = criteria or cls._default_criteria

        displayable_root = cls(root, parent, is_last)
        yield displayable_root

        children = sorted(list(path for path in root.iterdir()
                               if criteria(path)),
                          key=lambda s: str(s).lower())
        count = 1
        for path in children:
            is_last = count == len(children)
            if path.is_dir():
                yield from cls.make_tree(path,
                                         parent=displayable_root,
                                         is_last=is_last,
                                         criteria=criteria)
            else:
                yield cls(path, displayable_root, is_last)
            count += 1

    @classmethod
    def _default_criteria(cls, path):
        return True

    @property
    def displayname(self):
        if self.path.is_dir():
            return self.path.name + '/'
        return self.path.name

    def displayable(self):
        if self.parent is None:
            return self.displayname

        _filename_prefix = (self.display_filename_prefix_last if self.is_last
                            else self.display_filename_prefix_middle)

        parts = ['{!s} {!s}'.format(_filename_prefix, self.displayname)]

        parent = self.parent
        while parent and parent.parent is not None:
            parts.append(self.display_parent_prefix_middle if parent.
                         is_last else self.display_parent_prefix_last)
            parent = parent.parent

        return ''.join(reversed(parts))


def get_output_tree(path_to_draw):
    from pathlib import Path
    chain_log = "=" * 79 + "\n"
    chain_log += " " * 30 + "Output Directories" + " " * 31 + "\n"
    chain_log += "=" * 79 + "\n"
    # the call of Path ensure the path is valid ?
    out_cont = DisplayablePath.make_tree(Path(path_to_draw))
    for path in out_cont:
        chain_log += path.displayable() + "\n"
    return chain_log + "\n" * 3


def get_locale(path):
    tempfile = os.path.join(path, "locale.txt")
    os.system("locale>{}".format(tempfile))
    chain_log = "=" * 79 + "\n"
    chain_log += " " * 36 + "LOCALE" + " " * 37 + "\n"
    chain_log += "=" * 79 + "\n"
    with open(tempfile, 'r') as f_tmp:
        chain_log += f_tmp.read()
    os.remove(tempfile)
    return chain_log + "\n" * 3


def get_package_version(path):
    import os
    tempfile = os.path.join(path, "package.txt")
    os.system("conda list>{}".format(tempfile))
    chain_log = "=" * 79 + "\n"
    chain_log += " " * 30 + "Environment package" + " " * 30 + "\n"
    chain_log += "=" * 79 + "\n"
    with open(tempfile, 'r') as f_tmp:
        chain_log += f_tmp.read()
    os.remove(tempfile)
    return chain_log + "\n" * 3


def get_config_file(configfile, rst_dir):

    chain_log = "Configuration file\n"
    chain_log += "-" * len(chain_log) + "\n\n"

    chain_log += ".. code-block:: python\n\n"

    with open(configfile, 'r') as f_tmp:
        for line in f_tmp:
            chain_log += "\t" + line

    config_file_rst = os.path.join(rst_dir, "configuration_file.rst")
    with open(config_file_rst, "w") as conf_file:
        conf_file.write(chain_log)
    return {"configuration file": config_file_rst}


def get_cont_path_from_config(configfile, rst_source_dir):
    from iota2.configuration_files import read_config_file as rcf
    cfg = rcf.read_config_file(configfile)
    block_chain = cfg.getSection("chain")

    path_rst_dict = {}
    for key in block_chain.keys():
        if 'path' in key.lower():
            path_to_draw = cfg.getParam("chain", key)
            if isinstance(path_to_draw, str):
                if os.path.isdir(path_to_draw):
                    rst_file = os.path.join(rst_source_dir,
                                            f"{key}_content.rst")
                    path_rst_dict[f"{key} content"] = rst_file

                    chain_log = f"Content of {key}\n"
                    chain_log += "-" * len(chain_log) + "\n\n"
                    chain_log += ".. code-block:: bash\n\n"
                    out_cont = DisplayablePath.make_tree(Path(path_to_draw))
                    for path in out_cont:
                        chain_log += "\t" + path.displayable() + "\n"
                    with open(rst_file, "w") as rst:
                        rst.write(chain_log)
    return path_rst_dict


def get_cont_file_from_config(configfile, rst_source_dir):
    from iota2.configuration_files import read_config_file as rcf
    cfg = rcf.read_config_file(configfile)
    block_chain = cfg.getSection("chain")
    rst_file = os.path.join(rst_source_dir, f"input_files_content.rst")

    chain_log = "Input files content\n"
    chain_log += "-" * len(chain_log) + "\n\n"

    for key in block_chain.keys():
        file_to_read = cfg.getParam("chain", key)
        if isinstance(file_to_read, str):
            if (os.path.isfile(file_to_read)
                    and ('txt' in file_to_read or 'csv' in file_to_read)):
                chain_log += key + "\n"
                chain_log += "*" * len(key) + "\n\n"
                chain_log += f"content of {file_to_read}\n"
                chain_log += "\n.. code-block:: bash\n\n"
                with open(file_to_read, "r") as contfile:
                    for line in contfile:
                        chain_log += f"\t{line}\n"
                chain_log += "\n\n"
    with open(rst_file, "w") as rst:
        rst.write(chain_log)
    return {"input files content": rst_file}


def get_log_file(logfile):
    import os
    chain_log = "=" * 79 + "\n"
    name = os.path.basename(logfile)
    chain_log += " " * int((79 - len(name)) / 2) + "{}".format(name)
    chain_log += " " * int((79 - len(name)) / 2) + "\n"
    chain_log += "=" * 79 + "\n"
    cont = "Unable to open logfile : " + logfile
    with open(logfile, "r") as logf:
        cont = logf.read()
    chain_log += cont
    return chain_log + "\n" * 3


def get_log_info(path, configfile, logfile):
    chain = "\n"
    # chain += get_config_file(configfile)
    # chain += get_log_file(logfile)
    # chain += get_cont_path_from_config(configfile)
    # chain += get_cont_file_from_config(configfile)
    chain += get_locale(path)
    chain += get_package_version(path)
    return chain


def index_logging(rst_source_dir: str, pages: Dict):
    """generate iota2 index.rst dedicated to logs
    """

    header = (
        "iota² logging informations\n"
        "==========================\n\n"
        "The aime of this html documentation is to help users to "
        "understand the iota² execution and if something is going "
        "wrong, try to figure it out. If the user does not understand "
        "why the chain crashed or failed, an archive containing the "
        "```/logs``` directory (with or without the ```/html``` directory) "
        "can be attached to an issue at : https://framagit.org/iota2-project/iota2/-/issues.\n"
        "\n"
        ".. toctree::\n"
        "   :maxdepth: 1\n"
        "   :caption: Contents:\n\n")

    body = ""
    for title, rst_file in pages.items():
        body += f"   {title} <{os.path.splitext(os.path.split(rst_file)[-1])[0]}>\n"

    with open(os.path.join(rst_source_dir, "index.rst"), "w") as rst:
        rst.write(header + body)


def get_tasks_status_rst(svg_status_files, rst_dir):
    """
    """
    rst_dic = {}
    for cpt, svg_file in enumerate(svg_status_files):
        chain_log = "iota² tasks status\n"
        chain_log += "-" * len(chain_log) + "\n\n"

        chain_log += "This page allow users to check every tasks status, every node are clickable and present the dedicated log\n\n"

        chain_log += (".. raw:: html\n" f"\t:file: {svg_file}\n")
        tasks_status_rst = os.path.join(rst_dir, f"tasks_status_{cpt+1}.rst")
        with open(tasks_status_rst, "w") as rst_file:
            rst_file.write(chain_log)
        rst_dic[f"iota² tasks status graph {cpt+1}"] = tasks_status_rst
    return rst_dic


def get_env_informations(tmp_dir, rst_source_dir):

    tempfile = os.path.join(tmp_dir, "locale.txt")
    os.system("locale>{}".format(tempfile))
    chain_log = "Running environment informations\n"
    chain_log += "-" * len("Running environment informations") + "\n"
    chain_log += "LOCALE\n"
    chain_log += "*" * len("LOCALE") + "\n"
    chain_log += ".. code-block:: bash\n\n"
    with open(tempfile, 'r') as f_tmp:
        for line in f_tmp:
            chain_log += "\t" + line + "\n"
    chain_log += "\n\n"
    os.remove(tempfile)

    tempfile = os.path.join(tmp_dir, "package.txt")
    os.system("conda list>{}".format(tempfile))
    chain_log += "CONDA ENVIRONEMENT\n"
    chain_log += "*" * len("CONDA ENVIRONEMENT") + "\n"
    chain_log += ".. code-block:: bash\n\n"
    with open(tempfile, 'r') as f_tmp:
        for line in f_tmp:
            chain_log += "\t" + line + "\n"
    chain_log += "\n\n"
    os.remove(tempfile)

    rst_file = os.path.join(rst_source_dir, "environment_info.rst")
    with open(rst_file, "w") as rst:
        rst.write(chain_log)

    return {"running environment": rst_file}


def gen_rst_pages(config_file, rst_source_dir, svg_status_files,
                  i2_output_dir):
    """
    """
    config_file_rst = get_config_file(config_file, rst_source_dir)
    tasks_status_rst = get_tasks_status_rst(svg_status_files, rst_source_dir)
    path_content_rst = get_cont_path_from_config(config_file, rst_source_dir)
    file_content_rst = get_cont_file_from_config(config_file, rst_source_dir)
    environment_info_rst = get_env_informations(i2_output_dir, rst_source_dir)
    return dict(config_file_rst, **tasks_status_rst, **path_content_rst,
                **file_content_rst, **environment_info_rst), tasks_status_rst


def set_iota2_tasks_figure_size(html_page):
    """
    the svg figure size seems to be configurable, like done at
    https://github.com/pymedphys/pymedphys/blob/f4d404fa1cf3f551c4aa80ef27438f418c61a436/docs/conf.py
    by using the setup() method in sphinx conf.py 
    and specify at 
    https://www.sphinx-doc.org/en/master/usage/configuration.html

    However, by using sphinx python API (without conf.py) it seems not to be accessible.
    Currently the solution is to directly alter the html page and set the img size.
    """
    html_buffer = []
    with open(html_page, "r") as origin_html:
        for line in origin_html:
            if "<svg width=" in line:
                html_buffer.append('<svg width="550pt" height="1000pt"')
            else:
                html_buffer.append(line)
    with open(html_page, "w") as dest_html:
        dest_html.write("\n".join(html_buffer))


def gen_html_logging_pages(config_file, html_source_dir, source_dir,
                           svg_status_files, i2_output_dir) -> str:

    rst_pages, rst_status_pages = gen_rst_pages(config_file, source_dir,
                                                svg_status_files,
                                                i2_output_dir)
    rst_files = FileSearch_AND(source_dir, True, ".rst")
    index_logging(source_dir, rst_pages)

    master_doc = 'index'
    source_suffix = '.rst'
    output_file = html_source_dir
    html_theme = 'sphinx_rtd_theme'
    build_format = 'html'

    args = f"{source_dir} -b {build_format} -D extensions=sphinx.ext.autodoc " \
       f"-D master_doc={master_doc} " \
       f"-D source_suffix={source_suffix} " \
       f"-D html_theme={html_theme} " \
       f"-C {output_file} "\
       f"{' '.join(rst_files)}"

    sphinx_main(args.split())

    for _, rst_file in rst_status_pages.items():
        _, rst_tasks_status = os.path.split(rst_file)
        html_fn = f"{os.path.splitext(rst_tasks_status)[0]}.html"
        # html_source_dir
        tasks_status_html = FileSearch_AND(html_source_dir, True, html_fn)[0]
        set_iota2_tasks_figure_size(tasks_status_html)
    return FileSearch_AND(html_source_dir, True, "index.html")[0]
