#!/usr/bin/env python3
#-*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================

import os
import re
import csv
import ogr
import osr
import sys
import gdal
import logging
import argparse
from typing import List
from typing import Tuple

LOGGER = logging.getLogger("distributed.worker")


def write_color_table(
        convert_table_file: str, convert_labels: List[Tuple[str, int, int, int,
                                                            int]]) -> None:
    """write color table

    Parameters
    ----------
    convert_table_file
        output csv file
    convert_labels
        List of rules -> convert_labels[0] = (user_label, i2_label, r_value, g_value, b_value)
    """
    with open(convert_table_file, "w", newline="") as csv_file:
        writer = csv.writer(csv_file, delimiter=",")
        writer.writerow([
            "#USER LABEL", "IOTA2_LABEL", "RED_VALUE", "GREEN_VALUE",
            "BLUE_VALUE"
        ])
        for rule in convert_labels:
            writer.writerow(rule)


def CreateColorTable(fileLUT,
                     logger=LOGGER,
                     labels_conversion=None,
                     default_label_value=0,
                     default_label_color=(255, 255, 255)):
    """
    from a csv file create a gdal color table

    Parameters
    ----------
    fileLUT :
        path to the color file table
        ex : for a table containing 3 classes ("8","90","21"), "8" must be represent in red, "90" in green, "21" in blue
        then /path/to/myColorTable.csv will contains
            8 255 0 0
            90 0 255 0
            21 0 255 0
    OUT :
        ct [gdalColorTable]
    """
    filein = open(fileLUT)
    ct = gdal.ColorTable()
    conversion_list = []
    for line in filein:
        entry = line
        rgb_colors = re.findall(r'\d+', entry)
        rgb_values = (rgb_colors[-3], rgb_colors[-2], rgb_colors[-1])
        class_id = entry.rstrip().replace(" ".join(rgb_values), "").strip()
        rgb_values = tuple([int(rgb_value) for rgb_value in rgb_values])

        float_or_int = class_id.replace(".", "", 1).isdigit()

        if not float_or_int and labels_conversion:
            if class_id in labels_conversion:
                conversion_list.append(
                    (class_id, labels_conversion[class_id], rgb_values[0],
                     rgb_values[1], rgb_values[2]))
                class_id = labels_conversion[class_id]
            else:
                logger.warning(
                    f"label '{class_id}' does not exists in {labels_conversion}, skipping the label"
                )
                conversion_list.append(
                    (class_id, default_label_value, default_label_color[0],
                     default_label_color[1], default_label_color[2]))
                class_id = default_label_value
                rgb_values = default_label_color
        elif not float_or_int and labels_conversion is None:
            logger.warning(
                f"string label '{class_id}' value can't be convert into integer, skipping"
            )
            conversion_list.append(
                (class_id, default_label_value, default_label_color[0],
                 default_label_color[1], default_label_color[2]))
            class_id = default_label_value
            rgb_values = default_label_color
            conversion_list.append(
                (class_id, rgb_values[0], rgb_values[1], rgb_values[2]))
        try:
            ct.SetColorEntry(int(class_id), rgb_values)
        except:
            logger.warning(
                "a color entry was not recognize, default value set. Class label 0, RGB code : 255, 255, 255"
            )
            ct.SetColorEntry(default_label_value, default_label_color)
    filein.close()
    return ct, conversion_list


def CreateIndexedColorImage(pszFilename,
                            fileL,
                            co_option=[],
                            output_pix_type=gdal.GDT_Byte,
                            labels_conversion=None,
                            logger=LOGGER) -> str:
    """
    from a labeled image (pszFilename), attribute a color described by fileL and save it next to pszFilename with the suffix _ColorIndexed
    IN :
        pszFileName [string] : path to the image of classification
        fileL [string] : path to the file.txt representing a color_table

    Notes
    -----
    return the indexed color image path
    """
    indataset = gdal.Open(pszFilename, gdal.GA_ReadOnly)
    if indataset is None:
        print('Could not open ' + pszFilename)
        sys.exit(1)
    outpath = pszFilename.split('/')
    if len(outpath) == 1:
        outname = os.getcwd() + '/' + outpath[0].split(
            '.')[0] + '_ColorIndexed.tif'
    else:
        outname = '/'.join(outpath[0:-1]) + '/' + outpath[-1].split(
            '.')[0] + '_ColorIndexed.tif'
    inband = indataset.GetRasterBand(1)
    gt = indataset.GetGeoTransform()
    driver = gdal.GetDriverByName("GTiff")
    outdataset = driver.Create(outname,
                               indataset.RasterXSize,
                               indataset.RasterYSize,
                               1,
                               output_pix_type,
                               options=co_option)
    if gt is not None and gt != (0.0, 1.0, 0.0, 0.0, 0.0, 1.0):
        outdataset.SetGeoTransform(gt)
    prj = indataset.GetProjectionRef()
    if prj is not None and len(prj) > 0:
        outdataset.SetProjection(prj)
    inarray = inband.ReadAsArray(0, 0)
    ct, convert_labels = CreateColorTable(fileL,
                                          labels_conversion=labels_conversion,
                                          logger=logger)

    outband = outdataset.GetRasterBand(1)
    outband.SetColorTable(ct)
    outband.WriteArray(inarray)
    logger.info(f"The file {outname} has been created")
    if convert_labels:
        convert_table_file = outname.replace(".tif", "_color_table.csv")
        write_color_table(convert_table_file, convert_labels)
        logger.info(f"The file {convert_table_file} has been created")
    return outname


if __name__ == "__main__":

    parser = argparse.ArgumentParser(
        description=
        "This function allow you to generate an image of classification with color"
    )
    parser.add_argument("-color ",
                        dest="color",
                        help="path to the color file (mandatory)",
                        required=True)
    parser.add_argument("-classification",
                        dest="pathClassification",
                        help="path to the image of classification",
                        required=True)
    args = parser.parse_args()

    CreateIndexedColorImage(args.pathClassification, args.color)
