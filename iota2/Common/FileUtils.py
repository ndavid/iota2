#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
""" This module contains a lot of useful file manipulation functions"""

import sys
import os
import csv
import shutil
import glob
import math
import logging
import random
from collections import defaultdict
from datetime import timedelta
import datetime
import errno
from typing import Optional, List, Tuple
import numpy as np
from config import Config
import osgeo
from osgeo import gdal
from osgeo import ogr
from osgeo import osr
# from osgeo.gdalconst import *
#~ import otbApplication as otb
from iota2.Common.Utils import run
from iota2.Common.Utils import remove_in_string_list

LOGGER = logging.getLogger("distributed.worker")


def detect_csv_delimiter(csv_file: str) -> str:
    """detect csv delimiter"""
    sniffer = csv.Sniffer()
    with open(csv_file, 'r') as csvfile:
        first_line = next(csvfile).rstrip()
    dialect = sniffer.sniff(first_line)
    delimiter = dialect.delimiter
    return delimiter


def find_and_replace_file(input_file: str, output_file: str, str_to_find: str,
                          new_str: str):
    """search string in file and replace by the new one into an output file
    """
    with open(input_file, 'r') as in_file:
        with open(output_file, 'w') as out_file:
            for line in in_file:
                out_file.write(line.replace(str_to_find, new_str))


def is_writable_directory(directory_path):
    """
    """
    out = True
    try:
        ensure_dir(directory_path)
    except:
        out = False
    return out


def get_iota2_project_dir():
    """
    """
    parent = os.path.abspath(
        os.path.join(os.path.dirname(os.path.realpath(__file__)), os.pardir))
    iota2dir = os.path.abspath(os.path.join(parent, os.pardir))
    return iota2dir


def ensure_dir(dirname, raise_exe=True):
    """
    Ensure that a named directory exists; if it does not, attempt to create it.
    """
    import errno
    try:
        os.makedirs(dirname)
    except OSError as e:
        if e.errno != errno.EEXIST:
            if raise_exe:
                raise


def getOutputPixType(nomencalture_path):
    """use to deduce de classifications pixels type
    
    Parameters
    ----------
    nomencalture_path : string
        path to the nomenclature
    
    Return
    ------
    string
        "uint8" or "uint16"
    """
    label_max = 0
    dico_format = {"uint8": 256, "uint16": 65536}

    with open(nomencalture_path, "r") as nomencalture_path_f:
        for line in nomencalture_path_f:
            label = int(line.rstrip().split(":")[-1].replace(" ", ""))
            if label < 0:
                raise Exception("labels must be > 0")
            if label > label_max:
                label_max = label

    if label <= dico_format["uint8"]:
        output_format = "uint8"
    elif label > dico_format["uint8"] and label < dico_format["uint16"]:
        output_format = "uint16"
    elif label_max > dico_format["uint16"]:
        raise Exception("label must inferior of 65536")
    return output_format


def WriteNewFile(newFile, fileContent):
    """
    """
    with open(newFile, "w") as new_f:
        new_f.write(fileContent)


def memory_usage_psutil(unit="MB"):
    # return the memory usage in MB
    import resource
    if unit == "MB":
        coeff = 1000.0
    elif unit == "GB":
        coeff = 1000.0 * 1000.0
    mem = resource.getrusage(resource.RUSAGE_SELF).ru_maxrss / coeff
    #print 'Memory usage: %s (MB)' % (mem)
    return mem


def dateInterval(dateMin, dataMax, tr):
    """
    dateMin [string] : Ex -> 20160101
    dateMax [string] > dateMin
    tr [int/string] -> temporal resolution
    """
    start = datetime.date(int(dateMin[0:4]), int(dateMin[4:6]),
                          int(dateMin[6:8]))
    end = datetime.date(int(dataMax[0:4]), int(dataMax[4:6]),
                        int(dataMax[6:8]))
    delta = timedelta(days=int(tr))
    curr = start
    while curr <= end:
        yield curr
        curr += delta


def updateDirectory(src, dst):

    content = os.listdir(src)
    for currentContent in content:
        if os.path.isfile(src + "/" + currentContent):
            if not os.path.exists(dst + "/" + currentContent):
                shutil.copy(src + "/" + currentContent,
                            dst + "/" + currentContent)
        if os.path.isdir(src + "/" + currentContent):
            if not os.path.exists(dst + "/" + currentContent):
                try:
                    shutil.copytree(src + "/" + currentContent,
                                    dst + "/" + currentContent)
                # python >2.5
                except OSError as exc:
                    if exc.errno == errno.ENOTDIR:
                        shutil.copy(src, dst)
                    else:
                        raise


def getDateLandsat(pathLandsat, tiles, sensor="Landsat8"):
    """
    Get the min and max dates for the given tile.
    """
    dateMin = 30000000000
    dateMax = 0
    for tile in tiles:
        #~ folder = os.listdir(pathLandsat + "/" + sensor + "_" + tile)
        folder = os.listdir(pathLandsat + "/" + tile)
        for i in range(len(folder)):
            if folder[i].count(".tgz") == 0 and folder[i].count(
                    ".jpg") == 0 and folder[i].count(".xml") == 0:
                #~ contenu = os.listdir(pathLandsat + "/" + sensor + "_" + tile + "/" + folder[i])
                contenu = os.listdir(pathLandsat + "/" + tile + "/" +
                                     folder[i])
                for i in range(len(contenu)):
                    if contenu[i].count(".TIF") != 0:
                        Date = int(contenu[i].split("_")[3])
                        if Date > dateMax:
                            dateMax = Date
                        if Date < dateMin:
                            dateMin = Date
    return str(dateMin), str(dateMax)


def getDateS2(pathS2, tiles):
    """
    Get the min and max dates for the given tile.
    """
    datePos = 2
    if "T" in tiles[0]:
        datePos = 1
    dateMin = 30000000000
    dateMax = 0
    for tile in tiles:
        folder = os.listdir(pathS2 + "/" + tile)
        for i in range(len(folder)):
            if folder[i].count(".tgz") == 0 and folder[i].count(
                    ".jpg") == 0 and folder[i].count(".xml") == 0:
                Date = int(folder[i].split("_")[datePos].split("-")[0])
                if Date > dateMax:
                    dateMax = Date
                if Date < dateMin:
                    dateMin = Date
    return str(dateMin), str(dateMax)


def getDateS2_S2C(pathS2, tiles):
    """
    Get the min and max dates for the given tile.
    """
    datePos = 2
    dateMin = 30000000000
    dateMax = 0
    for tile in tiles:
        folder = os.listdir(pathS2 + "/" + tile)
        for i in range(len(folder)):
            if folder[i].count(".tgz") == 0 and folder[i].count(
                    ".jpg") == 0 and folder[i].count(".xml") == 0:
                Date = int(folder[i].split("_")[datePos].split("T")[0])
                if Date > dateMax:
                    dateMax = Date
                if Date < dateMin:
                    dateMin = Date
    return str(dateMin), str(dateMax)


def unPackFirst(someListOfList):
    """
    python generator
    return first element of an iterable

    Example:
    ListOfLists = [[1, 2], [6], [0, 1, 2]]
    for first in unPackFirst(ListOfLists):
        print first
    >> 1
    >> 6
    >> 0
    """
    for values in someListOfList:
        if isinstance(values, list) or isinstance(values, tuple):
            yield values[0]
        else:
            yield values


def commonPixTypeToOTB(string):
    import otbApplication as otb
    dico = {
        "complexDouble": otb.ComplexImagePixelType_double,
        "complexFloat": otb.ComplexImagePixelType_float,
        "double": otb.ImagePixelType_double,
        "float": otb.ImagePixelType_float,
        "int16": otb.ImagePixelType_int16,
        "int32": otb.ImagePixelType_int32,
        "uint16": otb.ImagePixelType_uint16,
        "uint32": otb.ImagePixelType_uint32,
        "uint8": otb.ImagePixelType_uint8
    }
    try:
        return dico[string]
    except:
        raise Exception(
            "Error in commonPixTypeToOTB function input parameter : " +
            string + " not available, choices are :"
            "'complexDouble','complexFloat','double','float','int16','int32','uint16','uint32','uint8'"
        )


def splitList(InList, nbSplit):
    """
    IN :
    InList [list]
    nbSplit [int] : number of output fold

    OUT :
    splitList [list of nbSplit list]

    Examples :
        foo = ['a', 'b', 'c', 'd', 'e']
        print splitList(foo,4)
        >> [['e', 'c'], ['d'], ['a'], ['b']]

        print splitList(foo,8)
        >> [['b'], ['d'], ['c'], ['e'], ['a'], ['d'], ['a'], ['b']]
    """
    def chunk(xs, n):
        ys = list(xs)
        random.shuffle(ys)
        size = len(ys) // n
        leftovers = ys[size * n:]
        for c in range(n):
            if leftovers:
                extra = [leftovers.pop()]
            else:
                extra = []
            yield ys[c * size:(c + 1) * size] + extra

    splitList = list(chunk(InList, nbSplit))

    #check empty content (if nbSplit > len(Inlist))
    All = []
    for splits in splitList:
        for split in splits:
            if split not in All:
                All.append(split)

    for i in range(len(splitList)):
        if len(splitList[i]) == 0:
            randomChoice = random.sample(All, 1)[0]
            splitList[i].append(randomChoice)

    return splitList


def findCurrentTileInString(string, allTiles):
    """
    IN:
    string [string]: string where we want to found a string in the string list 'allTiles'
    allTiles [list of strings]

    OUT:
    if there is a unique occurence of a string in allTiles, return this occurence. else, return Exception
    """
    #must contain same element
    tileList = [
        currentTile for currentTile in allTiles if currentTile in string
    ]
    if len(set(tileList)) == 1:
        return tileList[0]
    else:
        raise Exception("more than one tile found into the string :'" +
                        string + "'")


def sortByFirstElem(MyList):
    """
    Example 1:
        MyList = [(1,2),(1,1),(6,1),(1,4),(6,7)]
        print sortByElem(MyList)
        >> [(1, [2, 1, 4]), (6, [1, 7])]

    Example 2:
        MyList = [((1,6),2),((1,6),1),((1,2),1),((1,6),4),((1,2),7)]
        print sortByElem(MyList)
        >> [((1, 2), [1, 7]), ((1, 6), [2, 1, 4])]
    """
    d = defaultdict(list)
    for k, v in MyList:
        d[k].append(v)
    return list(d.items())


def readRaster(name, data=False, band=1):
    """
    Open raster and return metadate information about it.

    in :
        name : raster name
    out :
        [datas] : numpy array from raster dataset
        xsize : xsize of raster dataset
        ysize : ysize of raster dataset
        projection : projection of raster dataset
        transform : coordinates and pixel size of raster dataset
    """

    try:
        if isinstance(name, str):
            raster = gdal.Open(name, 0)
        elif isinstance(name, osgeo.gdal.Dataset):
            raster = name
    except:
        print("Problem on raster file path")
        sys.exit()

    raster_band = raster.GetRasterBand(band)

    #property of raster
    projection = raster.GetProjectionRef()
    transform = raster.GetGeoTransform()
    xsize = raster.RasterXSize
    ysize = raster.RasterYSize

    if data:
        # convert raster to an array
        datas = raster_band.ReadAsArray()
        return datas, xsize, ysize, projection, transform
    else:
        return xsize, ysize, projection, transform


def arraytoRaster(array, output, model, driver='GTiff'):

    driver = gdal.GetDriverByName(driver)

    modelfile = readRaster(model, False)
    cols = modelfile[0]
    rows = modelfile[1]
    outRaster = driver.Create(output, cols, rows, 1, gdal.GDT_Byte)
    outRaster.SetGeoTransform((modelfile[3][0], modelfile[3][1], 0,
                               modelfile[3][3], 0, modelfile[3][5]))
    outband = outRaster.GetRasterBand(1)
    outband.WriteArray(array)
    outRasterSRS = osr.SpatialReference()
    outRasterSRS.ImportFromWkt(modelfile[2])
    outRaster.SetProjection(outRasterSRS.ExportToWkt())
    outband.FlushCache()


def getRasterResolution(rasterIn):
    """
    IN :
    rasterIn [string]:path to raster

    OUT :
    return pixelSizeX, pixelSizeY
    """

    if isinstance(rasterIn, str):
        if not os.path.isfile(rasterIn):
            return []
        raster = gdal.Open(rasterIn, gdal.GA_ReadOnly)
    elif isinstance(rasterIn, osgeo.gdal.Dataset):
        raster = rasterIn

    if raster is None:
        raise Exception("can't open " + rasterIn)
    geotransform = raster.GetGeoTransform()
    spacingX = geotransform[1]
    spacingY = geotransform[5]
    return spacingX, spacingY


def assembleTile_Merge(AllRaster: List[str],
                       spatialResolution: Tuple[float, float],
                       out: str,
                       ot="Int16",
                       co=None):
    """
    usage : function use to mosaic rasters

    IN :
    AllRaster [list of strings] : rasters path
    spatialResolution [int] :
    out [string] : output path
    ot [string] (not mandatory) : output pixelType (gdal format)
    co [dictionary] gdal rasters creation options (not mandatory)

    OUT:
    a mosaic of all images in AllRaster.
    0 values are considered as noData. Usefull for pixel superposition.
    """

    gdal_co = ""
    if co:
        gdal_co = " -co " + " -co ".join([
            "{}={}".format(co_name, co_value)
            for co_name, co_value in list(co.items())
        ])

    AllRaster = " ".join(AllRaster)
    if os.path.exists(out):
        os.remove(out)

    spx = float(spatialResolution[0])
    spy = -abs(float(spatialResolution[1]))

    cmd = "gdal_merge.py {} -ps {} {} -o {} -ot {} -n 0 {}".format(
        gdal_co, spx, spy, out, ot, AllRaster)
    run(cmd)


def getDateFromString(vardate):
    """
    usage get date from string date
    vardate [string] : ex : 20160101
    """
    Y = int(vardate[0:4])
    M = int(vardate[4:6])
    D = int(vardate[6:len(vardate)])
    return Y, M, D


def getNbDateInTile(dateInFile, display=True, raw_dates=False):
    """
    usage : get available dates in file
    dateInFile [string] : path to txt containing one date per line
    raw_dates [bool] flag use to return all available dates
    """
    allDates = []
    with open(dateInFile) as f:
        for i, l in enumerate(f):
            vardate = l.rstrip()
            try:
                Y, M, D = getDateFromString(vardate)
                validDate = datetime.datetime(int(Y), int(M), int(D))
                allDates.append(vardate)
                if display:
                    print(validDate)
            except ValueError:
                raise Exception("unvalid date in : " + dateInFile + " -> '" +
                                str(vardate) + "'")
    if raw_dates:
        output = allDates
    else:
        output = i + 1
    return output


def str2bool(value):
    """
    usage : use in argParse as function to parse options

    IN:
    value [string]
    out [bool]
    """
    import argparse
    if value.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif value.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')


def getRasterProjectionEPSG(FileName):
    """
    usage get raster EPSG projection code
    """
    SourceDS = gdal.Open(FileName, gdal.GA_ReadOnly)
    Projection = osr.SpatialReference()
    Projection.ImportFromWkt(SourceDS.GetProjectionRef())
    ProjectionCode = Projection.GetAttrValue("AUTHORITY", 1)
    return ProjectionCode


def getRasterNbands(raster):
    """
    usage get raster's number of bands
    """
    try:
        if isinstance(raster, str):
            src_ds = gdal.Open(raster)
        elif isinstance(raster, osgeo.gdal.Dataset):
            src_ds = raster
    except:
        print("Problem on raster file path")
        sys.exit()

    if src_ds is None:
        raise Exception(raster + " doesn't exist")

    return int(src_ds.RasterCount)


def getRasterExtent(raster_in):
    """
        Get raster extent of raster_in from GetGeoTransform()
        ARGs:
            INPUT:
                - raster_in: input raster
            OUTPUT
                - ex: extent with [minX,maxX,minY,maxY]
    """

    if isinstance(raster_in, str):
        if not os.path.isfile(raster_in):
            return []
        raster = gdal.Open(raster_in, gdal.GA_ReadOnly)
    elif isinstance(raster_in, osgeo.gdal.Dataset):
        raster = raster_in

    if raster is None:
        return []
    geotransform = raster.GetGeoTransform()
    originX = geotransform[0]
    originY = geotransform[3]
    spacingX = geotransform[1]
    spacingY = geotransform[5]
    r, c = raster.RasterYSize, raster.RasterXSize

    minX = originX
    maxY = originY
    maxX = minX + c * spacingX
    minY = maxY + r * spacingY

    return [minX, maxX, minY, maxY]


def matchGrid(coordinate, grid):
    """
    """
    interval_list = []
    pix_coordinate = None
    for cpt, value in enumerate(grid[:-1]):
        interval_list.append((value, grid[cpt + 1]))
    for index, (inf, sup) in enumerate(interval_list):
        if (coordinate > inf and coordinate < sup) or (coordinate < inf
                                                       and coordinate > sup):
            pix_coordinate = index
    return pix_coordinate


def geoToPix(raster, geoX, geoY, disp=False):
    """conver geographical coordinates to pixels

    Parameters
    ----------
    raster : string
        absolute path to an image
    geoX : float
        X geographic coordinate
    geoY : float
        Y geographic coordinate
    disp : bool
        flag to print coordinates
    """
    minXe, maxXe, minYe, maxYe = getRasterExtent(raster)
    spacingX, spacingY = getRasterResolution(raster)
    stepX = spacingX
    Xgrid = np.arange(minXe, maxXe + spacingX, spacingX)
    Ygrid = np.arange(maxYe, minYe + spacingY, spacingY)

    pixY = matchGrid(geoY, Ygrid)
    pixX = matchGrid(geoX, Xgrid)

    coordinates = pixX, pixY

    if pixX is None or pixY is None:
        coordinates = None

    if disp:
        disp_msg = "X : {}\nY : {}".format(pixX, pixY)
        if coordinates is None:
            disp_msg = "out of bounds"
        print(disp_msg)

    return coordinates


def gen_confusionMatrix(csv_f, AllClass):
    """
    IN:
    csv_f [list of list] : comes from confCoordinatesCSV function.
    AllClass [list of strings] : all class
    OUT :
    confMat [numpy array] : generate a numpy array representing a confusion matrix
    """
    NbClasses = len(AllClass)

    confMat = [[0] * NbClasses] * NbClasses
    confMat = np.asarray(confMat)
    row = 0
    for classRef in AllClass:
        #in order to manage the case "this reference label was never classified"
        flag = 0
        for classRef_csv in csv_f:
            if classRef_csv[0] == classRef:
                col = 0
                for classProd in AllClass:
                    for classProd_csv in classRef_csv[1]:
                        if classProd_csv[0] == classProd:
                            confMat[row][
                                col] = confMat[row][col] + classProd_csv[1]
                    col += 1
                #row +=1
        row += 1
        #if flag == 0:
        #   row+=1

    return confMat


def confCoordinatesCSV(csvPaths):
    """
    IN :
    csvPaths [string] : list of path to csv files
        ex : ["/path/to/file1.csv","/path/to/file2.csv"]
    OUT :
    out [list of lists] : containing csv's coordinates

    ex : file1.csv
        #Reference labels (rows):11
        #Produced labels (columns):11,12
        14258,52

         file2.csv
        #Reference labels (rows):12
        #Produced labels (columns):11,12
        38,9372

    out = [[12,[11,38]],[12,[12,9372]],[11,[11,14258]],[11,[12,52]]]
    """
    out = []
    for csvPath in csvPaths:
        cpty = 0
        FileMat = open(csvPath, "r")
        while 1:
            data = FileMat.readline().rstrip('\n\r')
            if data == "":
                FileMat.close()
                break
            if data.count('#Reference labels (rows):') != 0:
                ref = data.split(":")[-1].split(",")
            elif data.count('#Produced labels (columns):') != 0:
                prod = data.split(":")[-1].split(",")
            else:
                y = ref[cpty]
                line = data.split(",")
                cptx = 0
                for val in line:
                    x = prod[cptx]
                    out.append([int(y), [int(x), float(val)]])
                    cptx += 1
                cpty += 1
    return out


def getListTileFromModel(modelIN, pathToConfig):
    """
    IN :
        modelIN [string] : model name (generally an integer)
        pathToConfig [string] : path to the configuration file which link a model and all tiles uses to built him.
    OUT :
        list of tiles uses to built "modelIN"

    Exemple
    $cat /path/to/myConfigFile.cfg
    AllModel:
    [
        {
        modelName:'1'
        tilesList:'D0005H0001 D0005H0002'
        }
        {
        modelName:'22'
        tilesList:'D0004H0004 D0005H0008'
        }
    ]
    tiles = getListTileFromModel('22',/path/to/myConfigFile.cfg)
    print tiles
    >>tiles = ['D0004H0004','D0005H0008']
    """
    f = open(pathToConfig)
    cfg = Config(f)
    AllModel = cfg.AllModel

    for model in AllModel.data:
        if model.modelName == modelIN:
            return model.tilesList.split("_")


@remove_in_string_list(".aux.xml", ".sqlite-journal")
def fileSearchRegEx(Pathfile):
    """
    get files by regEx
    """
    return [f for f in glob.glob(Pathfile.replace("[", "[[]"))]


# def getFeatStackName(list_indices: List[str],
def get_feat_stack_name(list_indices: List[str],
                        user_feat_path: str,
                        user_feat_pattern: Optional[str] = None) -> str:
    """
    usage : get Feature Stack name
    Parameters
    ----------
    list_indices: list(string)
        the features list
    user_feat_path: string
        the path to images
    user_feat_pattern: string
        contains features name separated by comma
    Return
    ------
    string: the image stack name
    """

    # if user_feat_path:
    #     user_feat_path = None
    if user_feat_pattern is None:
        user_feat_pattern = ""
    else:
        user_feat_pattern = "_".join(user_feat_pattern.split(","))

    stack_ind = f"SL_MultiTempGapF{user_feat_pattern}.tif"
    return_list_feat = True
    if len(list_indices) > 1:
        list_indices = list(list_indices)
        list_indices = sorted(list_indices)
        list_feat = "_".join(list_indices)
    elif len(list_indices) == 1:
        list_feat = list_indices[0]
    else:
        return_list_feat = False

    if return_list_feat is True:
        stack_ind = f"SL_MultiTempGapF_{list_feat}_{user_feat_pattern}_.tif"
    return stack_ind


def writeCmds(path, cmds, mode="w"):

    cmdFile = open(path, mode)
    for i in range(len(cmds)):
        if i == 0:
            cmdFile.write("%s" % (cmds[i]))
        else:
            cmdFile.write("\n%s" % (cmds[i]))
    cmdFile.close()


def getCmd(path):
    with open(path, "r") as f:
        All_cmd = [line for line in f]
    return All_cmd


@remove_in_string_list(".aux.xml", ".sqlite-journal")
def FileSearch_AND(PathToFolder, AllPath, *names):
    """
        search all files in a folder or sub folder which contains all names in their name

        IN :
            - PathToFolder : target folder
                    ex : /xx/xxx/xx/xxx
            - *names : target names
                    ex : "target1","target2"
        OUT :
            - out : a list containing all file name (without extension) which are containing all name
    """
    out = []
    for path, dirs, files in os.walk(PathToFolder, followlinks=True):
        for i in range(len(files)):
            flag = 0
            for name in names:
                if files[i].count(name) != 0:
                    flag += 1
            if flag == len(names):
                if not AllPath:
                    out.append(files[i].split(".")[0])
                else:
                    pathOut = path + '/' + files[i]
                    out.append(pathOut)
    return out


def GetSerieList(*SerieList):
    """
    Returns a list of images likes a character chain.
    ARGs:
        INPUT:
            -SerieList: the list of different series
        OUTPUT:
    """
    ch = ""
    for serie in SerieList:
        name = serie.split('.')
        ch = ch + serie + " "
    return ch
