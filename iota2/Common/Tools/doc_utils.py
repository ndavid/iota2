#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
import os

from iota2.Common.FileUtils import get_iota2_project_dir


def create_init_dictionary():
    import iota2.configuration_files.default_config_parameters as mod
    from inspect import getmembers, isclass
    attr_list = [
        fun for fun in getmembers(mod)
        if isclass(fun[1]) and fun[0].startswith("init")
    ]
    d = {}
    for attr in attr_list:
        param = attr[1]()
        d[param.name] = (param.value, param.desc, param.type_exp,
                         param.long_desc)
    return d


def create_doc_dictionary():
    import iota2.configuration_files.default_config_parameters as mod
    from inspect import getmembers, isclass
    attr_list = [
        fun for fun in getmembers(mod)
        if isclass(fun[1]) and fun[0].startswith("init")
    ]
    d = {}
    for attr in attr_list:
        param = attr[1]()

        for builder in param.builders:
            d[builder, param.section,
              param.name] = (param.value, param.desc, param.type_exp,
                             param.long_desc)
    return d


def convert_description_to_rst(input_dictionnary):

    keys = input_dictionnary.keys()
    # print(keys)
    keys = sorted(keys)

    list_of_values = ["Default value"]
    list_of_desc = ["Description"]
    list_of_type = ["Type"]
    max_len_desc = len("Description")
    max_len_key = len("Parameter")
    max_len_val = len("Default value")
    max_len_type = len("Type")
    # val and description are stored in list to determine le maximum
    # lenght of a row
    for key in keys:
        value, desc, type_exp, _ = input_dictionnary[key]
        if len(key) > max_len_key:
            max_len_key = len(key)
        if len(desc) > max_len_desc:
            max_len_desc = len(desc)

        if len(str(type_exp)) > max_len_type:
            max_len_type = len(str(type_exp))

        if key == "builder_file_path" or key == "module":
            value = "/path/to/iota2/sources"
        # else:
        list_of_values.append(str(value))

        if len(str(value)) > max_len_val:
            max_len_val = len(str(value))
        list_of_desc.append(desc)
        list_of_type.append(type_exp.__name__)

    table = ("+" + "-" * (max_len_key + 2) + "+" + "-" * (max_len_type + 2) +
             "+" + "-" * (max_len_val + 2) + "+" + "-" * (max_len_desc + 2) +
             "+\n")

    keys = ["Parameter"] + keys
    for i, key in enumerate(keys):
        table += "| " + key + " " * (
            max_len_key - len(key)) + " | " + list_of_type[i] + " " * (
                max_len_type - len(list_of_type[i])
            ) + " | " + list_of_values[i] + " " * (max_len_val - len(
                list_of_values[i])) + " | " + list_of_desc[i] + " " * (
                    max_len_desc - len(list_of_desc[i])) + " |\n"
        if i == 0:
            table += ("+" + "=" * (max_len_key + 2) + "+" + "=" *
                      (max_len_type + 2) + "+" + "=" * (max_len_val + 2) +
                      "+" + "=" * (max_len_desc + 2) + "+\n")

        else:
            table += ("+" + "-" * (max_len_key + 2) + "+" + "-" *
                      (max_len_type + 2) + "+" + "-" * (max_len_val + 2) +
                      "+" + "-" * (max_len_desc + 2) + "+\n")
    return table


def generate_builder_doc(in_dico, output_path):

    keys = in_dico.keys()

    builders = {builder for builder, _, _ in keys}
    # print(builders)
    for builder in builders:
        output_file = os.path.join(output_path, builder + "_builder.rst")
        with open(output_file, "w") as out_file:
            out_file.write(f"{builder}\n")
            out_file.write("#" * len(builder) + "\n" * 2)
            partial_keys = [key for key in keys if builder in key]
            partial_keys.sort()
            # print("partial_keys ", partial_keys)
            blocs = {bloc for _, bloc, _ in partial_keys}
            blocs = sorted(blocs)
            for bloc in blocs:
                partial_bloc_keys = [
                    key for key in partial_keys if bloc in key
                ]
                # print(partial_bloc_keys)
                partial_dict = {
                    param: in_dico[build, blo, param]
                    for build, blo, param in partial_bloc_keys
                }
                # print(partial_dict)
                table = convert_description_to_rst(partial_dict)
                # print(table)

                out_file.write(f"{bloc}\n")
                # Two \n to ensure rst formating
                out_file.write("=" * (len(bloc)) + "\n\n")
                out_file.write(table)
                out_file.write("\n" * 2)
                notes_flag = True
                for param in partial_dict.items():
                    key, value = param
                    if value[3] is not None:
                        if notes_flag:
                            out_file.write("Notes\n")
                            out_file.write("-" * len("Notes") + "\n\n")
                            notes_flag = False
                        out_file.write(key + "\n")
                        out_file.write("^" * len(key) + "\n" * 2)
                        out_file.write(value[3] + "\n" * 3)

    return 0


def generate_all_parameters_doc(output_path):
    dico = create_init_dictionary()
    table = convert_description_to_rst(dico)
    with open(os.path.join(output_path, "all_parameters.rst"),
              "w") as out_file:
        title = "All configuration parameters"
        out_file.write(title + "\n")
        out_file.write("#" * len(title) + "\n\n")
        out_file.write(table)
        out_file.write("\n")


def generate_list_steps_files(iota2_dir, output_path):
    """
    Load configurations files and write corresponding
    steps rst files
    """
    # Classif tutorial
    config_file = os.path.join(output_path, "examples",
                               "config_tutorial_classification.cfg")
    steps_file = os.path.join(output_path, "examples",
                              "steps_classification.txt")
    os.system(f"Iota2.py -config {config_file} -only_summary > {steps_file}")
    # Vecto tutorial
    config_file = os.path.join(output_path, "examples",
                               "config_tutorial_vectorization.cfg")
    steps_file = os.path.join(output_path, "examples",
                              "steps_vectorization.txt")
    os.system(f"Iota2.py -config {config_file} -only_summary > {steps_file}")
    # features maps
    config_file = os.path.join(output_path, "examples",
                               "config_tutorial_features_maps.cfg")
    steps_file = os.path.join(output_path, "examples",
                              "steps_features_maps.txt")
    os.system(f"Iota2.py -config {config_file} -only_summary > {steps_file}")


def generate_doc_files():
    iota2_dir = get_iota2_project_dir()
    output_path = os.path.join(iota2_dir, "doc", "source")
    if not os.path.exists(output_path):
        os.mkdir(output_path)
    dico = create_doc_dictionary()
    generate_builder_doc(dico, output_path)
    generate_all_parameters_doc(output_path)
    generate_list_steps_files(iota2_dir, output_path)


if __name__ == "__main__":

    generate_doc_files()
