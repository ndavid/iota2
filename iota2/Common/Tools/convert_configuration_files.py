#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================


class conversion_config_file():
    """ This is the conversion module for configuration file
    for lazy or dizzy user"""
    def __init__(self):
        self.conversion_dict = {
            "outputStatistics": "output_statistics",
            "l5path_old": "l5_path_old",
            "L5_old_output_path": "l5_old_output_path",
            "L8Path": "l8_path",
            "L8_output_path": "l8_output_path",
            "L8Path_old": "l8_path_old",
            "L8_old_output_path": "l8_old_output_path",
            "S2Path": "s2_path",
            "S2_output_path": "s2_output_path",
            "S2_S2C_Path": "s2_s2c_path",
            "S2_S2C_output_path": "s2_s2c_output_path",
            "S2_L3A_Path": "s2_l3a_path",
            "S2_L3A_output_path": "s2_l3a_output_path",
            "S1Path": "s1_path",
            "userFeatPath": "user_feat_path",
            "runs": "runs",
            "cloud_threshold": "cloud_threshold",
            "splitGroundTruth": "split_ground_truth",
            "ratio": "ratio",
            "random_seed": "random_seed",
            "firstStep": "first_step",
            "lastStep": "last_step",
            "mode_outside_RegionSplit": "mode_outside_regionsplit",
            "loggerLevel": "logger_level",
            "regionPath": "region_path",
            "regionField": "region_field",
            # "enableConsole": "enable_console",
            "merge_final_classifications": "merge_final_classifications",
            "merge_final_classifications_method":
            "merge_final_classifications_method",
            "merge_final_classifications_undecidedlabel":
            "merge_final_classifications_undecidedlabel",
            "fusionOfClassificationAllSamplesValidation":
            "fusionofclassification_all_samples_validation",
            "dempstershafer_mob": "dempstershafer_mob",
            "merge_final_classifications_ratio":
            "merge_final_classifications_ratio",
            "keep_runs_results": "keep_runs_results",
            "check_inputs": "check_inputs",
            "enable_autoContext": "enable_autocontext",
            "autoContext_iterations": "autocontext_iterations",
            "force_standard_labels": "force_standard_labels",
            # remove spatial resolution from init
            "spatialResolution": "spatial_resolution",
            "remove_outputPath": "remove_output_path",
            "outputPath": "output_path",
            "nomenclaturePath": "nomenclature_path",
            "listTile": "list_tile",
            "groundTruth": "ground_truth",
            "dataField": "data_field",
            "colorTable": "color_table",
            "VHRPath": "vhr_path",
            "dateVHR": "date_vhr",
            "dateSrc": "date_src",
            "bandRef": "band_ref",
            "bandSrc": "band_src",
            "resample": "resample",
            "step": "step",
            "minstep": "minstep",
            "minsiftpoints": "minsiftpoints",
            "iterate": "iterate",
            "prec": "prec",
            "mode": "mode",
            "pattern": "pattern",
            "model_type": "model_type",
            "cross_validation_folds": "cross_validation_folds",
            "cross_validation_grouped": "cross_validation_grouped",
            "standardization": "standardization",
            "cross_validation_parameters": "cross_validation_parameters",
            "sampleSelection": "sample_selection",
            "sampleAugmentation": "sample_augmentation",
            "sampleManagement": "sample_management",
            "dempster_shafer_SAR_Opt_fusion": "dempster_shafer_sar_opt_fusion",
            "cropMix": "crop_mix",
            "prevFeatures": "prev_features",
            "outputPrevFeatures": "output_prev_features",
            "annualCrop": "annual_crop",
            "ACropLabelReplacement": "a_crop_label_replacement",
            "samplesClassifMix": "samples_classif_mix",
            "classifier": "classifier",
            "options": "options",
            "annualClassesExtractionSource":
            "annual_classes_extraction_source",
            "validityThreshold": "validity_threshold",
            "noLabelManagement": "no_label_management",
            "enable_probability_map": "enable_probability_map",
            "fusionOptions": "fusion_options",
            "classifMode": "classif_mode",
            "features": "features",
            "autoDate": "auto_date",
            "writeOutputs": "write_outputs",
            "useAdditionalFeatures": "use_additional_features",
            "useGapFilling": "use_gapfilling",
            "proj": "proj",
            "copyinput": "copy_input",
            "relrefl": "rel_refl",
            "keepduplicates": "keep_duplicates",
            "extractBands": "extract_bands",
            "acorfeat": "acor_feat",
            "dimRed": "dim_red",
            "targetDimension": "target_dimension",
            "reductionMode": "reduction_mode",
            "additionalFeatures": "additional_features",
            "temporalResolution": "temporal_resolution",
            "write_reproject_resampled_input_dates_stack":
            "write_reproject_resampled_input_dates_stack",
            "startDate": "start_date",
            "endDate": "end_date",
            "keepBands": "keep_bands",
            "patterns": "patterns",
            "arbo": "arbo",
            "classification": "classification",
            "confidence": "confidence",
            "validity": "validity",
            "seed": "seed",
            "umc1": "umc1",
            "umc2": "umc2",
            "inland": "inland",
            "rssize": "rssize",
            "lib64bit": "lib64bit",
            "gridsize": "gridsize",
            "grasslib": "grasslib",
            "douglas": "douglas",
            "hermite": "hermite",
            "mmu": "mmu",
            "angle": "angle",
            "clipfile": "clipfile",
            "clipfield": "clipfield",
            "clipvalue": "clipvalue",
            "outprefix": "outprefix",
            "lcfield": "lcfield",
            "blocksize": "blocksize",
            "dozip": "dozip",
            "bingdal": "bingdal",
            "chunk": "chunk",
            "systemcall": "systemcall",
            "nomenclature": "nomenclature",
            "statslist": "statslist",
            "external_features_flag": "external_features_flag",
            "module": "module",
            "functions": "functions",
            "number_of_chunks": "number_of_chunks",
            "chunk_size_x": "chunk_size_x",
            "chunk_size_y": "chunk_size_y",
            "chunk_size_mode": "chunk_size_mode",
            "concat_mode": "concat_mode",
            "output_name": "output_name",
            "allowed_retry": "allowed_retry",
            "maximum_ram": "maximum_ram",
            "maximum_cpu": "maximum_cpu",
            "builders_class_name": "builders_class_name",
            "builders_paths": "builders_paths",
            # section name
            "chain": "chain",
            "Simplification": "simplification",
            "argTrain": "arg_train",
            "coregistration": "coregistration",
            "scikit_models_parameters": "scikit_models_parameters",
            "argClassification": "arg_classification",
            "iota2FeatureExtraction": "iota2_feature_extraction",
            "Landsat8": "Landsat8",
            "Landsat8_old": "Landsat8_old",
            "Landsat5_old": "Landsat5_old",
            "Sentinel_2": "Sentinel_2",
            "Sentinel_2_S2C": "Sentinel_2_S2C",
            "Sentinel_2_L3A": "Sentinel_2_L3A",
            "userFeat": "userFeat",
            "external_features": "external_features",
            "sensors_data_interpolation": "sensors_data_interpolation",
            "builders": "builders",
            "task_retry_limits": "task_retry_limits"
        }
        self.deprecated_params = [
            "pyAppPath", "logFileLevel", "enableConsole", "model",
            "remove_tmp_files", "jobsPath", "chainName", "enableCrossValidation"
        ]

        self.change_bloc_dict = {
            "runs": "arg_train",
            "splitGroundTruth": "arg_train",
            "ratio": "arg_train",
            "random_seed": "arg_train",
            "mode_outside_RegionSplit": "arg_train",
            "merge_final_classifications": "arg_classification",
            "merge_final_classifications_method": "arg_classification",
            "merge_final_classifications_undecidedlabel": "arg_classification",
            "fusionOfClassificationAllSamplesValidation": "arg_classification",
            "dempstershafer_mob": "arg_classification",
            "merge_final_classifications_ratio": "arg_classification",
            "keep_runs_results": "arg_classification",
            "enable_autoContext": "arg_train",
            "autoContext_iterations": "arg_train",
            "force_standard_labels": "arg_train",

            # glob_chain
            "features": "arg_train",
            "autoDate": "sensors_data_interpolation",
            "writeOutputs": "sensors_data_interpolation",
            "useAdditionalFeatures": "sensors_data_interpolation",
            "useGapFilling": "sensors_data_interpolation",
            "proj": "chain"
        }

    def convert(self, config_file: str, out_config_file: str):
        """
        Apply snake_case changes to a configuration file
        Parameter
        ---------
        config_file:
            the file to be converted
        out_config_file:
            the configuration file converted
        """
        from config import Config
        message = ("Lists are stored without comma, "
                   "but config understand both format")
        print("#" * (len(message) + 4))
        print("# Typo Warning : " + " " *
              (len(message) - len("# Typo Warning : ")) + "   #")
        print(f"# {message} #")
        print("#" * (len(message) + 4))

        cfg = Config(open(config_file))
        output_file = open(out_config_file, "w")
        # new_sec_interp_keys = [
        #     "autoDate", "writeOutputs", "useAdditionalFeatures",
        #     "useGapFilling"
        # ]

        old_keys = cfg.keys()

        # Get all section needed
        section = []
        for key in old_keys:

            for param in cfg[key]:
                if param in self.change_bloc_dict.keys():
                    sec = self.change_bloc_dict[param]
                    if sec not in section:
                        section.append(sec)
                else:
                    if key in self.conversion_dict.keys():
                        nkey = self.conversion_dict[key]
                        if nkey not in section:
                            section.append(nkey)
                    else:
                        print(f"Old section detected: {key}")
        for key in section:
            output_file.write(f"{key}" + ":{}\n")
        output_file.close()
        cfg_out = Config(open(out_config_file))

        for old_bloc in old_keys:
            for param in cfg[old_bloc].keys():
                if param in self.deprecated_params:
                    print(f"INFO: Deprecated parameter detected : {param}."
                          " It was removed.")
                    continue
                if param not in self.conversion_dict.keys():
                    print(
                        f"WARNING: Parameter {param} not found in dictionnary."
                        " Please check the output file")
                    # if line is unknown write the line
                    cfg_out[self.conversion_dict[old_bloc]][param] = cfg[
                        old_bloc][param]
                else:
                    if param in self.change_bloc_dict.keys():
                        nbloc = self.change_bloc_dict[param]
                    else:
                        nbloc = self.conversion_dict[old_bloc]
                    cfg_out[nbloc][
                        self.conversion_dict[param]] = cfg[old_bloc][param]
        cfg_out.save(open(out_config_file, "w"))


if __name__ == "__main__":
    import sys
    converter = conversion_config_file()
    print(f"Input : {sys.argv[1]}, output {sys.argv[2]}")
    converter.convert(sys.argv[1], sys.argv[2])
