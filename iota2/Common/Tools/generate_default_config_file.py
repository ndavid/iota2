#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
import argparse
from typing import Optional
from inspect import getmembers, isclass
from config import Config
from iota2.configuration_files import default_config_parameters as dcp


def generate_default_config_file(output_path: Optional[str] = None):

    param_class_list = [
        fun for fun in getmembers(dcp)
        if isclass(fun[1]) and fun[0].startswith("init")
    ]

    sections = []
    dict_param = {}
    for fun in param_class_list:
        param = fun[1]()
        if param.section not in sections:
            sections.append(param.section)
        dict_param[param.section, param.name] = param.value

    # Prepare writing
    chain_message = ("# Default configuration file\n\n")
    sections = sorted(sections)
    for section in sections:
        partial_keys = [key for key in dict_param.keys() if section in key]
        chain_message += (f"{section}:" + "{\n")
        for key in partial_keys:
            _, param = key
            value = dict_param[key]
            if value == "":
                print("emppty string")

                chain_message += f"\t{param}:" + " \"\" \n"
            else:
                if isinstance(value, str):
                    chain_message += f"\t{param}: \"{value}\"\n"
                else:
                    chain_message += f"\t{param}: {value}\n"
        chain_message += ("}\n")

    #print(chain_message)
    if output_path:
        with open(output_path, "w") as outfile:
            outfile.write(chain_message)

    return chain_message


def display_complete_config_file(in_file):
    cfg = Config(open(in_file))
    param_class_list = [
        fun for fun in getmembers(dcp)
        if isclass(fun[1]) and fun[0].startswith("init")
    ]

    sections = []
    dict_param = {}
    for fun in param_class_list:
        param = fun[1]()
        if param.section not in sections:
            sections.append(param.section)
        dict_param[param.section, param.name] = param.value

    # Prepare writing
    chain_message = ("# Default configuration file\n\n")
    sections = sorted(sections)
    for section in sections:
        partial_keys = [key for key in dict_param.keys() if section in key]
        chain_message += (f"{section}:" + "{\n")
        for key in partial_keys:
            _, param = key
            if section in cfg.keys():
                if param in cfg[section]:
                    value = cfg[section][param]
                    print(value)
                else:
                    value = dict_param[key]
            else:
                value = dict_param[key]
            if value == "":
                print("emppty string")

                chain_message += f"\t{param}:" + " \"\" \n"
            else:
                if isinstance(value, str):
                    chain_message += f"\t{param}: \"{value}\"\n"
                else:
                    chain_message += f"\t{param}: {value}\n"
        chain_message += ("}\n")

    print(chain_message)

    return chain_message


if __name__ == "__main__":
    PARSER = argparse.ArgumentParser(description="Create a configuration file")
    PARSER.add_argument(
        "-out_file",
        default=None,
        dest="out_file",
        required=False,
        help="An optional output file to write default config file")
    ARGS = PARSER.parse_args()
    generate_default_config_file(ARGS.out_file)
