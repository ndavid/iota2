#!/usr/bin/env python3
#-*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""module to manage fusion of classifications in iota2"""
import os
import shutil
import logging
from logging import Logger
from typing import List, Dict, Optional
from iota2.Common import FileUtils as fu
from iota2.Common.Utils import run

LOGGER = logging.getLogger("distributed.worker")


def dempster_shafer_fusion_parameters(iota2_dir: str) -> List[Dict[str, str]]:
    """
    Feed dempster_shafer_fusion function

    from the iota2 output directory, return parameter needed to compute
    a fusion of classifcations by dempster-shafer method

    Return a list of dict containing keys {"sar_classif", "opt_classif",
                                      "sar_model", "opt_model"}
    Parameters
    ----------
    iota2_dir :
        iota2 output directory

    """
    classif_seed_pos = 5
    classif_tile_pos = 1
    classif_model_pos = 3

    iota2_ds_confusions_dir = os.path.join(iota2_dir, "dataAppVal", "bymodels")
    iota2_classif_dir = os.path.join(iota2_dir, "classif")
    classifications = fu.FileSearch_AND(iota2_classif_dir, True, "Classif",
                                        ".tif")
    # group by models
    model_group = []
    for classif in classifications:
        classif_name = os.path.basename(classif)
        classif_seed = classif_name.split("_")[classif_seed_pos].replace(
            ".tif", "")
        classif_model = classif_name.split("_")[classif_model_pos]
        classif_tile = classif_name.split("_")[classif_tile_pos]
        key_param = (classif_seed, classif_model, classif_tile)
        model_group.append((key_param, classif))
    raster_group = [param for key, param in fu.sortByFirstElem(model_group)]

    out_parameters = []
    for raster_model in raster_group:
        for raster in raster_model:
            raster_name = os.path.basename(raster)
            classif_seed = raster_name.split("_")[classif_seed_pos].replace(
                ".tif", "")
            classif_model = raster_name.split("_")[classif_model_pos]
            if "SAR.tif" in raster_name:
                sar_model = fu.fileSearchRegEx(
                    os.path.join(
                        iota2_ds_confusions_dir,
                        "model_{}_seed_{}_SAR.csv".format(
                            classif_model, classif_seed)))[0]
                sar_classif = raster
            else:
                opt_model = fu.fileSearchRegEx(
                    os.path.join(
                        iota2_ds_confusions_dir,
                        "model_{}_seed_{}.csv".format(classif_model,
                                                      classif_seed)))[0]
                opt_classif = raster
        out_parameters.append({
            "sar_classif": sar_classif,
            "opt_classif": opt_classif,
            "sar_model": sar_model,
            "opt_model": opt_model
        })
    return out_parameters


def perform_fusion(fusion_dic: Dict[str, str],
                   mob: str,
                   classif_model_pos: int,
                   classif_tile_pos: int,
                   classif_seed_pos: int,
                   working_directory: str,
                   logger: Optional[Logger] = LOGGER) -> str:
    """
    From classifications, perform the DS fusion of classifications
    Return the output path the the fusion of classifications
    Parameters
    ----------
    fusion_dic :
        dictionnary containing keys : "sar_classif", "opt_classif", "sar_model"
        "opt_model"
    mob :
        Dempster-Shafer's mass of belive
    classif_model_pos :
        position of the model's name in classification's name if
        splited by '_'
    classif_tile_pos :
        position of the tile's name in classification's name if
        splited by '_'
    classif_seed_pos :
        position of the seed number in classification's name if
        splited by '_'
    working_directory :
        path to a working directory

    """
    from iota2.Common import OtbAppBank

    model = os.path.basename(
        fusion_dic["sar_classif"]).split("_")[classif_model_pos]
    seed = os.path.basename(
        fusion_dic["sar_classif"]).split("_")[classif_seed_pos]
    tile = os.path.basename(
        fusion_dic["sar_classif"]).split("_")[classif_tile_pos]
    classif_dir, _ = os.path.split(fusion_dic["sar_classif"])

    sar_opt_fus_name = "Classif_{}_model_{}_seed_{}_DS.tif".format(
        tile, model, seed)
    sar_opt_fus = os.path.join(classif_dir, sar_opt_fus_name)
    if working_directory:
        sar_opt_fus = os.path.join(working_directory, sar_opt_fus_name)
    im_list = [fusion_dic["sar_classif"], fusion_dic["opt_classif"]]
    csv_list = [fusion_dic["sar_model"], fusion_dic["opt_model"]]

    fusion_parameters = {
        "il": im_list,
        "method": "dempstershafer",
        "method.dempstershafer.mob": mob,
        "method.dempstershafer.cmfl": csv_list,
        "out": sar_opt_fus
    }

    ds_fus = OtbAppBank.CreateFusionOfClassificationsApplication(
        fusion_parameters)
    if not os.path.exists(os.path.join(classif_dir, sar_opt_fus_name)):
        logger.info(f"computing : {sar_opt_fus}")
        ds_fus.ExecuteAndWriteOutput()
        logger.debug(f"{sar_opt_fus} DONE")
        if working_directory:
            shutil.copy(sar_opt_fus, os.path.join(classif_dir,
                                                  sar_opt_fus_name))
            os.remove(sar_opt_fus)
    return os.path.join(classif_dir, sar_opt_fus_name)


def compute_fusion_choice(iota2_dir: str,
                          fusion_dic: Dict[str, str],
                          fusion_class: str,
                          classif_model_pos: int,
                          classif_tile_pos: int,
                          classif_seed_pos: int,
                          ds_choice_both: int,
                          ds_choice_sar: int,
                          ds_choice_opt: int,
                          ds_no_choice: int,
                          working_directory: str,
                          logger: Optional[Logger] = LOGGER) -> str:
    """
    using the resulting fusion of classification and originals classifications,
    generate a raster which determine which input was chosen
    Return the output path.
    Parameters
    ----------

    iota2_dir :
        iota2's output path
    fusion_dic :
        dictionnary containing keys : "sar_classif", "opt_classif", "sar_model"
        "opt_model"
    fusion_class :
        path to the fusion of classifications
    classif_model_pos :
        position of the model's name in classification's name if
        splited by '_'
    classif_tile_pos :
        position of the tile's name in classification's name if
        splited by '_'
    classif_seed_pos :
        position of the seed number in classification's name if
        splited by '_'
    ds_choice_both :
        output value if fusion of classifications get the same label than
        SAR classification and optical classification
    ds_choice_sar :
        output value if fusion of classifications get the same label than
        SAR classification
    ds_choice_opt :
        output value if fusion of classifications get the same label than
        optical classification
    ds_no_choice :
        default case
    working_directory :
        path to a working directory
    logger :
        root logger

    """
    from iota2.Common import OtbAppBank

    logger.info("compute fusion choice")
    model = os.path.basename(
        fusion_dic["sar_classif"]).split("_")[classif_model_pos]
    seed = os.path.basename(
        fusion_dic["sar_classif"]).split("_")[classif_seed_pos]
    tile = os.path.basename(
        fusion_dic["sar_classif"]).split("_")[classif_tile_pos]
    im_list = [
        fusion_dic["sar_classif"], fusion_dic["opt_classif"], fusion_class
    ]
    choice_exp = (f"im1b1==im3b1 and im2b1==im3b1?{ds_choice_both}:im1b1"
                  f"==im3b1?{ds_choice_sar}:im2b1==im3b1?"
                  f"{ds_choice_opt}:{ds_no_choice}")
    ds_choice_name = "DSchoice_{}_model_{}_seed_{}.tif".format(
        tile, model, seed)
    ds_choice_dir = os.path.join(iota2_dir, "final", "TMP")
    if not os.path.exists(ds_choice_dir):
        try:
            os.mkdir(ds_choice_dir)
        except OSError:
            pass
    ds_choice = os.path.join(ds_choice_dir, ds_choice_name)
    if working_directory:
        ds_choice = os.path.join(working_directory, ds_choice_name)
    ds_choice_params = {
        "il": im_list,
        "out": ds_choice,
        "exp": choice_exp,
        "pixType": "uint8"
    }
    choice = OtbAppBank.CreateBandMathApplication(ds_choice_params)
    if not os.path.exists(os.path.join(ds_choice_dir, ds_choice_name)):
        logger.info(f"computing : {ds_choice}")
        choice.ExecuteAndWriteOutput()
        logger.debug(f"{ds_choice} : DONE")
        if working_directory:
            shutil.copy(ds_choice, os.path.join(ds_choice_dir, ds_choice_name))
            os.remove(ds_choice)
    return os.path.join(ds_choice_dir, ds_choice_name)


def compute_confidence_fusion(fusion_dic: Dict[str, str],
                              ds_choice: str,
                              classif_model_pos: int,
                              classif_tile_pos: int,
                              classif_seed_pos: int,
                              ds_choice_both: int,
                              ds_choice_sar: int,
                              ds_choice_opt: int,
                              ds_no_choice: int,
                              working_directory: str,
                              logger: Optional[Logger] = LOGGER) -> str:
    """
    from the fusion of classification's raster choice compute the fusion
     of confidence map
    Return the output path
    Parameters
    ----------

    fusion_dic :
        dictionnary containing keys : "sar_classif", "opt_classif", "sar_model"
        "opt_model"
    ds_choice :
        path to the fusion of classifications choice map
    classif_model_pos :
        position of the model's name in classification's name if
        splited by '_'
    classif_tile_pos :
        position of the tile's name in classification's name if
        splited by '_'
    classif_seed_pos :
        position of the seed number in classification's name if
        splited by '_'
    ds_choice_both :
        output value if fusion of classifications get the same label than
        SAR classification and optical classification
    ds_choice_sar :
        output value if fusion of classifications get the same label than
        SAR classification
    ds_choice_opt :
        output value if fusion of classifications get the same label than
        optical classification
    ds_no_choice :
        default case
    working_directory :
        path to a working directory
    logger :
        root logger

    Notes
    -----
    confidence fusion rules are :
        If SAR's label is chosen by the DS method then SAR confidence is
        chosen.
        If Optical's label is chosen by the DS method then optical confidence
        is chosen.
        If the same label is chosen by SAR and optical models, then the
        maximum confidence is chosen.

    """
    from iota2.Common import OtbAppBank

    classif_dir, _ = os.path.split(fusion_dic["sar_classif"])
    model = os.path.basename(
        fusion_dic["sar_classif"]).split("_")[classif_model_pos]
    seed = os.path.basename(
        fusion_dic["sar_classif"]).split("_")[classif_seed_pos]
    tile = os.path.basename(
        fusion_dic["sar_classif"]).split("_")[classif_tile_pos]
    sar_confidence = fu.fileSearchRegEx(
        os.path.join(
            classif_dir,
            "{}_model_{}_confidence_seed_{}_SAR.tif".format(tile, model,
                                                            seed)))[0]
    opt_confidence = fu.fileSearchRegEx(
        os.path.join(
            classif_dir,
            "{}_model_{}_confidence_seed_{}.tif".format(tile, model, seed)))[0]

    im_list = [ds_choice, sar_confidence, opt_confidence]
    confidence_exp = (f"im1b1=={ds_choice_both}?max(im2b1, im3b1):im1b1=="
                      f"{ds_choice_sar}?im2b1:im1b1=={ds_choice_opt}"
                      f"?im3b1:{ds_no_choice}")
    ds_confidence_name = f"{tile}_model_{model}_confidence_seed_{seed}_DS.tif"
    ds_confidence_dir = classif_dir
    ds_confidence = os.path.join(ds_confidence_dir, ds_confidence_name)
    if working_directory:
        ds_confidence = os.path.join(working_directory, ds_confidence_name)
    confidence_param = {
        "il": im_list,
        "out": ds_confidence,
        "exp": confidence_exp
    }
    confidence = OtbAppBank.CreateBandMathApplication(confidence_param)

    if not os.path.exists(os.path.join(ds_confidence_dir, ds_confidence_name)):
        logger.info(f"computing : {ds_confidence}")
        confidence.ExecuteAndWriteOutput()
        logger.debug(f"{ds_choice} : DONE")
        if working_directory:
            # copy confidence
            shutil.copy(ds_confidence,
                        os.path.join(ds_confidence_dir, ds_confidence_name))
            # remove
            os.remove(ds_confidence)
    return os.path.join(ds_confidence_dir, ds_confidence_name)


def compute_probamap_fusion(fusion_dic: Dict[str, str],
                            ds_choice: str,
                            classif_model_pos: int,
                            classif_tile_pos: int,
                            classif_seed_pos: int,
                            ds_choice_both: int,
                            ds_choice_sar: int,
                            ds_choice_opt: int,
                            ds_no_choice: int,
                            working_directory: str,
                            ram: Optional[int] = 128,
                            logger: Optional[Logger] = LOGGER) -> str:
    """
    from the fusion of classification's raster choice compute the
    fusion of confidence map
    Return the output path
    Parameters
    ----------

    fusion_dic :
        dictionnary containing keys : "sar_classif", "opt_classif", "sar_model"
        "opt_model"
    ds_choice :
        path to the fusion of classifications choice map
    classif_model_pos :
        position of the model's name in classification's name if
        splited by '_'
    classif_tile_pos :
        position of the tile's name in classification's name if
        splited by '_'
    classif_seed_pos :
        position of the seed number in classification's name if
        splited by '_'
    ds_choice_both :
        output value if fusion of classifications get the same label than
        SAR classification and optical classification
    ds_choice_sar :
        output value if fusion of classifications get the same label than
        SAR classification
    ds_choice_opt :
        output value if fusion of classifications get the same label than
        optical classification
    ds_no_choice :
        default case
    working_directory :
        path to a working directory
    LOGGER :
        root logger

    Notes
    -----
    fusion rules are :
        - If SAR's label is chosen by the DS method then SAR confidence
          is chosen.
        - If Optical's label is chosen by the DS method then optical
          confidence is chosen.
        - If the same label is chosen by SAR and optical models, then the
          output pixel vector is given my the model which is more
          confidence in it's choice.
    """
    from iota2.Common import OtbAppBank
    from iota2.Common.FileUtils import getRasterNbands

    classif_dir, _ = os.path.split(fusion_dic["sar_classif"])
    model = os.path.basename(
        fusion_dic["sar_classif"]).split("_")[classif_model_pos]
    seed = os.path.basename(
        fusion_dic["sar_classif"]).split("_")[classif_seed_pos]
    tile = os.path.basename(
        fusion_dic["sar_classif"]).split("_")[classif_tile_pos]
    sar_proba_map = fu.fileSearchRegEx(
        os.path.join(
            classif_dir,
            "PROBAMAP_{}_model_{}_seed_{}_SAR.tif".format(tile, model,
                                                          seed)))[0]
    opt_proba_map = fu.fileSearchRegEx(
        os.path.join(
            classif_dir,
            "PROBAMAP_{}_model_{}_seed_{}.tif".format(tile, model, seed)))[0]
    sar_confidence = fu.fileSearchRegEx(
        os.path.join(
            classif_dir,
            "{}_model_{}_confidence_seed_{}_SAR.tif".format(tile, model,
                                                            seed)))[0]
    opt_confidence = fu.fileSearchRegEx(
        os.path.join(
            classif_dir,
            "{}_model_{}_confidence_seed_{}.tif".format(tile, model, seed)))[0]
    im_list = [
        ds_choice, sar_proba_map, opt_proba_map, sar_confidence, opt_confidence
    ]
    nb_bands_probamap_opt = getRasterNbands(opt_proba_map)
    nb_bands_probamap_sar = getRasterNbands(sar_proba_map)
    if nb_bands_probamap_opt != nb_bands_probamap_sar:
        raise Exception("SAR probality map and Optical probality map "
                        "must have the same number of bands")
    ds_no_choice = ("{" +
                    ",".join([str(ds_no_choice)] * nb_bands_probamap_opt) +
                    "}")
    exp = (f"im1b1=={ds_choice_both} and im4b1>im5b1?im2:"
           f"im1b1=={ds_choice_both} and im4b1<im5b1?im3:"
           f"im1b1=={ds_choice_sar}?im2:"
           f"im1b1=={ds_choice_opt}?im3:"
           f"{ds_no_choice}")
    ds_probamap_name = f"PROBAMAP_{tile}_model_{model}_seed_{seed}_DS.tif"
    ds_probamap_dir = classif_dir
    ds_probamap = os.path.join(ds_probamap_dir, ds_probamap_name)
    if working_directory:
        ds_probamap = os.path.join(working_directory, ds_probamap_name)
    probamap_param = {
        "il": im_list,
        "out": ds_probamap,
        "ram": str(ram),
        "exp": exp
    }
    probamap_app = OtbAppBank.CreateBandMathXApplication(probamap_param)

    if not os.path.exists(os.path.join(ds_probamap_dir, ds_probamap_name)):
        logger.info(f"computing : {ds_probamap}")
        probamap_app.ExecuteAndWriteOutput()
        logger.debug(f"{ds_probamap} : DONE")
        if working_directory:
            # copy confidence
            shutil.copy(
                ds_probamap,
                os.path.join(os.path.join(ds_probamap_dir, ds_probamap_name)))
            # remove
            os.remove(ds_probamap)
    return os.path.join(os.path.join(ds_probamap_dir, ds_probamap_name))


def dempster_shafer_fusion(iota2_dir: str,
                           fusion_dic: Dict[str, str],
                           mob: Optional[str] = "precision",
                           proba_map_flag: Optional[bool] = False,
                           working_directory: Optional[str] = None,
                           logger=LOGGER) -> List[str]:
    """
    perform a fusion of classifications thanks to
    Dempster-Shafer's method
    Return a list containing : the Dempster-Shafer fusion labels path,
                               Dempster-Shafer fusion confidence path,
                               Dempster-Shafer fusion probability path,
                               Dempster-Shafer fusion choice path
    Parameters
    ----------
    iota2_dir : string
        iota2's output path
    fusion_dic : dict
        dictionnary containing keys : "sar_classif", "opt_classif", "sar_model"
        "opt_model"
    mob : string
        Dempster-Shafer's mass of belive
    proba_map_flag : bool
        perform fusion to probability maps ?
    workingDirectory : string
        path to a working directory
    """
    # const
    classif_seed_pos = 5
    classif_tile_pos = 1
    classif_model_pos = 3

    ds_choice_both = 1
    ds_choice_sar = 2
    ds_choice_opt = 3
    ds_no_choice = 0

    # fusion
    sar_opt_fus = perform_fusion(fusion_dic,
                                 mob,
                                 classif_model_pos,
                                 classif_tile_pos,
                                 classif_seed_pos,
                                 working_directory,
                                 logger=logger)

    # dempster-shafer raster choice
    ds_choice = compute_fusion_choice(iota2_dir,
                                      fusion_dic,
                                      sar_opt_fus,
                                      classif_model_pos,
                                      classif_tile_pos,
                                      classif_seed_pos,
                                      ds_choice_both,
                                      ds_choice_sar,
                                      ds_choice_opt,
                                      ds_no_choice,
                                      working_directory,
                                      logger=logger)

    # confidence
    confidence_fus = compute_confidence_fusion(
        fusion_dic, ds_choice, classif_model_pos, classif_tile_pos,
        classif_seed_pos, ds_choice_both, ds_choice_sar, ds_choice_opt,
        ds_no_choice, working_directory)
    # probability map
    proba_map_fus = None
    if proba_map_flag:
        proba_map_fus = compute_probamap_fusion(fusion_dic,
                                                ds_choice,
                                                classif_model_pos,
                                                classif_tile_pos,
                                                classif_seed_pos,
                                                ds_choice_both,
                                                ds_choice_sar,
                                                ds_choice_opt,
                                                ds_no_choice,
                                                working_directory,
                                                ram=2000,
                                                logger=logger)
    return sar_opt_fus, confidence_fus, proba_map_fus, ds_choice


def fusion(in_classif: List[str],
           fusion_options: str,
           out_classif: str,
           out_pix_type: str,
           logger=LOGGER) -> None:
    """
    Launch the otb application FusionOfClassifications

    Parameters
    ----------
    in_classif :
        the list of classification to fuse
    fusion_options:
        the fusion method and the associated parameters
    out_classif:
        the output classification
    out_pix_type:
        the output pixel type
    """
    in_classif = " ".join(in_classif)
    cmd = (f"otbcli_FusionOfClassifications -il {in_classif} {fusion_options}"
           f" -out {out_classif} {out_pix_type}")
    run(cmd, logger=logger)
