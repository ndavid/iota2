#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================

import argparse
import os
import logging
from typing import List, Dict, Union, Optional
from iota2.Common import FileUtils as fu

sensors_params_type = Dict[str, Union[str, List[str], int]]

LOGGER = logging.getLogger("distributed.worker")


def str2bool(val: str) -> bool:
    """
    Convert a string value to boolean
    Parameters
    ----------
    val :
        the value to convert
    """
    if val.lower() not in ('yes', 'true', 't', 'y', '1', 'no', 'false', 'f',
                           'n', '0'):
        raise argparse.ArgumentTypeError('Boolean value expected.')

    retour = True
    if val.lower() in ("no", "false", "f", "n", "0"):
        retour = False
    return retour


def autocontext_launch_classif(parameters_dict: List[Dict[str,
                                                          Union[str,
                                                                List[str]]]],
                               classifier_type: str,
                               tile: str,
                               proba_map_expected: bool,
                               dimred: bool,
                               data_field: str,
                               write_features: bool,
                               reduction_mode: str,
                               iota2_run_dir: str,
                               sar_optical_post_fusion: bool,
                               nomenclature_path: str,
                               sensors_parameters: sensors_params_type,
                               ram: int,
                               working_directory: str,
                               logger=LOGGER) -> None:
    """
    Launch the classification in autocontext mode

    Parameters
    ----------
    parameter_dict:
        A dictionnary containing the parameters for autocontext
    classifier_type:
        the name of the classifier
    tile:
        the tile to be processed
    proba_map_expected:
        enable the creation of probability maps
    dimred:
        enable the dimensionality reduction
    data_field:
        the id class name field
    write_features:
        enable the save of temporary data
    reduction_mode:
        the mode for dimensionality reduction
    iota2_run_dir:
        the output path
    sar_optical_post_fusion:
        enable the fusion between SAR and optical
    nomenclature_path:
        the nomenclature file
    sensors_parameters:
        the dictionary containing all information about sensors
    ram:
        the available ram
    working_directory:
        temporary folder to save data

    """
    from iota2.Common.FileUtils import FileSearch_AND
    from iota2.VectorTools.vector_functions import getFieldElement
    pixtype = "uint8"

    models = parameters_dict["model_list"]
    classif_mask = parameters_dict["tile_mask"]

    class_labels_in_model = []
    sample_sel_directory = os.path.join(iota2_run_dir, "samplesSelection")
    model_sample_sel = FileSearch_AND(
        sample_sel_directory, True,
        "samples_region_{}_seed_{}.shp".format(parameters_dict["model_name"],
                                               parameters_dict["seed_num"]))[0]
    class_labels_in_model = getFieldElement(model_sample_sel,
                                            driverName="ESRI Shapefile",
                                            field=data_field,
                                            mode="unique")

    stats = os.path.join(
        iota2_run_dir, "stats",
        "Model_{}_seed_{}.xml".format(parameters_dict["model_name"],
                                      parameters_dict["seed_num"]))

    output_classif = "Classif_{}_model_{}_seed_{}.tif".format(
        parameters_dict["tile"], parameters_dict["model_name"],
        parameters_dict["seed_num"])

    launch_classification(classif_mask,
                          models,
                          stats,
                          output_classif,
                          working_directory,
                          classifier_type,
                          tile,
                          proba_map_expected,
                          dimred,
                          sar_optical_post_fusion,
                          iota2_run_dir,
                          data_field,
                          write_features,
                          reduction_mode,
                          sensors_parameters,
                          pixtype,
                          ram=ram,
                          auto_context={
                              "labels_list":
                              class_labels_in_model,
                              "tile_segmentation":
                              parameters_dict["tile_segmentation"]
                          },
                          logger=LOGGER)


class iota2_classification():
    def __init__(
            self,
            features_stack,
            classifier_type,
            model,
            tile,
            output_directory,
            models_class,
            proba_map: Optional[bool] = False,
            classif_mask: Optional[str] = None,
            pixtype: Optional[int] = "uint8",
            working_directory: Optional[str] = None,
            stat_norm: Optional[str] = None,
            ram: Optional[int] = 128,
            auto_context: Optional[Dict[str, Union[str, int]]] = None,
            # logger: Optional[Logger]=LOGGER,
            external_features_flag: Optional[bool] = False,
            targeted_chunk: Optional[int] = None,
            mode: Optional[str] = "usually",
            logger=LOGGER) -> None:
        """
        Create a instance for classification

        PARAMETERS
        ----------
        features_stack:
            the time series to be classified
        classifier_type:
            the classifier name
        model:
            the model name
        tile:
            the tile name
        output_directory:
            the output path
        models_class:
            TODO
        proba_map:
            if enabled write the probability maps
        classif_mask:
            if provided the produced classification is masked
        pixtype:
            define the pixel type for classification
        working_directory:
            temporarly folder for writing data
        stat_norm:
            a statistics file name. if provided data are normalized
        ram:
            the ram available for OTB applications
        auto_context:
            a dictionary containing all the information for use autocontext
        # logger:
        #    File for logging
        external_features_flag:
            enable the use of external features
        targeted_chunk:
            indicate the part of image to be processed
        mode:
            the classificaton mode
        """
        self.models_class = models_class
        self.classif_mask = classif_mask
        self.output_directory = output_directory
        self.ram = ram
        self.pixtype = pixtype
        self.stats = stat_norm
        self.classifier_model = model
        self.auto_context = auto_context if auto_context else {}
        self.tile = tile
        self.external_features = external_features_flag
        self.logger = logger
        # use this to remove one parameter
        # self.external_features = targeted_chunk is not None

        if isinstance(model, list):
            self.model_name = self.get_model_name(os.path.split(model[0])[0])
            self.seed = self.get_model_seed(os.path.split(model[0])[0])
        else:
            self.model_name = self.get_model_name(model)
            self.seed = self.get_model_seed(model)
        self.features_stack = features_stack
        sub_name = ("" if targeted_chunk is None else
                    f"_SUBREGION_{targeted_chunk}")
        classification_name = (f"Classif_{tile}_model_{self.model_name}"
                               f"_seed_{self.seed}{sub_name}.tif")
        confidence_name = (f"{tile}_model_{self.model_name}_"
                           f"confidence_seed_{self.seed}{sub_name}.tif")
        proba_map_name = (f"PROBAMAP_{tile}_model_"
                          f"{self.model_name}_seed_{self.seed}{sub_name}.tif")
        if mode == "SAR":
            classification_name = classification_name.replace(
                ".tif", "_SAR.tif")
            confidence_name = confidence_name.replace(".tif", "_SAR.tif")
            proba_map_name = proba_map_name.replace(".tif", "_SAR.tif")
        self.classification = os.path.join(output_directory,
                                           classification_name)
        self.confidence = os.path.join(output_directory, confidence_name)
        self.proba_map_path = self.get_proba_map(classifier_type,
                                                 output_directory, proba_map,
                                                 proba_map_name)
        self.working_directory = working_directory

    def get_proba_map(self, classifier_type: str, output_directory: str,
                      gen_proba: bool, proba_map_name: str) -> str:
        """
        Get probability map absolute path

        Parameters
        ----------
        classifier_type :
            classifier's name (provided by OTB)
        output_directory :
            output directory
        gen_proba :
            indicate if proba map is asked
        proba_map_name :
            probability raster name

        """
        proba_map = ""
        classifier_avail = ["sharkrf"]
        if classifier_type in classifier_avail:
            proba_map = os.path.join(output_directory, proba_map_name)
        if gen_proba and proba_map == "":
            warn_mes = (
                f"classifier '{classifier_type}' not available to generate"
                f" a probability map, those available are {classifier_avail}")
            self.logger.warning(warn_mes)
        return proba_map if gen_proba else ""

    def get_model_name(self, model):
        """
        """
        return os.path.splitext(os.path.basename(model))[0].split("_")[1]

    def get_model_seed(self, model):
        """
        """
        return os.path.splitext(os.path.basename(model))[0].split("_")[3]

    def set_autocontext_options(self):
        from iota2.Common.FileUtils import ensure_dir

        tmp_dir = os.path.join(
            self.output_directory,
            "tmp_model_{}_seed_{}_tile_{}".format(self.model_name, self.seed,
                                                  self.tile))
        if self.working_directory:
            tmp_dir = os.path.join(
                self.working_directory,
                "tmp_model_{}_seed_{}_tile_{}".format(self.model_name,
                                                      self.seed, self.tile),
            )

        ensure_dir(tmp_dir)
        classifier_options = {
            "in": self.features_stack,
            "inseg": self.auto_context["tile_segmentation"],
            "models": self.classifier_model,
            "lablist": [str(lab) for lab in self.auto_context["labels_list"]],
            "confmap": "{}?&writegeom=false".format(self.confidence),
            "ram": str(0.4 * float(self.ram)),
            "pixType": self.pixtype,
            "tmpdir": tmp_dir,
            "out": "{}?&writegeom=false".format(self.classification)
        }

        return classifier_options, tmp_dir

    def apply_mask(self, nb_class_run: int) -> None:
        """
        If a mask is provided, apply it to all products

        Parameters
        ----------
        nb_class_run:
            the number of classes readed in the model file
        """
        from iota2.Common.OtbAppBank import CreateBandMathApplication
        from iota2.Common.OtbAppBank import CreateBandMathXApplication
        if self.classif_mask:
            mask_filter = CreateBandMathApplication({
                "il": [self.classification, self.classif_mask],
                "ram":
                str(self.ram),
                "pixType":
                self.pixtype,
                "out":
                self.classification,
                "exp":
                "im2b1>=1?im1b1:0"
            })
            mask_filter.ExecuteAndWriteOutput()
            mask_filter = CreateBandMathApplication({
                "il": [self.confidence, self.classif_mask],
                "ram":
                str(self.ram),
                "pixType":
                "float",
                "out":
                self.confidence,
                "exp":
                "im2b1>=1?im1b1:0"
            })
            mask_filter.ExecuteAndWriteOutput()
            if self.proba_map_path:
                expr = "im2b1>=1?im1:{}".format("{" + ";".join(["0"] *
                                                               nb_class_run) +
                                                "}")
                mask_filter = CreateBandMathXApplication({
                    "il": [self.proba_map_path, self.classif_mask],
                    "ram":
                    str(self.ram),
                    "pixType":
                    "uint16",
                    "out":
                    self.proba_map_path,
                    "exp":
                    expr
                })
                mask_filter.ExecuteAndWriteOutput()

    def clean_working_directory(self, tmp_dir: str):
        """
        Move classification from working directory to output_path
        Then remove all temporarly data
        Parameters
        ----------
        tmp_dir:
            temporary folder
        """
        import shutil

        if self.working_directory:
            shutil.copy(
                self.classification,
                os.path.join(self.output_directory,
                             os.path.split(self.classification)[-1]))
            # os.remove(self.classification)
            shutil.copy(
                self.confidence,
                os.path.join(self.output_directory,
                             os.path.split(self.confidence)[-1]))
            # os.remove(self.confidence)
            if self.proba_map_path:
                shutil.copy(
                    self.proba_map_path,
                    os.path.join(self.output_directory,
                                 os.path.split(self.proba_map_path)[-1]))
                os.remove(self.proba_map_path)
            if self.auto_context:
                shutil.rmtree(tmp_dir)

    def generate(self):
        """
        Create and execute all applications for produce classification
        """

        from iota2.Common.OtbAppBank import CreateImageClassifierApplication
        from iota2.Common.OtbAppBank import CreateClassifyAutoContext

        if self.working_directory:
            self.classification = os.path.join(
                self.working_directory,
                os.path.split(self.classification)[-1])
            self.confidence = os.path.join(self.working_directory,
                                           os.path.split(self.confidence)[-1])

        classifier_options = {
            "in": self.features_stack,
            "model": self.classifier_model,
            "confmap": "{}?&writegeom=false".format(self.confidence),
            "ram": str(0.2 * float(self.ram)),
            "pixType": self.pixtype,
            "out": "{}?&writegeom=false".format(self.classification)
        }

        tmp_dir = None
        if self.auto_context:
            classifier_options, tmp_dir = self.set_autocontext_options()
        # Used only if probamap is asked then 0 is a good default value
        nb_class_run = 0
        if self.proba_map_path:
            all_class = []
            for _, dico_seed in list(self.models_class.items()):
                for _, avail_class in list(dico_seed.items()):
                    all_class += avail_class
            all_class = sorted(list(set(all_class)))
            nb_class_run = len(all_class)
            if self.working_directory:
                self.proba_map_path = os.path.join(
                    self.working_directory,
                    os.path.split(self.proba_map_path)[-1])
            classifier_options["probamap"] = "{}?&writegeom=false".format(
                self.proba_map_path)
            classifier_options["nbclasses"] = str(nb_class_run)

        if self.stats:
            classifier_options["imstat"] = self.stats
        if self.auto_context:
            classifier = CreateClassifyAutoContext(classifier_options)
        else:
            classifier = CreateImageClassifierApplication(classifier_options)
            if self.external_features:
                classifier.ImportVectorImage("in", self.features_stack)

        self.logger.info(f"Compute Classification : {self.classification}")
        self.logger.info("ram before classification : "
                         f"{fu.memory_usage_psutil()} MB")
        classifier.ExecuteAndWriteOutput()
        self.logger.info("ram after classification : "
                         f"{fu.memory_usage_psutil()} MB")
        self.logger.info(f"Classification : {self.classification} done")

        self.apply_mask(nb_class_run)
        if self.proba_map_path:
            class_model = self.models_class[self.model_name][int(self.seed)]
            if len(class_model) != len(all_class):
                self.logger.info(
                    f"reordering the probability map : {self.proba_map_path}")
                self.reorder_proba_map(self.proba_map_path,
                                       self.proba_map_path, class_model,
                                       all_class)
        self.clean_working_directory(tmp_dir)

    def reorder_proba_map(self, proba_map_path_in: str,
                          proba_map_path_out: str, class_model: List[int],
                          all_class: List[int]):
        """reorder the probability map

        in order to merge proability raster containing a different number of
        effective class it is needed to reorder them according to a reference

        Parameters
        ----------
        proba_map_path_in :
            input probability map
        proba_map_path_out :
            output probability map
        class_model :
            list containing labels in the model used to classify
        all_class :
            list containing all possible labels
        """
        from iota2.Common.OtbAppBank import CreateBandMathXApplication

        class_model_copy = class_model[:]

        # NODATA index in probability map
        nodata_label_idx = len(all_class)
        nodata_label_buff = "NODATALABEL"
        index_vector = []

        for index_label, expected_label in enumerate(all_class):
            if expected_label not in class_model:
                class_model_copy.insert(index_label, nodata_label_buff)
        for class_model_label in class_model_copy:
            if class_model_label in class_model:
                idx = class_model.index(class_model_label) + 1
            else:
                idx = nodata_label_idx
            index_vector.append(idx)
        exp = "bands(im1, {})".format("{" + ",".join(map(str, index_vector)) +
                                      "}")
        reorder_app = CreateBandMathXApplication({
            "il": proba_map_path_in,
            "ram": str(self.ram),
            "exp": exp,
            "out": proba_map_path_out
        })
        reorder_app.ExecuteAndWriteOutput()


def get_model_dictionnary(model: str) -> List[int]:
    """
    From a model file, get the class dictionary
    Parameters
    ----------
    model:
        the model file
    """
    classes = []
    with open(model) as modelfile:
        line = next(modelfile)
        if "#" in line and "with_dictionary" in line:
            classes = next(modelfile).split(" ")[1:-1]
            classes = [int(x) for x in classes]
    return classes


def get_class_by_models(iota2_samples_dir: str,
                        data_field: str,
                        model: Optional[str] = None) -> Dict[str, int]:
    """ inform which class will be used to by models

    Parameters
    ----------
    iota2_samples_dir :
        path to the directory containing samples dedicated to learn models

    data_field :
        field which contains labels in vector file
    model :
        the model file
    Return
    ------
    dic[model][seed]

    Example
    -------
    >>> dico_models = get_class_by_models("/somewhere/learningSamples", "code")
    >>> print dico_models["1"][0]
    >>> [11, 12, 31]
    """
    from iota2.Common.FileUtils import FileSearch_AND
    from iota2.VectorTools.vector_functions import getFieldElement

    class_models = {}
    if model is not None:
        modelpath = os.path.dirname(model)
        models_files = FileSearch_AND(modelpath, True, "model", "seed", ".txt")

        for model_file in models_files:
            model_name = os.path.splitext(
                os.path.basename(model_file))[0].split("_")[1]
            class_models[model_name] = {}

        for model_file in models_files:
            model_name = os.path.splitext(
                os.path.basename(model_file))[0].split("_")[1]
            seed_number = int(
                os.path.splitext(
                    os.path.basename(model_file))[0].split("_")[3].replace(
                        ".txt", ""))
            classes = get_model_dictionnary(model_file)
            class_models[model_name][seed_number] = classes
    else:
        samples_files = FileSearch_AND(iota2_samples_dir, True,
                                       "Samples_region_", "_seed",
                                       "_learn.sqlite")

        for samples_file in samples_files:
            model_name = os.path.splitext(
                os.path.basename(samples_file))[0].split("_")[2]
            class_models[model_name] = {}
        for samples_file in samples_files:
            model_name = os.path.splitext(
                os.path.basename(samples_file))[0].split("_")[2]
            seed_number = int(
                os.path.splitext(
                    os.path.basename(samples_file))[0].split("_")[3].replace(
                        "seed", ""))
            class_models[model_name][seed_number] = sorted(
                getFieldElement(samples_file,
                                driverName="SQLite",
                                field=data_field.lower(),
                                mode="unique",
                                elemType="int"))
    return class_models


def launch_classification(classifmask: str,
                          model: str,
                          stats: str,
                          output_classif: str,
                          path_wd: str,
                          classifier_type: str,
                          tile: str,
                          proba_map_expected: bool,
                          dimred: bool,
                          sar_optical_post_fusion: bool,
                          output_path: str,
                          data_field: str,
                          write_features: bool,
                          reduction_mode: str,
                          sensors_parameters: sensors_params_type,
                          pixtype: str,
                          ram: Optional[int] = 500,
                          auto_context: Optional[Dict[str, str]] = None,
                          force_standard_labels: Optional[bool] = None,
                          external_features: Optional[bool] = False,
                          module_path: Optional[str] = None,
                          list_functions: Optional[str] = None,
                          number_of_chunks: Optional[int] = None,
                          chunk_size_mode: Optional[str] = None,
                          chunk_size_x: Optional[int] = None,
                          chunk_size_y: Optional[int] = None,
                          targeted_chunk: Optional[int] = None,
                          concat_mode: Optional[bool] = True,
                          logger=LOGGER) -> None:
    """
    Launch the classification

    Parameters
    ----------
    classifmask:
        the classification mask
    model:
        the model name
    stats:
        the statistics file name
    output_classif:
        the output classification name
    path_wd:
        working directory path for temporarly data
    classifier_type:
        the classifier name
    tile:
        the tile name to be classified
    proba_map_expected:
        enable the writting of probability map (sharkrf only)
    dimred:
        enable the dimensionality reduction
    sar_optical_post_fusion:
        enable fusion between SAR and optical data
    output_path:
        the path where final classification must be stored
    data_field:
        the column name for class labels
    write_features:
        write the features stack
    reduction_mode:
        parameters for reduction mode
    sensors_parameters:
        dictionary with all sensors information
    pixtype:
        the output pixel type
    ram:
        the ram available for OTB applications
    auto_context:
        a dictionnary of all parameters for autocontext
    force_standard_labels:
        use generic label (value_x) instead of named label (sensor_band_date)
    external_features:
        enable external features mode
    module_path:
        the path to external features code
    list_functions:
        the list of function used to compute external features
    number_of_chunks:
        the number of chunks required
    chunk_size_mode:
        the image split mode
    chunk_size_x:
        the number of pixel in column for extracted chunk
    chunk_size_y:
        the number of pixel in row for extracted chunk
    targeted_chunk:
        indicate which chunk must be processed
    concat_mode:
        activate the concatenation mode
    logger:
        the logging instance

    Notes
    -----
    
    - Several parameter are activated only if external_features is set to true:
        - module_path
        - list_functions
        - number_of_chunks
        - chunk_size_mode
        - chunk_size_x
        - chunk_size_y
        - targeted_chunk
        - concat_mode

    - chunk_size_mode has two value and requiere different parameters for each

    +-------------------+-----------------------+
    | Parameter         | chunk_size_mode value |
    +-------------------+-----------------------+
    | chunk_size_x      | user_fixed            |
    +-------------------+-----------------------+
    | chunk_size_y      | user_fixed            |
    +-------------------+-----------------------+
    | number_of_chunks  | split_number          |
    +-------------------+-----------------------+
    | targeted_chunk    | both                  |
    +-------------------+-----------------------+

    - concat_mode:
        If enabled the extenal features are concatenated with all iota2
        features (reflectance, NDVI, NDWI,...)
        If disabled only the external features are used
    """
    from iota2.Common import GenerateFeatures as genFeatures
    from iota2.Sampling import DimensionalityReduction as DR
    from iota2.Common.OtbAppBank import getInputParameterOutput
    from iota2.Common.OtbAppBank import CreateExtractROIApplication
    from iota2.Common.customNumpyFeatures import compute_custom_features
    auto_context = auto_context if auto_context else {}
    output_directory = os.path.join(output_path, "classif")

    features_path = os.path.join(output_path, "features")

    working_dir = path_wd
    if not path_wd:
        working_dir = features_path

    working_dir = os.path.join(features_path, tile)

    if path_wd:
        working_dir = os.path.join(path_wd, tile)
        if not os.path.exists(working_dir):
            try:
                os.mkdir(working_dir)
            except OSError:
                logger.warning(f"{working_dir} Allready exists")

    mode = "usually"
    if "SAR.tif" in output_classif:
        mode = "SAR"

    all_features, _, dep_features = genFeatures.generate_features(
        pathWd=working_dir,
        tile=tile,
        sar_optical_post_fusion=sar_optical_post_fusion,
        output_path=output_path,
        sensors_parameters=sensors_parameters,
        mode=mode,
        force_standard_labels=force_standard_labels,
        logger=logger)
    feature_raster = all_features.GetParameterValue(
        getInputParameterOutput(all_features))
    if write_features:
        if not os.path.exists(feature_raster):
            all_features.ExecuteAndWriteOutput()
        all_features = feature_raster
    else:
        all_features.Execute()
    classif_input = all_features

    if dimred:
        logger.debug(f"Classification model : {model}")
        dim_red_model_list = DR.get_dim_red_models_from_classification_model(
            model, logger=logger)
        logger.debug("Dim red models : {}".format(dim_red_model_list))
        [classif_input,
         other_dep] = DR.apply_dimensionality_reduction_to_feature_stack(
             reduction_mode,
             output_path,
             all_features,
             dim_red_model_list,
             logger=logger)
        if write_features:
            classif_input.ExecuteAndWriteOutput()
        else:
            classif_input.Execute()
    remove_flag = False
    if external_features:
        otbimage, _ = compute_custom_features(
            tile=tile,
            output_path=output_path,
            sensors_parameters=sensors_parameters,
            module_path=module_path,
            list_functions=list_functions,
            otb_pipeline=all_features,
            feat_labels=[],
            path_wd=working_dir,
            chunk_size_mode=chunk_size_mode,
            targeted_chunk=targeted_chunk,
            number_of_chunks=number_of_chunks,
            chunk_size_x=chunk_size_x,
            chunk_size_y=chunk_size_y,
            concat_mode=concat_mode,
            logger=logger)
        classif_input = otbimage
        if classifmask:
            chunked_mask = os.path.join(
                working_dir, f"Chunk_{targeted_chunk}_classif_mask.tif")
            roi_param = {
                "in": classifmask,
                "out": chunked_mask,
                "mode": "fit",
                "mode.fit.im": otbimage,
                "ram": ram
            }
            roi_mask = CreateExtractROIApplication(roi_param)
            roi_mask.ExecuteAndWriteOutput()
            roi_mask = None
            classifmask = chunked_mask
            remove_flag = True
    iota2_samples_dir = os.path.join(output_path, "learningSamples")
    models_class = get_class_by_models(
        iota2_samples_dir,
        data_field,
        model=model if proba_map_expected else None)
    classif = iota2_classification(classif_input,
                                   classifier_type,
                                   model,
                                   tile,
                                   output_directory,
                                   models_class,
                                   proba_map=proba_map_expected,
                                   working_directory=path_wd,
                                   classif_mask=classifmask,
                                   pixtype=pixtype,
                                   stat_norm=stats,
                                   ram=ram,
                                   mode=mode,
                                   external_features_flag=external_features,
                                   targeted_chunk=targeted_chunk,
                                   auto_context=auto_context,
                                   logger=logger)
    classif.generate()
    if remove_flag:
        os.remove(chunked_mask)


if __name__ == "__main__":

    from iota2.configuration_files import read_config_file as rcf
    PARSER = argparse.ArgumentParser(
        description=("Performs a classification of the input image "
                     "(compute in ram) according to a model file, "))
    PARSER.add_argument(
        "-in",
        dest="tempFolderSerie",
        help="path to the folder which contains temporal series",
        default=None,
        required=True)
    PARSER.add_argument("-mask",
                        dest="Classifmask",
                        help="path to classification's mask",
                        default=None,
                        required=True)
    PARSER.add_argument("-classifier_type",
                        dest="classifier_type",
                        help="classifier name",
                        required=True)
    PARSER.add_argument("-tile",
                        dest="tile",
                        help="tile's name",
                        required=True)
    PARSER.add_argument("-proba_map_expected",
                        dest="proba_map_expected",
                        help="is probality maps were generated",
                        type=str2bool,
                        default=False,
                        required=False)
    PARSER.add_argument("-dimred",
                        dest="dimred",
                        help="flag to use dimensionality reduction",
                        type=str2bool,
                        default=False,
                        required=False)
    PARSER.add_argument("-sar_optical_post_fusion",
                        dest="sar_optical_post_fusion",
                        help=("flag to enable sar and optical "
                              "post-classification fusion"),
                        type=str2bool,
                        default=False,
                        required=False)
    PARSER.add_argument("-reduction_mode",
                        dest="reduction_mode",
                        help="reduction mode",
                        default=None,
                        required=True)
    PARSER.add_argument(
        "-data_field",
        dest="data_field",
        help="field containing labels in the groundtruth database",
        default=None,
        required=True)
    PARSER.add_argument("-pixType",
                        dest="pixType",
                        help="pixel format",
                        default=None,
                        required=True)
    PARSER.add_argument("-model",
                        dest="model",
                        help="path to the model",
                        default=None,
                        required=True)
    PARSER.add_argument("-imstat",
                        dest="stats",
                        help="path to statistics",
                        default=None,
                        required=False)
    PARSER.add_argument("-out",
                        dest="output_classif",
                        help="output classification's path",
                        default=None,
                        required=True)
    PARSER.add_argument("-confmap",
                        dest="confmap",
                        help="output classification confidence map",
                        default=None,
                        required=True)
    PARSER.add_argument("-output_path",
                        dest="output_path",
                        help="iota2 output path",
                        required=True)
    PARSER.add_argument("-ram",
                        dest="ram",
                        help="pipeline's size",
                        default=128,
                        required=False)
    PARSER.add_argument("--wd",
                        dest="pathWd",
                        help="path to the working directory",
                        default=None,
                        required=False)
    PARSER.add_argument("-conf",
                        help="path to the configuration file (mandatory)",
                        dest="pathConf",
                        required=True)
    PARSER.add_argument("-maxCPU",
                        help="True : Class all the image and after apply mask",
                        dest="MaximizeCPU",
                        default="False",
                        choices=["True", "False"],
                        required=False)
    ARGS = PARSER.parse_args()
    CFG = rcf.read_config_file(ARGS.pathConf)
    I2_PARAMS = rcf.iota2_parameters(ARGS.pathConf)
    SENSORS_PARAMETERS = I2_PARAMS.get_sensors_parameters(ARGS.tile)
    AUTO_CONTEXT_PARAMS = {}
    if CFG.getParam("chain", "enable_autocontext"):
        AUTO_CONTEXT_PARAMS = autocontext_classification_param(
            ARGS.output_path, ARGS.data_field)
    launch_classification(
        ARGS.Classifmask, ARGS.model, ARGS.stats, ARGS.output_classif,
        ARGS.pathWd, ARGS.classifier_type, ARGS.tile, ARGS.proba_map_expected,
        ARGS.dimred, ARGS.sar_optical_post_fusion, ARGS.output_path,
        ARGS.data_field, ARGS.write_features, ARGS.reduction_mode,
        SENSORS_PARAMETERS, ARGS.pixType, AUTO_CONTEXT_PARAMS)
