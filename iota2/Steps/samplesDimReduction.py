#!/usr/bin/env python3
#-*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
import os
import logging

from iota2.Steps import IOTA2Step
from iota2.Sampling import DimensionalityReduction as DR
from iota2.configuration_files import read_config_file as rcf
LOGGER = logging.getLogger("distributed.worker")


class samplesDimReduction(IOTA2Step.Step):
    resources_block_name = "dimensionalityReduction"

    def __init__(self,
                 cfg,
                 cfg_resources_file,
                 seed_granularity,
                 workingDirectory=None):
        # heritage init
        super(samplesDimReduction, self).__init__(cfg, cfg_resources_file,
                                                  self.resources_block_name)
        # step variables
        self.workingDirectory = workingDirectory
        self.output_path = rcf.read_config_file(self.cfg).getParam(
            'chain', 'output_path')
        self.nb_runs = rcf.read_config_file(self.cfg).getParam(
            'arg_train', 'runs')
        self.target_dimension = rcf.read_config_file(self.cfg).getParam(
            'dim_red', 'target_dimension')
        self.reduction_mode = rcf.read_config_file(self.cfg).getParam(
            'dim_red', 'reduction_mode')
        self.suffix_list = ["usually"]
        if rcf.read_config_file(self.cfg).getParam(
                'arg_train', 'dempster_shafer_sar_opt_fusion') is True:
            self.suffix_list.append("SAR")
        for suffix in self.suffix_list:
            for model_name, _ in self.spatial_models_distribution.items():
                for seed in range(self.nb_runs):
                    if suffix == "SAR":
                        samples_file = os.path.join(
                            self.output_path, "learningSamples",
                            f"Samples_region_{model_name}_seed{seed}_learn_SAR.sqlite"
                        )
                    else:
                        samples_file = os.path.join(
                            self.output_path, "learningSamples",
                            f"Samples_region_{model_name}_seed{seed}_learn.sqlite"
                        )
                    target_model = f"model_{model_name}_seed_{seed}_{suffix}"
                    task = self.i2_task(
                        task_name=f"data_reduction_{target_model}",
                        log_dir=self.log_step_dir,
                        execution_mode=self.execution_mode,
                        task_parameters={
                            "f":
                            DR.sample_dimensionality_reduction,
                            "io_file_pair":
                            (samples_file,
                             samples_file.replace(".sqlite",
                                                  "_reduced.sqlite")),
                            "iota2_output":
                            self.output_path,
                            "target_dimension":
                            self.target_dimension,
                            "reduction_mode":
                            self.reduction_mode
                        },
                        task_resources=self.get_resources())
                    dep_key = "region_tasks" if not seed_granularity else "seed_tasks"
                    dep_values = [target_model
                                  ] if not seed_granularity else [seed]
                    self.add_task_to_i2_processing_graph(
                        task,
                        task_group="region_tasks",
                        task_sub_group=f"{target_model}",
                        task_dep_dico={dep_key: dep_values})

    @classmethod
    def step_description(cls):
        """
        function use to print a short description of the step's purpose
        """
        description = ("Dimensionality reduction")
        return description
