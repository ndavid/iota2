#!/usr/bin/env python3
#-*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
import os
import logging

from iota2.Steps import IOTA2Step
from iota2.Sampling import VectorFormatting as VF
from iota2.configuration_files import read_config_file as rcf

LOGGER = logging.getLogger("distributed.worker")


class VectorFormatting(IOTA2Step.Step):
    resources_block_name = "samplesFormatting"

    def __init__(self, cfg, cfg_resources_file, workingDirectory=None):
        # heritage init
        super(VectorFormatting, self).__init__(cfg, cfg_resources_file,
                                               self.resources_block_name)

        # step variables
        self.working_directory = workingDirectory

        # parameters
        output_path = rcf.read_config_file(self.cfg).getParam(
            'chain', 'output_path')
        ground_truth_vec = os.path.join(output_path,
                                        self.i2_const.re_encoding_label_file)
        data_field = (self.i2_const.re_encoding_label_name)

        classif_mode = rcf.read_config_file(self.cfg).getParam(
            'arg_classification', 'classif_mode')
        cloud_threshold = rcf.read_config_file(self.cfg).getParam(
            'chain', 'cloud_threshold')
        ratio = rcf.read_config_file(self.cfg).getParam('arg_train', 'ratio')
        random_seed = rcf.read_config_file(self.cfg).getParam(
            'arg_train', 'random_seed')
        enable_split_ground_truth = rcf.read_config_file(self.cfg).getParam(
            'arg_train', 'split_ground_truth')
        fusion_merge_all_validation = rcf.read_config_file(self.cfg).getParam(
            'arg_classification',
            'fusionofclassification_all_samples_validation')
        runs = rcf.read_config_file(self.cfg).getParam('arg_train', 'runs')
        epsg = int(
            (rcf.read_config_file(self.cfg).getParam('chain',
                                                     'proj')).split(":")[-1])
        region_vec = rcf.read_config_file(self.cfg).getParam(
            'chain', 'region_path')
        region_field = (rcf.read_config_file(self.cfg).getParam(
            'chain', 'region_field')).lower()
        if not region_vec:
            region_vec = os.path.join(
                rcf.read_config_file(self.cfg).getParam(
                    "chain", "output_path"), "MyRegion.shp")

        merge_final_classifications = rcf.read_config_file(self.cfg).getParam(
            'arg_classification', 'merge_final_classifications')
        merge_final_classifications_ratio = rcf.read_config_file(
            self.cfg).getParam('arg_classification',
                               'merge_final_classifications_ratio')

        for tile in self.tiles:
            task = self.i2_task(task_name=f"vector_form_{tile}",
                                log_dir=self.log_step_dir,
                                execution_mode=self.execution_mode,
                                task_parameters={
                                    "f": VF.vector_formatting,
                                    "tile_name": tile,
                                    "output_path": output_path,
                                    "ground_truth_vec": ground_truth_vec,
                                    "data_field": data_field,
                                    "cloud_threshold": cloud_threshold,
                                    "ratio": ratio,
                                    "random_seed": random_seed,
                                    "enable_split_ground_truth":
                                    enable_split_ground_truth,
                                    "fusion_merge_all_validation":
                                    fusion_merge_all_validation,
                                    "runs": runs,
                                    "epsg": epsg,
                                    "region_field": region_field,
                                    "merge_final_classifications":
                                    merge_final_classifications,
                                    "merge_final_classifications_ratio":
                                    merge_final_classifications_ratio,
                                    "region_vec": region_vec,
                                    "classif_mode": classif_mode,
                                    "working_directory": self.working_directory
                                },
                                task_resources=self.get_resources())
            self.add_task_to_i2_processing_graph(
                task,
                task_group="tile_tasks",
                task_sub_group=tile,
                task_dep_dico={"vector": ["vector"]})

    @classmethod
    def step_description(cls):
        """
        function use to print a short description of the step's purpose
        """
        description = ("Prepare samples")
        return description
