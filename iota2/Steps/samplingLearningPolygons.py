#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
import os
import logging

from iota2.Steps import IOTA2Step
from iota2.Sampling import SamplesSelection
from iota2.configuration_files import read_config_file as rcf
LOGGER = logging.getLogger("distributed.worker")


class samplingLearningPolygons(IOTA2Step.Step):
    resources_block_name = "samplesSelection"

    def __init__(self, cfg, cfg_resources_file, workingDirectory=None):
        # heritage init
        super(samplingLearningPolygons,
              self).__init__(cfg, cfg_resources_file,
                             self.resources_block_name)

        # step variables
        self.working_directory = workingDirectory
        self.output_path = rcf.read_config_file(self.cfg).getParam(
            'chain', 'output_path')
        self.nb_runs = rcf.read_config_file(self.cfg).getParam(
            'arg_train', 'runs')
        epsg = rcf.read_config_file(self.cfg).getParam('chain', 'proj')
        parameters = dict(
            rcf.read_config_file(self.cfg).getParam('arg_train',
                                                    'sample_selection'))
        data_field = self.i2_const.re_encoding_label_name
        random_seed = rcf.read_config_file(self.cfg).getParam(
            'arg_train', 'random_seed')
        for model_name, model_meta in self.spatial_models_distribution.items():
            for seed in range(self.nb_runs):
                target_model = f"model_{model_name}_seed_{seed}"
                task = self.i2_task(
                    task_name=f"s_sel_{target_model}",
                    log_dir=self.log_step_dir,
                    execution_mode=self.execution_mode,
                    task_parameters={
                        "f":
                        SamplesSelection.samples_selection,
                        "model":
                        os.path.join(
                            self.output_path, "samplesSelection",
                            f"samples_region_{model_name}_seed_{seed}.shp"),
                        "working_directory":
                        self.working_directory,
                        "output_path":
                        self.output_path,
                        "runs":
                        self.nb_runs,
                        "epsg":
                        epsg,
                        "masks_name":
                        "MaskCommunSL.tif",
                        "parameters":
                        parameters,
                        "data_field":
                        data_field,
                        "random_seed":
                        random_seed
                    },
                    task_resources=self.get_resources())
                self.add_task_to_i2_processing_graph(
                    task,
                    task_group="region_tasks",
                    task_sub_group=target_model,
                    task_dep_dico={
                        "tile_tasks_model": [
                            f"{tile}_{model_name}_{seed}"
                            for tile in model_meta["tiles"]
                        ]
                    })

    @classmethod
    def step_description(cls):
        """
        function use to print a short description of the step's purpose
        """
        description = ("Select pixels in learning polygons by models")
        return description
