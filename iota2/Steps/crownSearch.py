#!/usr/bin/env python3
#-*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
import os
import logging

from iota2.Steps import IOTA2Step
from iota2.Iota2Cluster import get_RAM
from iota2.simplification import searchCrownTile as sct
from iota2.configuration_files import read_config_file as rcf

LOGGER = logging.getLogger("distributed.worker")


class crownSearch(IOTA2Step.Step):
    resources_block_name = "crownsearch"

    def __init__(self, cfg, cfg_resources_file, workingDirectory=None):
        # heritage init
        super(crownSearch, self).__init__(cfg, cfg_resources_file,
                                          self.resources_block_name)

        # step variables
        self.RAM = 1024.0 * get_RAM(self.get_resources()["ram"])
        self.CPU = self.get_resources()["cpu"]
        self.workingDirectory = workingDirectory
        self.outputPath = rcf.read_config_file(self.cfg).getParam(
            'chain', 'output_path')
        self.gridsize = rcf.read_config_file(self.cfg).getParam(
            'simplification', 'gridsize')
        tmpdir = os.path.join(self.outputPath, 'simplification', 'tmp')

        for grid_tile in range(self.gridsize**2):
            task = self.i2_task(task_name=f"crown_search_{grid_tile}",
                                log_dir=self.log_step_dir,
                                execution_mode=self.execution_mode,
                                task_parameters={
                                    "f":
                                    sct.searchCrownTile,
                                    "inpath":
                                    tmpdir,
                                    "raster":
                                    os.path.join(self.outputPath,
                                                 "simplification",
                                                 "classif_regul_clump.tif"),
                                    "clump":
                                    os.path.join(self.outputPath,
                                                 "simplification",
                                                 "clump32bits.tif"),
                                    "ram":
                                    self.RAM,
                                    "grid":
                                    os.path.join(self.outputPath,
                                                 "simplification", "grid.shp"),
                                    "outpath":
                                    os.path.join(self.outputPath,
                                                 "simplification", "tiles"),
                                    "nbcore":
                                    self.CPU,
                                    "ngrid":
                                    grid_tile,
                                    "working_dir":
                                    self.workingDirectory
                                },
                                task_resources=self.get_resources())
            self.add_task_to_i2_processing_graph(
                task,
                task_group="tile_grid",
                task_sub_group=f"tile_grid_{grid_tile}",
                task_dep_dico={"clump": ["clump"]})

    @classmethod
    def step_description(cls):
        """
        function use to print a short description of the step's purpose
        """
        description = ("Search crown entities for serialization process")
        return description
