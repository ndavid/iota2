#!/usr/bin/env python3
#-*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
import os
import logging

from iota2.Steps import IOTA2Step
from iota2.Iota2Cluster import get_RAM
from iota2.simplification import manageRegularization as mr
from iota2.configuration_files import read_config_file as rcf

LOGGER = logging.getLogger("distributed.worker")


class Regularization(IOTA2Step.Step):
    resources_block_name = "regularisation"

    def __init__(self,
                 cfg,
                 cfg_resources_file,
                 umc,
                 step_name,
                 raster_to_regul=None,
                 nomenclature=None,
                 workingDirectory=None):

        # TODO : ask what happen if nomenclature is None

        # heritage init
        super(Regularization, self).__init__(cfg, cfg_resources_file,
                                             self.resources_block_name)

        self.RAM = 1024.0 * get_RAM(self.get_resources()["ram"])
        self.CPU = self.get_resources()["cpu"]
        self.workingDirectory = workingDirectory
        self.outputPath = rcf.read_config_file(self.cfg).getParam(
            'chain', 'output_path')
        self.raster_to_regul = rcf.read_config_file(self.cfg).getParam(
            'simplification', 'classification')
        self.outtmpdir = os.path.join(self.outputPath, 'simplification', 'tmp')

        for rule_num, rule in enumerate(
                mr.getMaskRegularisation(nomenclature)):
            task = self.i2_task(task_name=f"{step_name}_rule_{rule_num}",
                                log_dir=self.log_step_dir,
                                execution_mode=self.execution_mode,
                                task_parameters={
                                    "f": mr.adaptRegularization,
                                    "working_dir": workingDirectory,
                                    "raster": raster_to_regul,
                                    "output":
                                    os.path.join(self.outtmpdir, rule[2]),
                                    "ram": self.RAM,
                                    "rule": rule,
                                    "threshold": umc,
                                },
                                task_resources=self.get_resources())
            dep_dico = {"first_task": []}
            if step_name != "regul_1":
                dep_dico = {"merge_regul": ["merge_regul"]}
            self.add_task_to_i2_processing_graph(
                task,
                task_group="regul",
                task_sub_group=f"regul_{rule_num}",
                task_dep_dico=dep_dico)

    @classmethod
    def step_description(cls):
        """
        function use to print a short description of the step's purpose
        """
        description = ("regularisation of classification raster")
        return description
