#!/usr/bin/env python3
#-*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
import os
import logging

from iota2.Steps import IOTA2Step
from iota2.Iota2Cluster import get_RAM
import iota2.Common.i2_constants as i2_const
from iota2.simplification import VectAndSimp as vas
from iota2.VectorTools import vector_functions as vf
from iota2.configuration_files import read_config_file as rcf

LOGGER = logging.getLogger("distributed.worker")
I2_CONST = i2_const.iota2_constants()


class largeVectorization(IOTA2Step.Step):

    resources_block_name = "vectorization"

    def __init__(self, cfg, cfg_resources_file, workingDirectory=None):
        # heritage init
        super(largeVectorization, self).__init__(cfg, cfg_resources_file,
                                                 self.resources_block_name)
        # step variables
        self.output_path = rcf.read_config_file(self.cfg).getParam(
            'chain', 'output_path')
        self.out_mos = os.path.join(self.output_path, "simplification",
                                    "mosaic")
        self.clip_file = rcf.read_config_file(self.cfg).getParam(
            "simplification", "clipfile")
        self.grid = os.path.join(self.output_path, "simplification",
                                 "grid.shp")
        self.clip_field = rcf.read_config_file(self.cfg).getParam(
            "simplification", "clipfield")
        self.grasslib = rcf.read_config_file(self.cfg).getParam(
            "simplification", "grasslib")
        self.angle = rcf.read_config_file(self.cfg).getParam(
            "simplification", "angle")
        self.epsg = int(
            rcf.read_config_file(self.cfg).getParam("chain",
                                                    "proj").split(":")[-1])
        tmpdir = os.path.join(self.output_path, "simplification", "tmp")
        if self.clip_file is None:
            self.clip_file = os.path.join(self.output_path, "clip.shp")
        if self.clip_field is None:
            self.clip_field = I2_CONST.i2_vecto_clip_field

        if os.path.exists(self.grid):
            list_fid = vf.getFIDSpatialFilter(self.clip_file, self.grid,
                                              self.clip_field)
            for fid in list_fid:
                task = self.i2_task(
                    task_name=f"large_vecto_region_{fid}",
                    log_dir=self.log_step_dir,
                    execution_mode=self.execution_mode,
                    task_parameters={
                        "f": vas.topologicalPolygonize,
                        "path": tmpdir,
                        "grasslib": self.grasslib,
                        "raster":
                        f"{self.out_mos}/tile_{self.clip_field}_{fid}.tif",
                        "angle": self.angle,
                        "out":
                        f"{self.out_mos}/tile_{self.clip_field}_{fid}.shp",
                        "epsg": self.epsg,
                        "working_dir": workingDirectory,
                    },
                    task_resources=self.get_resources())
                self.add_task_to_i2_processing_graph(
                    task,
                    task_group="vectorization",
                    task_sub_group=f"vectorization_{fid}",
                    task_dep_dico={"regions": [f"region_{fid}"]})
        else:
            task = self.i2_task(
                task_name=f"large_vecto",
                log_dir=self.log_step_dir,
                execution_mode=self.execution_mode,
                task_parameters={
                    "f":
                    vas.topologicalPolygonize,
                    "path":
                    tmpdir,
                    "grasslib":
                    self.grasslib,
                    "raster":
                    os.path.join(self.output_path, "simplification",
                                 "classif_regul.tif"),
                    "angle":
                    self.angle,
                    "out":
                    f"{self.out_mos}/tile_{self.clip_field}_{I2_CONST.i2_vecto_clip_value}.shp",
                    #"out":
                    # os.path.join(self.output_path,
                    #              "simplification",
                    #              "classif_regul.shp"),
                    "epsg":
                    self.epsg,
                    "working_dir":
                    workingDirectory,
                },
                task_resources=self.get_resources())
            self.add_task_to_i2_processing_graph(
                task,
                task_group="vectorization",
                task_sub_group=f"vectorization_{I2_CONST.i2_vecto_clip_value}",
                task_dep_dico={"clump": ["clump"]})

    @classmethod
    def step_description(cls):
        """
        function use to print a short description of the step's purpose
        """
        description = (
            "Vectorisation of classification (Serialisation strategy)")
        return description
