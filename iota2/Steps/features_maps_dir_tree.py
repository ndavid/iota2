#!/usr/bin/env python3
#-*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
import os
import logging

from iota2.Steps import IOTA2Step
from iota2.configuration_files import read_config_file as rcf
from iota2.Common import IOTA2Directory as IOTA2_dir

LOGGER = logging.getLogger("distributed.worker")


class features_maps_dir_tree(IOTA2Step.Step):
    resources_block_name = "iota2_dir"

    def __init__(self, cfg, cfg_resources_file):
        # heritage init
        resources_block_name = self.resources_block_name
        super(features_maps_dir_tree, self).__init__(cfg, cfg_resources_file,
                                                     resources_block_name)
        output_path = rcf.read_config_file(self.cfg).getParam(
            'chain', 'output_path')
        dir_task = self.i2_task(
            task_name="generate_directories",
            log_dir=self.log_dir,
            execution_mode=self.execution_mode,
            task_parameters={
                "f": IOTA2_dir.generate_features_maps_directories,
                "root": output_path
            },
            task_resources=self.get_resources())
        self.add_task_to_i2_processing_graph(dir_task, "first_task")

    @classmethod
    def step_description(cls):
        """
        function use to print a short description of the step's purpose
        """
        description = ("Construct features maps output directories")
        return description
