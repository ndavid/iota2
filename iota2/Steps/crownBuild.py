#!/usr/bin/env python3
#-*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
import os
import logging

from iota2.Steps import IOTA2Step
from iota2.Iota2Cluster import get_RAM
from iota2.simplification import buildCrownRaster as bcr
from iota2.configuration_files import read_config_file as rcf

LOGGER = logging.getLogger("distributed.worker")


class crownBuild(IOTA2Step.Step):
    resources_block_name = "crownbuild"

    def __init__(self, cfg, cfg_resources_file, workingDirectory=None):
        # heritage init
        super(crownBuild, self).__init__(cfg, cfg_resources_file,
                                         self.resources_block_name)

        # step variables
        self.ram = 1024.0 * get_RAM(self.get_resources()["ram"])
        self.workingDirectory = workingDirectory
        self.outputPath = rcf.read_config_file(self.cfg).getParam(
            'chain', 'output_path')
        self.gridsize = rcf.read_config_file(self.cfg).getParam(
            'simplification', 'gridsize')
        self.blocksize = rcf.read_config_file(self.cfg).getParam(
            'simplification', 'blocksize')

        tmpdir = os.path.join(self.outputPath, "simplification", "tmp")
        outpathtile = os.path.join(self.outputPath, "simplification", "tiles")
        tiles_list = os.path.join(self.outputPath, "simplification", "tiles")

        for grid_tile in range(self.gridsize**2):
            task = self.i2_task(task_name=f"crown_build_{grid_tile}",
                                log_dir=self.log_step_dir,
                                execution_mode=self.execution_mode,
                                task_parameters={
                                    "f": bcr.manageBlocks,
                                    "pathCrowns": tiles_list,
                                    "tilenumber": grid_tile,
                                    "blocksize": self.blocksize,
                                    "inpath": tmpdir,
                                    "outpath": outpathtile,
                                    "ram": self.ram,
                                    "working_dir": self.workingDirectory,
                                },
                                task_resources=self.get_resources())
            self.add_task_to_i2_processing_graph(
                task,
                task_group="tile_grid",
                task_sub_group=f"tile_grid_{grid_tile}",
                task_dep_dico={"tile_grid": [f"tile_grid_{grid_tile}"]})

    @classmethod
    def step_description(cls):
        """
        function use to print a short description of the step's purpose
        """
        description = ("Build crown raster for serialization process")
        return description

    # def step_inputs(self):
    #     """
    #     Return
    #     ------
    #         the return could be and iterable or a callable
    #     """
    #     return list(range(self.gridsize * self.gridsize))

    # def step_execute(self):
    #     """
    #     Return
    #     ------
    #     lambda
    #         the function to execute as a lambda function. The returned object
    #         must be a lambda function.
    #     """
    #     from simplification import buildCrownRaster as bcr
    #     tmpdir = os.path.join(self.outputPath, 'final', 'simplification',
    #                           'tmp')
    #     if self.workingDirectory:
    #         tmpdir = self.workingDirectory
    #     tiles_list = os.path.join(self.outputPath, 'final', 'simplification',
    #                               'tiles')
    #     outpathtile = os.path.join(self.outputPath, 'final', 'simplification',
    #                                'tiles')
    #     step_function = lambda x: bcr.manageBlocks(
    #         tiles_list, x, self.blocksize, tmpdir, outpathtile, self.RAM)
    #     return step_function

    # def step_outputs(self):
    #     """
    #     """
    #     pass
