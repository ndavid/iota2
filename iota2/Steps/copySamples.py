#!/usr/bin/env python3
#-*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
import os
import logging

from iota2.Steps import IOTA2Step
from iota2.Sampling import DataAugmentation
from iota2.configuration_files import read_config_file as rcf
from iota2.VectorTools.vector_functions import get_re_encoding_labels_dic

LOGGER = logging.getLogger("distributed.worker")


class copySamples(IOTA2Step.Step):
    resources_block_name = "samplesManagement"

    def __init__(self, cfg, cfg_resources_file, workingDirectory=None):
        # heritage init
        super(copySamples, self).__init__(cfg, cfg_resources_file,
                                          self.resources_block_name)
        self.working_directory = workingDirectory
        self.output_path = rcf.read_config_file(self.cfg).getParam(
            'chain', 'output_path')
        self.dataField = self.i2_const.re_encoding_label_name
        self.sample_management = rcf.read_config_file(self.cfg).getParam(
            'arg_train', 'sample_management')
        nb_runs = rcf.read_config_file(self.cfg).getParam('arg_train', 'runs')
        self.suffix_list = ["usually"]
        if rcf.read_config_file(self.cfg).getParam(
                'arg_train', 'dempster_shafer_sar_opt_fusion') is True:
            self.suffix_list.append("SAR")

        ground_truth = rcf.read_config_file(self.cfg).getParam(
            'chain', 'ground_truth')
        user_data_field = rcf.read_config_file(self.cfg).getParam(
            'chain', 'data_field')
        user_labels_to_i2_labels = get_re_encoding_labels_dic(
            ground_truth, user_data_field)
        #inputs
        models_by_seeds = {}
        for seed in range(nb_runs):
            models_by_seeds[seed] = {"dep": [], "files": []}
            for suffix in self.suffix_list:
                for model_name, _ in self.spatial_models_distribution.items():
                    models_by_seeds[seed]["dep"].append(
                        f"model_{model_name}_seed_{seed}_{suffix}")
                    if suffix == "SAR":
                        models_by_seeds[seed]["files"].append(
                            os.path.join(
                                self.output_path, "learningSamples",
                                f"Samples_region_{model_name}_seed{seed}_learn_SAR.sqlite"
                            ))
                    else:
                        models_by_seeds[seed]["files"].append(
                            os.path.join(
                                self.output_path, "learningSamples",
                                f"Samples_region_{model_name}_seed{seed}_learn.sqlite"
                            ))
        for seed in range(nb_runs):
            task = self.i2_task(task_name=f"transfert_samples_seed_{seed}",
                                log_dir=self.log_step_dir,
                                execution_mode=self.execution_mode,
                                task_parameters={
                                    "f":
                                    DataAugmentation.DataAugmentationByCopy,
                                    "dataField": self.dataField.lower(),
                                    "csv_path": self.sample_management,
                                    "samplesSet":
                                    models_by_seeds[seed]["files"],
                                    "labels_table": user_labels_to_i2_labels,
                                    "workingDirectory": self.working_directory
                                },
                                task_resources=self.get_resources())
            self.add_task_to_i2_processing_graph(
                task,
                task_group="seed_tasks",
                task_sub_group=seed,
                task_dep_dico={"region_tasks": models_by_seeds[seed]["dep"]})

    @classmethod
    def step_description(cls):
        """
        function use to print a short description of the step's purpose
        """
        description = ("Copy samples between models according to user request")
        return description
