#!/usr/bin/env python3
#-*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
import os
import logging

from iota2.Steps import IOTA2Step
from iota2.Validation import ConfusionFusion as confFus
from iota2.configuration_files import read_config_file as rcf

LOGGER = logging.getLogger("distributed.worker")


class confusionSAROptMerge(IOTA2Step.Step):
    resources_block_name = "SAROptConfusionMatrixFusion"

    def __init__(self, cfg, cfg_resources_file, workingDirectory=None):
        # heritage init
        super(confusionSAROptMerge, self).__init__(cfg, cfg_resources_file,
                                                   self.resources_block_name)

        # step variables
        self.workingDirectory = workingDirectory
        self.output_path = rcf.read_config_file(self.cfg).getParam(
            'chain', 'output_path')
        self.data_field = self.i2_const.re_encoding_label_name
        self.runs = rcf.read_config_file(self.cfg).getParam(
            'arg_train', 'runs')
        self.suffix_list = ["usually"]
        if rcf.read_config_file(self.cfg).getParam(
                'arg_train', 'dempster_shafer_sar_opt_fusion') is True:
            self.suffix_list.append("SAR")
        for suffix in self.suffix_list:
            for model_name, model_meta in self.spatial_models_distribution.items(
            ):
                for seed in range(self.runs):
                    csv_files_to_merge = self.get_csv_files(
                        suffix, model_name, model_meta["tiles"], seed)
                    task_name_fusion = f"confusion_fusion_model_{model_name}_seed_{seed}"
                    if suffix == "SAR":
                        task_name_fusion = f"{task_name_fusion}_SAR"
                    task = self.i2_task(task_name=task_name_fusion,
                                        log_dir=self.log_step_dir,
                                        execution_mode=self.execution_mode,
                                        task_parameters={
                                            "f":
                                            confFus.confusion_models_merge,
                                            "csv_list": csv_files_to_merge
                                        },
                                        task_resources=self.get_resources())
                    self.add_task_to_i2_processing_graph(
                        task,
                        task_group="region_tasks",
                        task_sub_group=
                        f"model_{model_name}_seed_{seed}_{suffix}",
                        task_dep_dico={
                            "tile_tasks_model_mode": [
                                f"{tile}_{model_name}_{seed}_{suffix}"
                                for tile in model_meta["tiles"]
                            ]
                        })

    def get_csv_files(self, suffix, model_name, tiles, seed):
        csv_files = []
        for tile in tiles:
            if suffix == "SAR":
                csv_files.append(
                    os.path.join(
                        self.output_path, "dataAppVal", "bymodels",
                        f"{tile}_region_{model_name}_seed_{seed}_samples_val_SAR.csv"
                    ))
            else:
                csv_files.append(
                    os.path.join(
                        self.output_path, "dataAppVal", "bymodels",
                        f"{tile}_region_{model_name}_seed_{seed}_samples_val.csv"
                    ))
        return csv_files

    @classmethod
    def step_description(cls):
        """
        function use to print a short description of the step's purpose
        """
        description = ("Fusion of confusion matrix")
        return description
