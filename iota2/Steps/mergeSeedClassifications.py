#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
import os
import logging

from iota2.Steps import IOTA2Step
from iota2.Common import FileUtils as fut
from iota2.configuration_files import read_config_file as rcf
from iota2.Classification import MergeFinalClassifications as mergeCl
from iota2.VectorTools.vector_functions import get_re_encoding_labels_dic

LOGGER = logging.getLogger("distributed.worker")


class mergeSeedClassifications(IOTA2Step.Step):
    resources_block_name = "merge_final_classifications"

    def __init__(self, cfg, cfg_resources_file, workingDirectory=None):
        # heritage init
        super(mergeSeedClassifications,
              self).__init__(cfg, cfg_resources_file,
                             self.resources_block_name)

        # step variables
        self.workingDirectory = workingDirectory
        self.output_path = rcf.read_config_file(self.cfg).getParam(
            'chain', 'output_path')
        self.data_field = self.i2_const.re_encoding_label_name
        self.user_data_field = rcf.read_config_file(self.cfg).getParam(
            'chain', 'data_field')
        self.nomenclature = rcf.read_config_file(self.cfg).getParam(
            'chain', 'nomenclature_path')
        self.color_table = rcf.read_config_file(self.cfg).getParam(
            'chain', 'color_table')
        self.runs = rcf.read_config_file(self.cfg).getParam(
            'arg_train', 'runs')
        self.undecidedlabel = rcf.read_config_file(self.cfg).getParam(
            'arg_classification', 'merge_final_classifications_undecidedlabel')
        self.dempstershafer_mob = rcf.read_config_file(self.cfg).getParam(
            'arg_classification', 'dempstershafer_mob')
        self.keep_runs_results = rcf.read_config_file(self.cfg).getParam(
            'arg_classification', 'keep_runs_results')
        self.ground_truth = rcf.read_config_file(self.cfg).getParam(
            'chain', 'ground_truth')
        self.fusionClaAllSamplesVal = rcf.read_config_file(cfg).getParam(
            'arg_classification',
            'fusionofclassification_all_samples_validation')
        self.merge_final_classifications_method = rcf.read_config_file(
            cfg).getParam('arg_classification',
                          'merge_final_classifications_method')

        pixType = "uint8"
        validation_shape = None
        if self.fusionClaAllSamplesVal is True:
            validation_shape = self.ground_truth
        user_labels_to_i2_labels = get_re_encoding_labels_dic(
            self.ground_truth, self.user_data_field)
        i2_labels_to_user_labels = {
            v: k
            for k, v in user_labels_to_i2_labels.items()
        }
        i2_labels_to_user_labels_vector = i2_labels_to_user_labels.copy()

        all_castable = []
        for _, user_label in i2_labels_to_user_labels.items():
            try:
                __ = int(user_label)
                all_castable.append(True)
            except ValueError:
                all_castable.append(False)
        re_encode_labels = all(all_castable)
        if re_encode_labels:
            i2_labels_to_user_labels = None

        task = self.i2_task(task_name=f"final_report_merge",
                            log_dir=self.log_step_dir,
                            execution_mode=self.execution_mode,
                            task_parameters={
                                "f": mergeCl.mergeFinalClassifications,
                                "iota2_dir": self.output_path,
                                "dataField": self.data_field,
                                "nom_path": self.nomenclature,
                                "colorFile": self.color_table,
                                "runs": self.runs,
                                "pixType": pixType,
                                "method":
                                self.merge_final_classifications_method,
                                "undecidedlabel": self.undecidedlabel,
                                "dempstershafer_mob": self.dempstershafer_mob,
                                "keep_runs_results": self.keep_runs_results,
                                "validationShape": validation_shape,
                                "labels_raster_table":
                                i2_labels_to_user_labels,
                                "labels_vector_table":
                                i2_labels_to_user_labels_vector,
                                "workingDirectory": self.workingDirectory
                            },
                            task_resources=self.get_resources())
        self.add_task_to_i2_processing_graph(
            task,
            task_group="merge_final_classifications",
            task_sub_group="merge_final_classifications",
            task_dep_dico={"final_report": ["final_report"]})

    @classmethod
    def step_description(cls):
        """
        function use to print a short description of the step's purpose
        """
        description = ("Merge final classifications")
        return description
