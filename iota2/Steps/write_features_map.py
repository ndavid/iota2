#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
import logging
from iota2.Steps import IOTA2Step
from iota2.configuration_files import read_config_file as rcf
from iota2.Iota2Cluster import get_RAM
import iota2.Common.write_features_map as wfm
LOGGER = logging.getLogger("distributed.worker")


class write_features_map(IOTA2Step.Step):

    resources_block_name = "writeMaps"

    def __init__(self, cfg, cfg_resources_file, workingDirectory=None):
        # heritage init
        super(write_features_map, self).__init__(cfg, cfg_resources_file,
                                                 self.resources_block_name)

        # step variables
        self.workingDirectory = workingDirectory
        self.output_path = rcf.read_config_file(self.cfg).getParam(
            'chain', 'output_path')
        self.ram_extraction = 1024.0 * get_RAM(self.get_resources()["ram"])
        # read config file to init custom features and check validity
        self.external_features_flag = rcf.read_config_file(self.cfg).getParam(
            "external_features", "external_features_flag")
        if self.external_features_flag:
            self.number_of_chunks = rcf.read_config_file(self.cfg).getParam(
                'external_features', "number_of_chunks")
            self.chunk_size_mode = rcf.read_config_file(self.cfg).getParam(
                'external_features', "chunk_size_mode")

        for tile in self.tiles:
            sensors_params = rcf.iota2_parameters(
                self.cfg).get_sensors_parameters(tile)
            for i in range(self.number_of_chunks):
                task = self.i2_task(
                    task_name=f"write_features_{tile}_chunk_{i}",
                    log_dir=self.log_step_dir,
                    execution_mode=self.execution_mode,
                    task_parameters={
                        "f":
                        wfm.write_features_by_chunk,
                        "working_directory":
                        self.workingDirectory,
                        "output_path":
                        rcf.read_config_file(self.cfg).getParam(
                            'chain', 'output_path'),
                        "sar_optical_post_fusion":
                        rcf.read_config_file(self.cfg).getParam(
                            'arg_train', 'dempster_shafer_sar_opt_fusion'),
                        "module_path":
                        rcf.read_config_file(self.cfg).getParam(
                            'external_features', 'module'),
                        "list_functions":
                        rcf.read_config_file(self.cfg).getParam(
                            'external_features', 'functions').split(" "),
                        "force_standard_labels":
                        rcf.read_config_file(self.cfg).getParam(
                            'arg_train', 'force_standard_labels'),
                        "number_of_chunks":
                        self.number_of_chunks,
                        "chunk_size_mode":
                        self.chunk_size_mode,
                        "concat_mode":
                        rcf.read_config_file(self.cfg).getParam(
                            'external_features', "concat_mode"),
                        "sensors_parameters":
                        sensors_params,
                        "tile":
                        tile,
                        "targeted_chunk":
                        i
                    },
                    task_resources=self.get_resources())
                self.add_task_to_i2_processing_graph(
                    task,
                    task_group="writing maps",
                    task_sub_group=f"writing maps {tile} {i}",
                    task_dep_dico={"tile_tasks": [tile]})
        # implement tests for check if custom features are well provided
        # so the chain failed during step init
    @classmethod
    def step_description(self):
        """
        function use to print a short description of the step's purpose
        """
        description = ("Write features by chunk")
        return description
