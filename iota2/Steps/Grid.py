#!/usr/bin/env python3
#-*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
import os
import logging

from iota2.Steps import IOTA2Step
from iota2.Iota2Cluster import get_RAM
from iota2.simplification import GridGenerator as gridg
from iota2.configuration_files import read_config_file as rcf

LOGGER = logging.getLogger("distributed.worker")


class Grid(IOTA2Step.Step):
    resources_block_name = "grid"

    def __init__(self, cfg, cfg_resources_file, workingDirectory=None):
        # heritage init
        super(Grid, self).__init__(cfg, cfg_resources_file,
                                   self.resources_block_name)

        # step variables
        self.RAM = 1024.0 * get_RAM(self.get_resources()["ram"])
        self.workingDirectory = workingDirectory
        self.outputPath = rcf.read_config_file(self.cfg).getParam(
            'chain', 'output_path')
        self.gridsize = rcf.read_config_file(self.cfg).getParam(
            'simplification', 'gridsize')
        self.epsg = int(
            rcf.read_config_file(self.cfg).getParam('chain',
                                                    'proj').split(":")[-1])
        task = self.i2_task(task_name=f"grid",
                            log_dir=self.log_step_dir,
                            execution_mode=self.execution_mode,
                            task_parameters={
                                "f":
                                gridg.grid_generate,
                                "outname":
                                os.path.join(self.outputPath, "simplification",
                                             "grid.shp"),
                                "xysize":
                                self.gridsize,
                                "epsg":
                                self.epsg,
                                "raster":
                                os.path.join(self.outputPath, "simplification",
                                             "classif_regul_clump.tif"),
                                "coordinates":
                                None,
                            },
                            task_resources=self.get_resources())
        self.add_task_to_i2_processing_graph(
            task,
            task_group="grid",
            task_sub_group="grid",
            task_dep_dico={"clump": ["clump"]})

    @classmethod
    def step_description(cls):
        """
        function use to print a short description of the step's purpose
        """
        description = ("Generation of grid for serialisation")
        return description

    # def step_inputs(self):
    #     """
    #     Return
    #     ------
    #         the return could be and iterable or a callable
    #     """
    #     outfileclp = os.path.join(self.outputPath, 'final', 'simplification',
    #                               'classif_regul_clump.tif')
    #     return [outfileclp]

    # def step_execute(self):
    #     """
    #     Return
    #     ------
    #     lambda
    #         the function to execute as a lambda function. The returned object
    #         must be a lambda function.
    #     """
    #     from iota2.simplification import GridGenerator as gridg

    #     outfilegrid = os.path.join(self.outputPath, 'final', 'simplification',
    #                                'grid.shp')
    #     step_function = lambda x: gridg.grid_generate(
    #         outfilegrid, self.gridsize, int(self.epsg.split(':')[1]), x)
    #     return step_function

    # def step_outputs(self):
    #     """
    #     """
    #     pass
