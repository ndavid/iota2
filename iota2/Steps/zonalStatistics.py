#!/usr/bin/env python3
#-*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
import os
import logging
from typing import List, Tuple

from iota2.Steps import IOTA2Step
from iota2.simplification import ZonalStats as zs
from iota2.configuration_files import read_config_file as rcf
LOGGER = logging.getLogger("distributed.worker")

LOGGER = logging.getLogger("distributed.worker")


class zonalStatistics(IOTA2Step.Step):
    resources_block_name = "statistics"

    def __init__(self,
                 cfg,
                 cfg_resources_file,
                 classification,
                 confidence,
                 validity,
                 workingDirectory=None):
        # heritage init
        super(zonalStatistics, self).__init__(cfg, cfg_resources_file,
                                              self.resources_block_name)

        # step variables
        self.chunk = rcf.read_config_file(self.cfg).getParam(
            "simplification", "chunk")
        self.output_path = rcf.read_config_file(self.cfg).getParam(
            "chain", "output_path")
        self.stats_list = rcf.read_config_file(self.cfg).getParam(
            "simplification", "statslist")
        self.nomenclature = rcf.read_config_file(self.cfg).getParam(
            "simplification", "nomenclature")
        self.bin_gdal = rcf.read_config_file(self.cfg).getParam(
            "simplification", "bingdal")
        self.system_call = rcf.read_config_file(self.cfg).getParam(
            "simplification", "systemcall")
        self.classif = classification
        self.confidence = confidence
        self.validity = validity

        tmpdir = os.path.join(self.output_path, "simplification", "tmp")
        for in_vector, fids_features, out_vector in self.regions_stats_partition:
            out_vect_name = os.path.splitext(os.path.basename(out_vector))[0]
            task = self.i2_task(
                task_name=out_vect_name,
                log_dir=self.log_step_dir,
                execution_mode=self.execution_mode,
                task_parameters={
                    "f": zs.zonalstats,
                    "path": tmpdir,
                    "rasters": [self.classif, self.confidence, self.validity],
                    "params": [in_vector, fids_features],
                    "output": out_vector,
                    "paramstats": self.stats_list,
                    "classes": self.nomenclature,
                    "gdalpath": self.bin_gdal,
                    "systemcall": self.system_call,
                    "working_dir": workingDirectory
                },
                task_resources=self.get_resources())
            self.add_task_to_i2_processing_graph(
                task,
                task_group="zonal_stat",
                task_sub_group=f"zonal_stat_{out_vect_name}",
                task_dep_dico={})

    @classmethod
    def step_description(cls):
        """
        function use to print a short description of the step's purpose
        """
        description = (
            "Compute statistics for each polygon of the classification")
        return description
