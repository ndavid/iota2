#!/usr/bin/env python3
#-*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================

import os
import logging

from iota2.Steps import IOTA2Step
import iota2.Common.i2_constants as i2_const
from simplification import VectAndSimp as vas
from iota2.VectorTools import vector_functions as vf
from iota2.simplification import MergeTileRasters as mtr
from iota2.configuration_files import read_config_file as rcf

LOGGER = logging.getLogger("distributed.worker")
I2_CONST = i2_const.iota2_constants()


class clipVectors(IOTA2Step.Step):
    resources_block_name = "clipvectors"

    def __init__(self, cfg, cfg_resources_file, workingDirectory=None):
        # heritage init
        super(clipVectors, self).__init__(cfg, cfg_resources_file,
                                          self.resources_block_name)
        self.output_path = rcf.read_config_file(self.cfg).getParam(
            "chain", "output_path")
        self.grid = os.path.join(self.output_path, "simplification",
                                 "grid.shp")
        self.clip_file = rcf.read_config_file(self.cfg).getParam(
            "simplification", "clipfile")
        self.clip_field = rcf.read_config_file(self.cfg).getParam(
            "simplification", "clipfield")
        self.clip_value = rcf.read_config_file(self.cfg).getParam(
            "simplification", "clipvalue")
        self.out_mos = os.path.join(self.output_path, "simplification",
                                    "mosaic")
        self.out_prefix = rcf.read_config_file(self.cfg).getParam(
            "simplification", "outprefix")
        self.out_dir_vect = os.path.join(self.output_path, "simplification",
                                         "vectors")
        if self.clip_file is None:
            self.clip_file = os.path.join(self.output_path, "clip.shp")
        if self.clip_field is None:
            self.clip_field = I2_CONST.i2_vecto_clip_field

        tmpdir = os.path.join(self.output_path, "simplification", "tmp")
        if os.path.exists(self.grid):
            list_fid = vf.getFIDSpatialFilter(self.clip_file, self.grid,
                                              self.clip_field)
            for fid in list_fid:
                task = self.i2_task(
                    task_name=f"clip_vector_{fid}",
                    log_dir=self.log_step_dir,
                    execution_mode=self.execution_mode,
                    task_parameters={
                        "f":
                        vas.clipVectorfile,
                        "path":
                        tmpdir,
                        "vector":
                        os.path.join(
                            self.out_mos,
                            f"tile_{self.clip_field}_{fid}_douglas_hermite.shp"
                        ),
                        "clipfile":
                        self.clip_file,
                        "clipfield":
                        self.clip_field,
                        "clipvalue":
                        fid,
                        "outpath":
                        self.out_dir_vect,
                        "prefix":
                        self.out_prefix,
                        "working_dir":
                        workingDirectory
                    },
                    task_resources=self.get_resources())
                self.add_task_to_i2_processing_graph(
                    task,
                    task_group="clip_vecto",
                    task_sub_group=f"clip_vecto_{fid}",
                    task_dep_dico={"smoothing": [f"smoothing_{fid}"]})
        else:

            if self.clip_value is None:
                list_clip_vals = vf.ListValueFields(self.clip_file,
                                                    self.clip_field)
            else:
                list_clip_vals = [self.clip_value]

            for val in list_clip_vals:
                task = self.i2_task(
                    task_name=f"clip_vector_{val}",
                    log_dir=self.log_step_dir,
                    execution_mode=self.execution_mode,
                    task_parameters={
                        "f":
                        vas.clipVectorfile,
                        "path":
                        tmpdir,
                        "vector":
                        os.path.join(
                            self.out_mos,
                            f"tile_{self.clip_field}_{I2_CONST.i2_vecto_clip_value}_douglas_hermite.shp"
                        ),
                        "clipfile":
                        self.clip_file,
                        "clipfield":
                        self.clip_field,
                        "clipvalue":
                        val,
                        "outpath":
                        self.out_dir_vect,
                        "prefix":
                        self.out_prefix,
                        "working_dir":
                        workingDirectory
                    },
                    task_resources=self.get_resources())
                self.add_task_to_i2_processing_graph(
                    task,
                    task_group="clip_vecto",
                    task_sub_group=f"clip_vecto_{val}",
                    task_dep_dico={
                        "smoothing":
                        [f"smoothing_{I2_CONST.i2_vecto_clip_value}"]
                    })

    @classmethod
    def step_description(cls):
        """
        function use to print a short description of the step's purpose
        """
        description = (
            "Clip vector files for each feature of clipfile parameters")
        return description
