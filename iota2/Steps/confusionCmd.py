#!/usr/bin/env python3
#-*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
import os
import logging

from iota2.Steps import IOTA2Step
from iota2.Iota2Cluster import get_RAM
from iota2.Validation import GenConfusionMatrix as GCM
from iota2.configuration_files import read_config_file as rcf
from iota2.VectorTools.vector_functions import get_re_encoding_labels_dic

LOGGER = logging.getLogger("distributed.worker")


class confusionCmd(IOTA2Step.Step):
    resources_block_name = "gen_confusionMatrix"

    def __init__(self, cfg, cfg_resources_file, workingDirectory=None):
        # heritage init
        super(confusionCmd, self).__init__(cfg, cfg_resources_file,
                                           self.resources_block_name)

        # step variables
        self.workingDirectory = workingDirectory
        self.output_path = rcf.read_config_file(self.cfg).getParam(
            'chain', 'output_path')
        self.runs = rcf.read_config_file(self.cfg).getParam(
            'arg_train', 'runs')
        self.data_field = self.i2_const.re_encoding_label_name

        ground_truth = rcf.read_config_file(self.cfg).getParam(
            'chain', 'ground_truth')
        user_data_field = rcf.read_config_file(self.cfg).getParam(
            'chain', 'data_field')
        user_labels_to_i2_labels = get_re_encoding_labels_dic(
            ground_truth, user_data_field)
        i2_labels_to_user_labels = dict([
            (v, k) for k, v in user_labels_to_i2_labels.items()
        ])
        for seed in range(self.runs):
            for tile in self.tiles:
                task = self.i2_task(
                    task_name=f"confusion_{tile}_seed_{seed}",
                    log_dir=self.log_step_dir,
                    execution_mode=self.execution_mode,
                    task_parameters={
                        "f":
                        GCM.gen_conf_matrix,
                        "in_classif":
                        os.path.join(self.output_path, "final",
                                     f"Classif_Seed_{seed}.tif"),
                        "out_csv":
                        os.path.join(self.output_path, "final", "TMP",
                                     f"{tile}_seed_{seed}.csv"),
                        "data_field":
                        self.data_field,
                        "ref_vector":
                        os.path.join(self.output_path, "dataAppVal",
                                     f"{tile}_seed_{seed}_val.sqlite"),
                        "ram":
                        1024.0 * get_RAM(self.get_resources()["ram"]),
                        "labels_table":
                        i2_labels_to_user_labels
                    },
                    task_resources=self.get_resources())
                self.add_task_to_i2_processing_graph(
                    task,
                    task_group="tile_tasks_seed",
                    task_sub_group=f"{tile}_{seed}",
                    task_dep_dico={"mosaic": ["mosaic"]})

    @classmethod
    def step_description(cls):
        """
        function use to print a short description of the step's purpose
        """
        description = ("generate confusion matrix by tiles")
        return description
