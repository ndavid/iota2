#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================

# python -m unittest running_Tests
"""
Tests dedicated to launch iota2 runs
"""
import os
import sys
import shutil
import logging
import unittest
from config import Config

import iota2.Tests.UnitTests.tests_utils.tests_utils_rasters as TUR
from iota2.Tests.UnitTests.tests_utils.tests_utils_launcher import run_cmd

IOTA2DIR = os.environ.get('IOTA2DIR')

if not IOTA2DIR:
    raise Exception("IOTA2DIR environment variable must be set")

# if all tests pass, remove 'iota2_tests_directory' which contains all
# sub-directory tests
RM_IF_ALL_OK = True

IOTA2_SCRIPTS = os.path.join(IOTA2DIR, "iota2")
sys.path.append(IOTA2_SCRIPTS)

LOGGER = logging.getLogger("distributed.worker")


class iota2_tests_features_map_IT(unittest.TestCase):
    """
    Tests dedicated to launch iota2 runs
    """
    # before launching tests
    @classmethod
    def setUpClass(cls):
        # definition of local variables
        cls.group_test_name = "iota2_tests_features_map"
        cls.iota2_tests_directory = os.path.join(IOTA2DIR, "data",
                                                 cls.group_test_name)
        cls.all_tests_ok = []

        # input data
        cls.config_ref = os.path.join(IOTA2DIR, "data", "references",
                                      "running_iota2",
                                      "i2_config_feat_map.cfg")
        cls.dhi_module = os.path.join(IOTA2DIR, "data", "references",
                                      "running_iota2", "dhi.py")
        # Tests directory
        cls.test_working_directory = None
        if os.path.exists(cls.iota2_tests_directory):
            shutil.rmtree(cls.iota2_tests_directory, ignore_errors=True)
        os.mkdir(cls.iota2_tests_directory)

    @classmethod
    def tearDownClass(cls):
        """after launching all tests"""
        print("{} ended".format(cls.group_test_name))
        if RM_IF_ALL_OK and all(cls.all_tests_ok):
            shutil.rmtree(cls.iota2_tests_directory, ignore_errors=True)

    # before launching a test
    def setUp(self):
        """
        create test environement (directories)
        """
        # self.test_working_directory is the diretory dedicated to each tests
        # it changes for each tests

        test_name = self.id().split(".")[-1]
        self.test_working_directory = os.path.join(self.iota2_tests_directory,
                                                   test_name)
        if os.path.exists(self.test_working_directory):
            shutil.rmtree(self.test_working_directory, ignore_errors=True)
        os.mkdir(self.test_working_directory)

    def list2reason(self, exc_list):
        """list2reason"""
        out = None
        if exc_list and exc_list[-1][0] is self:
            out = exc_list[-1][1]
        return out

    def tearDown(self):
        """after launching a test, remove test's data if test succeed"""
        result = self.defaultTestResult()
        self._feedErrorsToResult(result, self._outcome.errors)
        error = self.list2reason(result.errors)
        failure = self.list2reason(result.failures)
        ok_test = not error and not failure

        self.all_tests_ok.append(ok_test)
        if ok_test:
            shutil.rmtree(self.test_working_directory, ignore_errors=True)

    def test_dhi_run(self):
        """Test dhi map generation
        """
        tile_name = "T31TCJ"
        fake_s2_theia_dir = os.path.join(self.test_working_directory,
                                         "s2_data")
        TUR.generate_fake_s2_data(fake_s2_theia_dir,
                                  tile_name,
                                  ["20200101", "20200112", "20200127"],
                                  res=10.0)
        running_output_path = os.path.join(self.test_working_directory,
                                           "test_results")
        fake_s2_theia_dir = os.path.join(self.test_working_directory,
                                         "s2_data")
        config_test = os.path.join(self.test_working_directory,
                                   "i2_config_dhi_map.cfg")
        shutil.copy(self.config_ref, config_test)
        cfg_test = Config(open(config_test))
        cfg_test.chain.output_path = running_output_path
        cfg_test.chain.s2_path = fake_s2_theia_dir
        cfg_test.chain.spatial_resolution = 10
        cfg_test.chain.list_tile = tile_name

        cfg_test.external_features.module = self.dhi_module

        cfg_test.save(open(config_test, 'w'))

        final_dhi_raster = os.path.join(running_output_path, "final",
                                        "features_map.tif")
        if os.path.exists(final_dhi_raster):
            os.remove(final_dhi_raster)
        cmd = f"Iota2.py -config {config_test} -starting_step 1 -ending_step 20 -scheduler_type local"
        run_cmd(cmd)
        self.assertTrue(os.path.exists(final_dhi_raster))
