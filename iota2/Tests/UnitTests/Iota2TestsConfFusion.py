#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================

# python -m unittest Iota2TestsConfFusion

import os
import sys
import shutil
import unittest
import filecmp
from iota2.configuration_files import read_config_file as rcf

IOTA2DIR = os.environ.get("IOTA2DIR")

if not IOTA2DIR:
    raise Exception("IOTA2DIR environment variable must be set")
# if all tests pass, remove 'iota2_tests_directory' which contains all
# sub-directory tests
RM_IF_ALL_OK = True


class iota_testConfFusion(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        # definition of local variables
        cls.group_test_name = "Iota2TestsConfFusion"
        cls.iota2_tests_directory = os.path.join(IOTA2DIR, "data",
                                                 cls.group_test_name)
        cls.all_tests_ok = []

        # definition of local variables
        cls.fichier_config = os.path.join(
            IOTA2DIR, "config", "Config_4Tuiles_Multi_FUS_Confidence.cfg")
        cls.ref_data = os.path.join(IOTA2DIR, "data", "references",
                                    "ConfFusion")
        # Tests directory
        cls.test_working_directory = None
        if os.path.exists(cls.iota2_tests_directory):
            shutil.rmtree(cls.iota2_tests_directory)
        os.mkdir(cls.iota2_tests_directory)

    # after launching all tests
    @classmethod
    def tearDownClass(cls):
        print("{} ended".format(cls.group_test_name))
        if RM_IF_ALL_OK and all(cls.all_tests_ok):
            shutil.rmtree(cls.iota2_tests_directory)

    # before launching a test
    def setUp(self):
        """
        create test environement (directories)
        """
        # self.test_working_directory is the diretory dedicated to each tests
        # it changes for each tests

        test_name = self.id().split(".")[-1]
        self.test_working_directory = os.path.join(self.iota2_tests_directory,
                                                   test_name)
        if os.path.exists(self.test_working_directory):
            shutil.rmtree(self.test_working_directory)
        os.mkdir(self.test_working_directory)

    def list2reason(self, exc_list):
        if exc_list and exc_list[-1][0] is self:
            return exc_list[-1][1]

    # after launching a test, remove test's data if test succeed
    def tearDown(self):
        if sys.version_info > (3, 4, 0):
            result = self.defaultTestResult()
            self._feedErrorsToResult(result, self._outcome.errors)
        else:
            result = getattr(self, "_outcomeForDoCleanups",
                             self._resultForDoCleanups)
        error = self.list2reason(result.errors)
        failure = self.list2reason(result.failures)
        ok = not error and not failure

        self.all_tests_ok.append(ok)
        if ok:
            shutil.rmtree(self.test_working_directory)

    def test_ConfFusion(self):
        from iota2.Validation import ConfusionFusion as confFus

        # Prepare data
        final = os.path.join(self.test_working_directory, "final")
        final_tmp = os.path.join(final, "TMP")
        # test and creation of Final
        if not os.path.exists(final):
            os.mkdir(final)
        if not os.path.exists(final_tmp):
            os.mkdir(final_tmp)

        # copy input data
        src_files = os.listdir(
            os.path.join(self.ref_data, "Input", "final", "TMP"))
        for file_name in src_files:
            full_file_name = os.path.join(self.ref_data, "Input", "final",
                                          "TMP", file_name)
            shutil.copy(full_file_name, final_tmp)

        # run test

        rcf.clearConfig()
        cfg = rcf.read_config_file(self.fichier_config)
        cfg.setParam('chain', 'output_path', self.test_working_directory)

        datafield = 'CODE'
        shape_data = os.path.join(IOTA2DIR, "data", "references",
                                  "D5H2_groundTruth_samples.shp")
        confFus.confusion_fusion(shape_data, datafield, final + "/TMP",
                                 final + "/TMP", final + "/TMP", 1, False, [],
                                 None)

        # file comparison to ref file
        file1 = os.path.join(final, "TMP", "ClassificationResults_seed_0.txt")
        referencefile1 = os.path.join(self.ref_data, "Output",
                                      "ClassificationResults_seed_0.txt")
        self.assertTrue(filecmp.cmp(file1, referencefile1))
