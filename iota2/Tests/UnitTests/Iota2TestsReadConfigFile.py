#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================

# python -m unittest Iota2TestsReadConfigFile

import os
import sys
import shutil
import unittest
from config import Config

from iota2.configuration_files import read_config_file as rcf
IOTA2DIR = os.environ.get("IOTA2DIR")

if not IOTA2DIR:
    raise Exception("IOTA2DIR environment variable must be set")
# if all tests pass, remove 'iota2_tests_directory' which contains all
# sub-directory tests
RM_IF_ALL_OK = True


class Iota2TestReadConfigFile(unittest.TestCase):
    @classmethod
    def setUpClass(cls):

        cls.group_test_name = "Iota2TestsReadConfigFile"
        cls.iota2_tests_directory = os.path.join(IOTA2DIR, "data",
                                                 cls.group_test_name)
        cls.all_tests_ok = []
        # the configuration file tested must be the one in /config.
        cls.fichierconfig = os.path.join(
            IOTA2DIR, "config", "Config_4Tuiles_Multi_FUS_Confidence.cfg")
        cls.fichierconfigbad1 = os.path.join(IOTA2DIR, "data", "config",
                                             "test_bad_config_boolean.cfg")
        cls.fichierconfigbad2 = os.path.join(IOTA2DIR, "data", "config",
                                             "test_bad_config_int.cfg")
        cls.fichierconfigbad3 = os.path.join(IOTA2DIR, "data", "config",
                                             "test_bad_config_resolution.cfg")
        cls.test_working_directory = None

        cls.config_ref = os.path.join(IOTA2DIR, "data", "references",
                                      "running_iota2",
                                      "i2_config_feat_map.cfg")

        if os.path.exists(cls.iota2_tests_directory):
            shutil.rmtree(cls.iota2_tests_directory)
        os.mkdir(cls.iota2_tests_directory)

    @classmethod
    def tearDownClass(cls):
        print("{} ended".format(cls.group_test_name))
        if RM_IF_ALL_OK and all(cls.all_tests_ok):
            shutil.rmtree(cls.iota2_tests_directory)

    # before launching a test
    def setUp(self):
        """
        create test environement (directories)
        """
        # self.test_working_directory is the diretory dedicated to each tests
        # it changes for each tests

        test_name = self.id().split(".")[-1]
        self.test_working_directory = os.path.join(self.iota2_tests_directory,
                                                   test_name)
        if os.path.exists(self.test_working_directory):
            shutil.rmtree(self.test_working_directory)
        os.mkdir(self.test_working_directory)

    def list2reason(self, exc_list):
        if exc_list and exc_list[-1][0] is self:
            return exc_list[-1][1]

    # after launching a test, remove test's data if test succeed
    def tearDown(self):
        if sys.version_info > (3, 4, 0):
            result = self.defaultTestResult()
            self._feedErrorsToResult(result, self._outcome.errors)
        else:
            result = getattr(self, "_outcomeForDoCleanups",
                             self._resultForDoCleanups)
        error = self.list2reason(result.errors)
        failure = self.list2reason(result.failures)
        ok = not error and not failure

        self.all_tests_ok.append(ok)
        if ok:
            shutil.rmtree(self.test_working_directory)

    def test_initConfigFile(self):

        # the class is instantiated with self.fichierConfig config file
        rcf.clearConfig()
        cfg = rcf.read_config_file(self.fichierconfig)
        cfg.setParam('arg_train', 'runs', 2)
        cfg.setParam('chain', 'region_path',
                     '../../../data/references/region_need_To_env.shp')
        cfg.setParam('chain', 'region_field', 'DN_char')
        cfg.setParam('arg_classification', 'classif_mode', 'separate')

        # we check the config file
        # Not required now as is intergrated in read_config_file
        # self.assertTrue(cfg.checkConfigParameters())

        # we get output_path variable
        self.assertEqual(cfg.getParam('chain', 'output_path'),
                         '../../../data/tmp/')

        # we check if bad section is detected
        self.assertRaises(Exception, cfg.getParam, 'BADchain', 'output_path')

        # we check if bad param is detected
        self.assertRaises(Exception, cfg.getParam, 'chain', 'BADoutput_path')

    def test_initConfigFileBad1(self):

        # the class is instantiated with self.fichierConfigBad1 config file
        # A mandatory variable is missing
        rcf.clearConfig()
        # cfg = rcf.read_config_file(self.fichierconfigbad1)
        # we check if the bad config file is detected
        self.assertRaises(ValueError, rcf.read_config_file,
                          self.fichierconfigbad1)

    def test_initConfigFileBad2(self):

        # the class is instantiated with self.fichierConfigBad2 config file
        # Bad type of variable
        rcf.clearConfig()
        # cfg = rcf.read_config_file(self.fichierConfigBad2)
        # we check if the bad config file is detected
        self.assertRaises(ValueError, rcf.read_config_file,
                          self.fichierconfigbad2)

    def test_initConfigFileBad3(self):

        # the class is instantiated with self.fichierConfigBad3 config file
        # Bad value in a variable
        rcf.clearConfig()
        # cfg = rcf.read_config_file(self.fichierConfigBad3)
        # we check if the bad config file is detected
        self.assertRaises(ValueError, rcf.read_config_file,
                          self.fichierconfigbad3)

    def test_config_builders_compatibility(self):
        """check builders compatibilty
        """

        # vecto before classification
        config_test_vecto_classif_order = os.path.join(
            self.test_working_directory, "i2_config_vecto_classif_order.cfg")
        shutil.copy(self.config_ref, config_test_vecto_classif_order)
        cfg_test = Config(open(config_test_vecto_classif_order))
        cfg_test.builders.builders_class_name = [
            'i2_vectorization', 'i2_classification'
        ]
        cfg_test.save(open(config_test_vecto_classif_order, 'w'))

        cfg = rcf.read_config_file(config_test_vecto_classif_order)
        self.assertRaises(ValueError, cfg.get_builders)

        # same builder twice
        config_test_vecto_vecto = os.path.join(self.test_working_directory,
                                               "i2_config_vecto_vecto.cfg")
        shutil.copy(self.config_ref, config_test_vecto_vecto)
        cfg_test = Config(open(config_test_vecto_vecto))
        cfg_test.builders.builders_class_name = [
            'i2_vectorization', 'i2_vectorization'
        ]
        cfg_test.save(open(config_test_vecto_vecto, 'w'))
        cfg = rcf.read_config_file(config_test_vecto_vecto)
        self.assertRaises(ValueError, cfg.get_builders)

        # features map and classification
        config_test_featuresmap_classif = os.path.join(
            self.test_working_directory, "i2_config_feat_classif.cfg")
        shutil.copy(self.config_ref, config_test_featuresmap_classif)
        cfg_test = Config(open(config_test_featuresmap_classif))
        cfg_test.builders.builders_class_name = [
            'i2_features_map', 'i2_classification'
        ]
        cfg_test.save(open(config_test_featuresmap_classif, 'w'))
        cfg = rcf.read_config_file(config_test_featuresmap_classif)
        self.assertRaises(ValueError, cfg.get_builders)

        # features map after vectorization
        config_test_vecto_feat = os.path.join(self.test_working_directory,
                                              "i2_config_vecto_feat.cfg")
        shutil.copy(self.config_ref, config_test_vecto_feat)
        cfg_test = Config(open(config_test_vecto_feat))
        cfg_test.builders.builders_class_name = [
            'i2_vectorization', 'i2_features_map'
        ]
        cfg_test.save(open(config_test_vecto_feat, 'w'))
        cfg = rcf.read_config_file(config_test_vecto_feat)
        self.assertRaises(ValueError, cfg.get_builders)
