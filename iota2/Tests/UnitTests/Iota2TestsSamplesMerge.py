#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
"""
Test sampler merge
"""
import os
import sys
import unittest
import shutil
from iota2.configuration_files import read_config_file as rcf

IOTA2DIR = os.environ.get('IOTA2DIR')
if not IOTA2DIR:
    raise Exception("IOTA2DIR environment variable must be set")
# if all tests pass, remove 'iota2_tests_directory' which contains all
# sub-directory tests
RM_IF_ALL_OK = True


class iota_testMergeSamples(unittest.TestCase):
    @classmethod
    def setUpClass(cls):

        # definition of local variables
        cls.group_test_name = "Iota2TestsMergeSamples"
        cls.iota2_tests_directory = os.path.join(IOTA2DIR, "data",
                                                 cls.group_test_name)
        cls.all_tests_ok = []
        # We initialize the expected mergeSamples for the function get_models()
        cls.expectedOutputGetModels = [('1', ['T31TCJ'], 0),
                                       ('1', ['T31TCJ'], 1)]
        # Tests directory
        cls.test_working_directory = None
        if os.path.exists(cls.iota2_tests_directory):
            shutil.rmtree(cls.iota2_tests_directory)
        os.mkdir(cls.iota2_tests_directory)

    # after launching all tests
    @classmethod
    def tearDownClass(cls):
        print("{} ended".format(cls.group_test_name))
        if RM_IF_ALL_OK and all(cls.all_tests_ok):
            shutil.rmtree(cls.iota2_tests_directory)

    # before launching a test
    def setUp(self):
        """
        create test environement (directories)
        """
        # self.test_working_directory is the diretory dedicated to each tests
        # it changes for each tests

        test_name = self.id().split(".")[-1]
        self.test_working_directory = os.path.join(self.iota2_tests_directory,
                                                   test_name)
        if os.path.exists(self.test_working_directory):
            shutil.rmtree(self.test_working_directory)
        os.mkdir(self.test_working_directory)

    def list2reason(self, exc_list):
        if exc_list and exc_list[-1][0] is self:
            return exc_list[-1][1]

    # after launching a test, remove test's data if test succeed
    def tearDown(self):
        if sys.version_info > (3, 4, 0):
            result = self.defaultTestResult()
            self._feedErrorsToResult(result, self._outcome.errors)
        else:
            result = getattr(self, "_outcomeForDoCleanups",
                             self._resultForDoCleanups)
        error = self.list2reason(result.errors)
        failure = self.list2reason(result.failures)
        ok = not error and not failure

        self.all_tests_ok.append(ok)
        if ok:
            shutil.rmtree(self.test_working_directory)

    def test_getModels(self):
        from iota2.Sampling import SamplesMerge

        # Copy and remove files in the test folder
        if os.path.exists(self.test_working_directory):
            shutil.rmtree(self.test_working_directory)
        shutil.copytree(
            os.path.join(IOTA2DIR, "data", "references", "mergeSamples",
                         "Input"), os.path.join(self.test_working_directory))
        os.mkdir(os.path.join(self.test_working_directory, 'samplesSelection'))
        # We define several parameters for the configuration file
        # cfg = rcf.read_config_file(
        #     os.path.join(self.test_working_directory, "config.cfg"))
        # cfg.setParam('chain', 'output_path', self.test_working_directory)
        # cfg.setParam('chain', 'region_field', 'region')
        # We execute the function : get_models()
        output = SamplesMerge.get_models(
            os.path.join(self.test_working_directory, "get_models",
                         'formattingVectors'), 'region', 2)

        # We check the output values with the expected values
        self.assertEqual(self.expectedOutputGetModels[0][0], output[0][0])
        self.assertEqual(self.expectedOutputGetModels[0][1][0],
                         output[0][1][0])
        self.assertEqual(self.expectedOutputGetModels[0][2], output[0][2])
        self.assertEqual(self.expectedOutputGetModels[1][0], output[1][0])
        self.assertEqual(self.expectedOutputGetModels[1][1][0],
                         output[1][1][0])
        self.assertEqual(self.expectedOutputGetModels[1][2], output[1][2])

    def test_samplesMerge(self):
        from iota2.Sampling import SamplesMerge

        # Copy and remove files in the test folder
        if os.path.exists(self.test_working_directory):
            shutil.rmtree(self.test_working_directory)
        ref_folder = os.path.join(IOTA2DIR, "data", "references",
                                  "mergeSamples", "Input")
        shutil.copytree(ref_folder, self.test_working_directory)
        sample_selection_path = os.path.join(self.test_working_directory,
                                             "samples_merge",
                                             "samplesSelection")
        if not os.path.exists(sample_selection_path):
            os.mkdir(sample_selection_path)

        # We define several parameters for the configuration file
        cfg = rcf.read_config_file(
            os.path.join(self.test_working_directory, 'config.cfg'))
        cfg.setParam(
            'chain', 'output_path',
            os.path.join(self.test_working_directory, "samples_merge"))
        cfg.setParam('chain', 'region_field', 'region')
        # We execute the function: samples_merge()

        output = SamplesMerge.get_models(
            os.path.join(self.test_working_directory, "get_models",
                         "formattingVectors"), 'region', 2)
        SamplesMerge.samples_merge(
            output[0], cfg.getParam('chain', 'output_path'),
            cfg.getParam('chain', 'region_field'),
            cfg.getParam('arg_train', 'runs'),
            cfg.getParam('arg_train', 'dempster_shafer_sar_opt_fusion'), None)

        # We check the produced files
        shape = os.path.join(IOTA2DIR, 'data', 'references', 'mergeSamples',
                             'Output', 'samples_region_1_seed_0.shp')
        proj = os.path.join(IOTA2DIR, 'data', 'references', 'mergeSamples',
                            'Output', 'samples_region_1_seed_0.prj')
        shapex = os.path.join(IOTA2DIR, 'data', 'references', 'mergeSamples',
                              'Output', 'samples_region_1_seed_0.shx')

        shape_t = os.path.join(sample_selection_path,
                               'samples_region_1_seed_0.shp')
        proj_t = os.path.join(sample_selection_path,
                              'samples_region_1_seed_0.prj')
        shapex_t = os.path.join(sample_selection_path,
                                'samples_region_1_seed_0.shx')
        print(shape, shape_t)
        self.assertEqual(0, os.system(f"diff {shape} {shape_t}"))
        self.assertEqual(0, os.system(f"diff {proj} {proj_t}"))
        self.assertEqual(0, os.system(f"diff {shapex} {shapex_t}"))


#
