#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================

# python -m unittest Iota2TestsLaunchTraining

import os
import sys
import shutil
import unittest
import filecmp
from iota2.VectorTools import vector_functions as vf
from iota2.configuration_files import read_config_file as rcf
IOTA2DIR = os.environ.get("IOTA2DIR")

if not IOTA2DIR:
    raise Exception("IOTA2DIR environment variable must be set")
# if all tests pass, remove 'iota2_tests_directory' which contains all
# sub-directory tests
RM_IF_ALL_OK = True


class Iota2TestLaunchTraining(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.group_test_name = "Iota2TestsLaunchTraining"
        cls.iota2_tests_directory = os.path.join(IOTA2DIR, "data",
                                                 cls.group_test_name)
        cls.all_tests_ok = []
        # definition of local variables
        cls.fichier_config = os.path.join(
            IOTA2DIR, "config", "Config_4Tuiles_Multi_FUS_Confidence.cfg")

        cls.pathTilesFeat = os.path.join(IOTA2DIR, "data", "references",
                                         "features")
        cls.ref_data = os.path.join(IOTA2DIR, "data", "references",
                                    "LaunchTraining")
        # Tests directory
        cls.test_working_directory = None
        if os.path.exists(cls.iota2_tests_directory):
            shutil.rmtree(cls.iota2_tests_directory)
        os.mkdir(cls.iota2_tests_directory)

    @classmethod
    def tearDownClass(cls):
        print("{} ended".format(cls.group_test_name))
        if RM_IF_ALL_OK and all(cls.all_tests_ok):
            shutil.rmtree(cls.iota2_tests_directory)

    # before launching a test
    def setUp(self):
        """
        create test environement (directories)
        """
        # self.test_working_directory is the diretory dedicated to each tests
        # it changes for each tests

        test_name = self.id().split(".")[-1]
        self.test_working_directory = os.path.join(self.iota2_tests_directory,
                                                   test_name)
        if os.path.exists(self.test_working_directory):
            shutil.rmtree(self.test_working_directory)
        os.mkdir(self.test_working_directory)

    def list2reason(self, exc_list):
        if exc_list and exc_list[-1][0] is self:
            return exc_list[-1][1]

    # after launching a test, remove test's data if test succeed
    def tearDown(self):
        if sys.version_info > (3, 4, 0):
            result = self.defaultTestResult()
            self._feedErrorsToResult(result, self._outcome.errors)
        else:
            result = getattr(self, "_outcomeForDoCleanups",
                             self._resultForDoCleanups)
        error = self.list2reason(result.errors)
        failure = self.list2reason(result.failures)
        ok = not error and not failure

        self.all_tests_ok.append(ok)
        if ok:
            shutil.rmtree(self.test_working_directory)

    def test_LaunchTraining(self):
        from iota2.Learning import TrainingCmd as TC

        # Prepare data for testing
        pathappval = os.path.join(self.test_working_directory, "dataAppVal")
        pathstats = os.path.join(self.test_working_directory, "stats")
        cmdpath = os.path.join(self.test_working_directory, "cmd")
        pathmodels = os.path.join(self.test_working_directory, "model")
        pathconfigmodels = os.path.join(self.test_working_directory,
                                        "config_model")
        pathlearningsamples = os.path.join(self.test_working_directory,
                                           "learningSamples")
        pathformattingsamples = os.path.join(self.test_working_directory,
                                             "formattingVectors")
        vector_formatting = os.path.join(self.ref_data, "Input",
                                         "D0005H0002.shp")

        # test and creation of pathappval
        if not os.path.exists(pathappval):
            os.mkdir(pathappval)
        # test and creation of pathstats
        if not os.path.exists(pathstats):
            os.mkdir(pathstats)
        # test and creation of cmdpath
        if not os.path.exists(cmdpath):
            os.mkdir(cmdpath)
        # test and creation of cmdpath
        if not os.path.exists(cmdpath + "/train"):
            os.mkdir(cmdpath + "/train")
        # test and creation of pathmodels
        if not os.path.exists(pathmodels):
            os.mkdir(pathmodels)
        # test and creation of pathconfigmodels
        if not os.path.exists(pathconfigmodels):
            os.mkdir(pathconfigmodels)
        # test and creation of pathlearningsamples
        if not os.path.exists(pathlearningsamples):
            os.mkdir(pathlearningsamples)
        if not os.path.exists(pathformattingsamples):
            os.mkdir(pathformattingsamples)
        # copy input data
        vf.cpShapeFile(vector_formatting.replace(".shp", ""),
                       pathformattingsamples, [".prj", ".shp", ".dbf", ".shx"],
                       spe=True)

        src_files = os.listdir(self.ref_data + "/Input/learningSamples")
        for file_name in src_files:
            full_file_name = os.path.join(
                self.ref_data + "/Input/learningSamples", file_name)
            shutil.copy(full_file_name, pathlearningsamples)

        # Test function
        rcf.clearConfig()
        cfg = rcf.read_config_file(self.fichier_config)
        cfg.setParam('chain', 'output_path', self.test_working_directory)
        cfg.setParam('chain', 'region_field', "region")

        TC.launch_training(
            classifier_name=cfg.getParam("arg_train", "classifier"),
            classifier_options=cfg.getParam("arg_train", "options"),
            output_path=self.test_working_directory,
            ground_truth=cfg.getParam("chain", "ground_truth"),
            data_field="CODE",
            region_field="region",
            path_to_cmd_train=os.path.join(cmdpath, "train"),
            out=pathmodels)

        # file comparison to ref file
        file1 = cmdpath + "/train/train.txt"
        self.assertTrue(os.path.getsize(file1) > 0)
        file2 = pathconfigmodels + "/configModel.cfg"
        referencefile2 = self.ref_data + "/Output/configModel.cfg"

        self.assertTrue(filecmp.cmp(file2, referencefile2))
