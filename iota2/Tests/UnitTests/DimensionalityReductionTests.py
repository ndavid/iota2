#!/usr/bin/env python3
#-*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
import os
import sys
import shutil
import logging
import filecmp
import unittest
from iota2.Common.FileUtils import ensure_dir
from iota2.Sampling import DimensionalityReduction as DR
from iota2.configuration_files import read_config_file as rcf
from iota2.Tests.UnitTests.tests_utils import tests_utils_rasters as TUR
from iota2.Tests.UnitTests.tests_utils import tests_utils_vectors as TUV

IOTA2DIR = os.environ.get('IOTA2DIR')
if not IOTA2DIR:
    raise Exception("IOTA2DIR environment variable must be set")

# if all tests pass, remove 'iota2_tests_directory' which contains all
# sub-directory tests
RM_IF_ALL_OK = True

LOGGER = logging.getLogger("distributed.worker")

# IOTA2_DATATEST = IOTA2DIR + "/data/"


class DimensionalityReductionTests(unittest.TestCase):
    """Class dedicated to launch several sampler tests"""
    @classmethod
    def setUpClass(cls):
        cls.group_test_name = "iota_tests_dim_red"
        cls.all_tests_ok = []
        cls.input_sample_file_name = os.path.join(IOTA2DIR, "data",
                                                  "dim_red_samples.sqlite")
        cls.iota2_tests_directory = os.path.join(IOTA2DIR, "data",
                                                 cls.group_test_name)
        cls.target_dimension = 6
        cls.fl_date = [
            'landsat8_b1_20140118', 'landsat8_b2_20140118',
            'landsat8_b3_20140118', 'landsat8_b4_20140118',
            'landsat8_b5_20140118', 'landsat8_b6_20140118',
            'landsat8_b7_20140118', 'landsat8_ndvi_20140118',
            'landsat8_ndwi_20140118', 'landsat8_brightness_20140118'
        ]
        cls.stats_file = os.path.join(IOTA2DIR, "data", 'dim_red_stats.xml')

        cls.output_model_file_name = os.path.join(IOTA2DIR, "data",
                                                  'model.pca')
        cls.test_output_model_file_name = os.path.join(IOTA2DIR, "data", 'tmp',
                                                       'model.pca')
        cls.reduced_output_file_name = os.path.join(IOTA2DIR, "data",
                                                    'reduced.sqlite')

        cls.joint_reduced_file = os.path.join(IOTA2DIR, "data", 'joint.sqlite')
        cls.output_sample_file_name = 'reduced_output_samples.sqlite'

        cls.config_file = os.path.join(
            IOTA2DIR, "config", "Config_4Tuiles_Multi_FUS_Confidence.cfg")
        cls.cfg = rcf.read_config_file(cls.config_file)
        cls.cfg.cfg.chain.output_path = os.path.join(IOTA2DIR, "data", "tmp")

        cls.field_list = [
            'id', 'lc', 'code', 'area_ha', 'originfid', 'landsat8_b1_20140118',
            'landsat8_b2_20140118', 'landsat8_b3_20140118',
            'landsat8_b4_20140118', 'landsat8_b5_20140118',
            'landsat8_b6_20140118', 'landsat8_b7_20140118',
            'landsat8_b1_20140203', 'landsat8_b2_20140203',
            'landsat8_b3_20140203', 'landsat8_b4_20140203',
            'landsat8_b5_20140203', 'landsat8_b6_20140203',
            'landsat8_b7_20140203', 'landsat8_b1_20140219',
            'landsat8_b2_20140219', 'landsat8_b3_20140219',
            'landsat8_b4_20140219', 'landsat8_b5_20140219',
            'landsat8_b6_20140219', 'landsat8_b7_20140219',
            'landsat8_b1_20140307', 'landsat8_b2_20140307',
            'landsat8_b3_20140307', 'landsat8_b4_20140307',
            'landsat8_b5_20140307', 'landsat8_b6_20140307',
            'landsat8_b7_20140307', 'landsat8_b1_20140323',
            'landsat8_b2_20140323', 'landsat8_b3_20140323',
            'landsat8_b4_20140323', 'landsat8_b5_20140323',
            'landsat8_b6_20140323', 'landsat8_b7_20140323',
            'landsat8_b1_20140408', 'landsat8_b2_20140408',
            'landsat8_b3_20140408', 'landsat8_b4_20140408',
            'landsat8_b5_20140408', 'landsat8_b6_20140408',
            'landsat8_b7_20140408', 'landsat8_b1_20140424',
            'landsat8_b2_20140424', 'landsat8_b3_20140424',
            'landsat8_b4_20140424', 'landsat8_b5_20140424',
            'landsat8_b6_20140424', 'landsat8_b7_20140424',
            'landsat8_b1_20140510', 'landsat8_b2_20140510',
            'landsat8_b3_20140510', 'landsat8_b4_20140510',
            'landsat8_b5_20140510', 'landsat8_b6_20140510',
            'landsat8_b7_20140510', 'landsat8_b1_20140526',
            'landsat8_b2_20140526', 'landsat8_b3_20140526',
            'landsat8_b4_20140526', 'landsat8_b5_20140526',
            'landsat8_b6_20140526', 'landsat8_b7_20140526',
            'landsat8_b1_20140611', 'landsat8_b2_20140611',
            'landsat8_b3_20140611', 'landsat8_b4_20140611',
            'landsat8_b5_20140611', 'landsat8_b6_20140611',
            'landsat8_b7_20140611', 'landsat8_b1_20140627',
            'landsat8_b2_20140627', 'landsat8_b3_20140627',
            'landsat8_b4_20140627', 'landsat8_b5_20140627',
            'landsat8_b6_20140627', 'landsat8_b7_20140627',
            'landsat8_b1_20140713', 'landsat8_b2_20140713',
            'landsat8_b3_20140713', 'landsat8_b4_20140713',
            'landsat8_b5_20140713', 'landsat8_b6_20140713',
            'landsat8_b7_20140713', 'landsat8_b1_20140729',
            'landsat8_b2_20140729', 'landsat8_b3_20140729',
            'landsat8_b4_20140729', 'landsat8_b5_20140729',
            'landsat8_b6_20140729', 'landsat8_b7_20140729',
            'landsat8_b1_20140814', 'landsat8_b2_20140814',
            'landsat8_b3_20140814', 'landsat8_b4_20140814',
            'landsat8_b5_20140814', 'landsat8_b6_20140814',
            'landsat8_b7_20140814', 'landsat8_b1_20140830',
            'landsat8_b2_20140830', 'landsat8_b3_20140830',
            'landsat8_b4_20140830', 'landsat8_b5_20140830',
            'landsat8_b6_20140830', 'landsat8_b7_20140830',
            'landsat8_b1_20140915', 'landsat8_b2_20140915',
            'landsat8_b3_20140915', 'landsat8_b4_20140915',
            'landsat8_b5_20140915', 'landsat8_b6_20140915',
            'landsat8_b7_20140915', 'landsat8_b1_20141001',
            'landsat8_b2_20141001', 'landsat8_b3_20141001',
            'landsat8_b4_20141001', 'landsat8_b5_20141001',
            'landsat8_b6_20141001', 'landsat8_b7_20141001',
            'landsat8_b1_20141017', 'landsat8_b2_20141017',
            'landsat8_b3_20141017', 'landsat8_b4_20141017',
            'landsat8_b5_20141017', 'landsat8_b6_20141017',
            'landsat8_b7_20141017', 'landsat8_b1_20141102',
            'landsat8_b2_20141102', 'landsat8_b3_20141102',
            'landsat8_b4_20141102', 'landsat8_b5_20141102',
            'landsat8_b6_20141102', 'landsat8_b7_20141102',
            'landsat8_b1_20141118', 'landsat8_b2_20141118',
            'landsat8_b3_20141118', 'landsat8_b4_20141118',
            'landsat8_b5_20141118', 'landsat8_b6_20141118',
            'landsat8_b7_20141118', 'landsat8_b1_20141204',
            'landsat8_b2_20141204', 'landsat8_b3_20141204',
            'landsat8_b4_20141204', 'landsat8_b5_20141204',
            'landsat8_b6_20141204', 'landsat8_b7_20141204',
            'landsat8_b1_20141220', 'landsat8_b2_20141220',
            'landsat8_b3_20141220', 'landsat8_b4_20141220',
            'landsat8_b5_20141220', 'landsat8_b6_20141220',
            'landsat8_b7_20141220', 'landsat8_b1_20141229',
            'landsat8_b2_20141229', 'landsat8_b3_20141229',
            'landsat8_b4_20141229', 'landsat8_b5_20141229',
            'landsat8_b6_20141229', 'landsat8_b7_20141229',
            'landsat8_ndvi_20140118', 'landsat8_ndvi_20140203',
            'landsat8_ndvi_20140219', 'landsat8_ndvi_20140307',
            'landsat8_ndvi_20140323', 'landsat8_ndvi_20140408',
            'landsat8_ndvi_20140424', 'landsat8_ndvi_20140510',
            'landsat8_ndvi_20140526', 'landsat8_ndvi_20140611',
            'landsat8_ndvi_20140627', 'landsat8_ndvi_20140713',
            'landsat8_ndvi_20140729', 'landsat8_ndvi_20140814',
            'landsat8_ndvi_20140830', 'landsat8_ndvi_20140915',
            'landsat8_ndvi_20141001', 'landsat8_ndvi_20141017',
            'landsat8_ndvi_20141102', 'landsat8_ndvi_20141118',
            'landsat8_ndvi_20141204', 'landsat8_ndvi_20141220',
            'landsat8_ndvi_20141229', 'landsat8_ndwi_20140118',
            'landsat8_ndwi_20140203', 'landsat8_ndwi_20140219',
            'landsat8_ndwi_20140307', 'landsat8_ndwi_20140323',
            'landsat8_ndwi_20140408', 'landsat8_ndwi_20140424',
            'landsat8_ndwi_20140510', 'landsat8_ndwi_20140526',
            'landsat8_ndwi_20140611', 'landsat8_ndwi_20140627',
            'landsat8_ndwi_20140713', 'landsat8_ndwi_20140729',
            'landsat8_ndwi_20140814', 'landsat8_ndwi_20140830',
            'landsat8_ndwi_20140915', 'landsat8_ndwi_20141001',
            'landsat8_ndwi_20141017', 'landsat8_ndwi_20141102',
            'landsat8_ndwi_20141118', 'landsat8_ndwi_20141204',
            'landsat8_ndwi_20141220', 'landsat8_ndwi_20141229',
            'landsat8_brightness_20140118', 'landsat8_brightness_20140203',
            'landsat8_brightness_20140219', 'landsat8_brightness_20140307',
            'landsat8_brightness_20140323', 'landsat8_brightness_20140408',
            'landsat8_brightness_20140424', 'landsat8_brightness_20140510',
            'landsat8_brightness_20140526', 'landsat8_brightness_20140611',
            'landsat8_brightness_20140627', 'landsat8_brightness_20140713',
            'landsat8_brightness_20140729', 'landsat8_brightness_20140814',
            'landsat8_brightness_20140830', 'landsat8_brightness_20140915',
            'landsat8_brightness_20141001', 'landsat8_brightness_20141017',
            'landsat8_brightness_20141102', 'landsat8_brightness_20141118',
            'landsat8_brightness_20141204', 'landsat8_brightness_20141220',
            'landsat8_brightness_20141229'
        ]

        # Tests directory
        cls.test_working_directory = None
        if os.path.exists(cls.iota2_tests_directory):
            shutil.rmtree(cls.iota2_tests_directory, ignore_errors=True)
        os.mkdir(cls.iota2_tests_directory)

    @classmethod
    def tearDownClass(cls):
        """after launching all tests"""
        print("{} ended".format(cls.group_test_name))
        if RM_IF_ALL_OK and all(cls.all_tests_ok):
            shutil.rmtree(cls.iota2_tests_directory)

    # before launching a test
    def setUp(self):
        """
        create test environement (directories)
        """
        # self.test_working_directory is the diretory dedicated to each tests
        # it changes for each tests

        test_name = self.id().split(".")[-1]
        self.test_working_directory = os.path.join(self.iota2_tests_directory,
                                                   test_name)
        if os.path.exists(self.test_working_directory):
            shutil.rmtree(self.test_working_directory)
        os.mkdir(self.test_working_directory)

    def list2reason(self, exc_list):
        """list2reason"""
        out = None
        if exc_list and exc_list[-1][0] is self:
            out = exc_list[-1][1]
        return out

    def tearDown(self):
        """after launching a test, remove test's data if test succeed"""
        result = self.defaultTestResult()
        self._feedErrorsToResult(result, self._outcome.errors)
        error = self.list2reason(result.errors)
        failure = self.list2reason(result.failures)
        ok_test = not error and not failure

        self.all_tests_ok.append(ok_test)
        if ok_test:
            shutil.rmtree(self.test_working_directory, ignore_errors=True)

    def test_get_available_features(self):

        expected = '20140118'
        (feats, _) = DR.get_available_features(self.input_sample_file_name)
        self.assertEqual(feats['landsat8']['brightness'][0], expected)

        expected = 'b1'
        (feats, _) = DR.get_available_features(self.input_sample_file_name,
                                               'date', 'sensor')
        self.assertEqual(feats['20141017']['landsat8'][0], expected)

        expected = 'landsat8'
        (feats, _) = DR.get_available_features(self.input_sample_file_name,
                                               'date', 'band')
        self.assertEqual(feats['20141118']['b2'][0], expected)

    def test_generate_feature_list_global(self):
        expected = [[
            'landsat8_b1_20140118', 'landsat8_b2_20140118',
            'landsat8_b3_20140118', 'landsat8_b4_20140118',
            'landsat8_b5_20140118', 'landsat8_b6_20140118',
            'landsat8_b7_20140118', 'landsat8_b1_20140203',
            'landsat8_b2_20140203', 'landsat8_b3_20140203',
            'landsat8_b4_20140203', 'landsat8_b5_20140203',
            'landsat8_b6_20140203', 'landsat8_b7_20140203',
            'landsat8_b1_20140219', 'landsat8_b2_20140219',
            'landsat8_b3_20140219', 'landsat8_b4_20140219',
            'landsat8_b5_20140219', 'landsat8_b6_20140219',
            'landsat8_b7_20140219', 'landsat8_b1_20140307',
            'landsat8_b2_20140307', 'landsat8_b3_20140307',
            'landsat8_b4_20140307', 'landsat8_b5_20140307',
            'landsat8_b6_20140307', 'landsat8_b7_20140307',
            'landsat8_b1_20140323', 'landsat8_b2_20140323',
            'landsat8_b3_20140323', 'landsat8_b4_20140323',
            'landsat8_b5_20140323', 'landsat8_b6_20140323',
            'landsat8_b7_20140323'
        ]]

        (feat_list, _) = DR.build_features_lists(self.input_sample_file_name,
                                                 'global')
        self.assertEqual(expected[0], feat_list[0][:len(expected[0])])

    def test_generate_feature_list_date(self):
        (feat_list, _) = DR.build_features_lists(self.input_sample_file_name,
                                                 'date')
        self.assertEqual(self.fl_date, feat_list[0])

    def test_generate_feature_list_band(self):
        # second spectral band
        expected = [
            'landsat8_b2_20140118', 'landsat8_b2_20140203',
            'landsat8_b2_20140219', 'landsat8_b2_20140307',
            'landsat8_b2_20140323', 'landsat8_b2_20140408',
            'landsat8_b2_20140424', 'landsat8_b2_20140510',
            'landsat8_b2_20140526', 'landsat8_b2_20140611',
            'landsat8_b2_20140627', 'landsat8_b2_20140713',
            'landsat8_b2_20140729', 'landsat8_b2_20140814',
            'landsat8_b2_20140830', 'landsat8_b2_20140915',
            'landsat8_b2_20141001', 'landsat8_b2_20141017',
            'landsat8_b2_20141102', 'landsat8_b2_20141118',
            'landsat8_b2_20141204', 'landsat8_b2_20141220',
            'landsat8_b2_20141229'
        ]
        (feat_list, _) = DR.build_features_lists(self.input_sample_file_name,
                                                 'band')
        self.assertEqual(expected, feat_list[1])

    def test_compute_feature_statistics(self):
        """test compute feature statistics"""

        test_stats_file = os.path.join(self.test_working_directory,
                                       'stats.xml')
        DR.compute_feature_statistics(self.input_sample_file_name,
                                      test_stats_file, self.fl_date)
        self.assertTrue(filecmp.cmp(test_stats_file,
                                    self.stats_file,
                                    shallow=False),
                        msg="Stats files don't match")

    def test_train_dimensionality_reduction(self):
        """test train dimensionality reduction"""
        DR.train_dimensionality_reduction(self.input_sample_file_name,
                                          self.test_output_model_file_name,
                                          self.fl_date, self.target_dimension,
                                          self.stats_file)
        self.assertTrue(filecmp.cmp(self.test_output_model_file_name,
                                    self.output_model_file_name,
                                    shallow=False),
                        msg="Model files don't match")

    def test_apply_dimensionality_reduction(self):
        """test apply dimensionality reduction"""
        from iota2.Tests.UnitTests.tests_utils import tests_utils_vectors as VU

        output_features = ['reduced_' + str(x) for x in range(6)]
        test_reduced_output_file_name = os.path.join(
            self.test_working_directory, "reduced.sqlite")
        DR.apply_dimensionality_reduction(self.input_sample_file_name,
                                          test_reduced_output_file_name,
                                          self.output_model_file_name,
                                          self.fl_date,
                                          output_features,
                                          stats_file=self.stats_file,
                                          writing_mode='overwrite')
        self.assertTrue(VU.compare_sqlite(test_reduced_output_file_name,
                                          self.reduced_output_file_name,
                                          cmp_mode="coordinates"),
                        msg="Joined files don't match")

    def test_join_reduced_sample_files(self):
        """test join reduced sample files"""
        from iota2.Tests.UnitTests.tests_utils import tests_utils_vectors as VU

        feat_list = [
            self.reduced_output_file_name, self.reduced_output_file_name
        ]
        output_features = [f'reduced_{x + 1}' for x in range(5)]
        test_joint_reduced_file = os.path.join(self.test_working_directory,
                                               'joint.sqlite')
        DR.join_reduced_sample_files(feat_list, test_joint_reduced_file,
                                     output_features)
        self.assertTrue(VU.compare_sqlite(test_joint_reduced_file,
                                          self.joint_reduced_file,
                                          cmp_mode="coordinates"),
                        msg="Joined files don't match")

    def test_sample_file_pca_reduction(self):
        """test sample file PCA reduction"""
        from iota2.Tests.UnitTests.tests_utils import tests_utils_vectors as VU
        test_test_output_sample_file_name = os.path.join(
            IOTA2DIR, "data", "reduced_output_samples_test.sqlite")
        DR.sample_file_pca_reduction(self.input_sample_file_name,
                                     test_test_output_sample_file_name,
                                     'date',
                                     self.target_dimension,
                                     tmp_dir=os.path.join(
                                         IOTA2DIR, "data", "tmp"))

        self.assertTrue(VU.compare_sqlite(test_test_output_sample_file_name,
                                          os.path.join(
                                              IOTA2DIR, "data",
                                              self.output_sample_file_name),
                                          cmp_mode="coordinates"),
                        msg="Output sample files don't match")

    def test_build_channel_groups(self):
        """test build channel groups"""
        fake_db_dir = os.path.join(IOTA2DIR, "data", "tmp", "dimRed",
                                   "before_reduction")
        ensure_dir(fake_db_dir)
        fake_db_file = os.path.join(fake_db_dir, "dim_red_samples.sqlite")
        TUV.random_features_database_generator(fake_db_file, self.field_list,
                                               20)
        chan_group = DR.build_channel_groups(
            "sensor_date", self.cfg.getParam("chain", "output_path"))
        expected = [[
            'Channel1', 'Channel2', 'Channel3', 'Channel4', 'Channel5',
            'Channel6', 'Channel7', 'Channel162', 'Channel185', 'Channel208'
        ],
                    [
                        'Channel8', 'Channel9', 'Channel10', 'Channel11',
                        'Channel12', 'Channel13', 'Channel14', 'Channel163',
                        'Channel186', 'Channel209'
                    ],
                    [
                        'Channel15', 'Channel16', 'Channel17', 'Channel18',
                        'Channel19', 'Channel20', 'Channel21', 'Channel164',
                        'Channel187', 'Channel210'
                    ],
                    [
                        'Channel22', 'Channel23', 'Channel24', 'Channel25',
                        'Channel26', 'Channel27', 'Channel28', 'Channel165',
                        'Channel188', 'Channel211'
                    ],
                    [
                        'Channel29', 'Channel30', 'Channel31', 'Channel32',
                        'Channel33', 'Channel34', 'Channel35', 'Channel166',
                        'Channel189', 'Channel212'
                    ],
                    [
                        'Channel36', 'Channel37', 'Channel38', 'Channel39',
                        'Channel40', 'Channel41', 'Channel42', 'Channel167',
                        'Channel190', 'Channel213'
                    ],
                    [
                        'Channel43', 'Channel44', 'Channel45', 'Channel46',
                        'Channel47', 'Channel48', 'Channel49', 'Channel168',
                        'Channel191', 'Channel214'
                    ],
                    [
                        'Channel50', 'Channel51', 'Channel52', 'Channel53',
                        'Channel54', 'Channel55', 'Channel56', 'Channel169',
                        'Channel192', 'Channel215'
                    ],
                    [
                        'Channel57', 'Channel58', 'Channel59', 'Channel60',
                        'Channel61', 'Channel62', 'Channel63', 'Channel170',
                        'Channel193', 'Channel216'
                    ],
                    [
                        'Channel64', 'Channel65', 'Channel66', 'Channel67',
                        'Channel68', 'Channel69', 'Channel70', 'Channel171',
                        'Channel194', 'Channel217'
                    ],
                    [
                        'Channel71', 'Channel72', 'Channel73', 'Channel74',
                        'Channel75', 'Channel76', 'Channel77', 'Channel172',
                        'Channel195', 'Channel218'
                    ],
                    [
                        'Channel78', 'Channel79', 'Channel80', 'Channel81',
                        'Channel82', 'Channel83', 'Channel84', 'Channel173',
                        'Channel196', 'Channel219'
                    ],
                    [
                        'Channel85', 'Channel86', 'Channel87', 'Channel88',
                        'Channel89', 'Channel90', 'Channel91', 'Channel174',
                        'Channel197', 'Channel220'
                    ],
                    [
                        'Channel92', 'Channel93', 'Channel94', 'Channel95',
                        'Channel96', 'Channel97', 'Channel98', 'Channel175',
                        'Channel198', 'Channel221'
                    ],
                    [
                        'Channel99', 'Channel100', 'Channel101', 'Channel102',
                        'Channel103', 'Channel104', 'Channel105', 'Channel176',
                        'Channel199', 'Channel222'
                    ],
                    [
                        'Channel106', 'Channel107', 'Channel108', 'Channel109',
                        'Channel110', 'Channel111', 'Channel112', 'Channel177',
                        'Channel200', 'Channel223'
                    ],
                    [
                        'Channel113', 'Channel114', 'Channel115', 'Channel116',
                        'Channel117', 'Channel118', 'Channel119', 'Channel178',
                        'Channel201', 'Channel224'
                    ],
                    [
                        'Channel120', 'Channel121', 'Channel122', 'Channel123',
                        'Channel124', 'Channel125', 'Channel126', 'Channel179',
                        'Channel202', 'Channel225'
                    ],
                    [
                        'Channel127', 'Channel128', 'Channel129', 'Channel130',
                        'Channel131', 'Channel132', 'Channel133', 'Channel180',
                        'Channel203', 'Channel226'
                    ],
                    [
                        'Channel134', 'Channel135', 'Channel136', 'Channel137',
                        'Channel138', 'Channel139', 'Channel140', 'Channel181',
                        'Channel204', 'Channel227'
                    ],
                    [
                        'Channel141', 'Channel142', 'Channel143', 'Channel144',
                        'Channel145', 'Channel146', 'Channel147', 'Channel182',
                        'Channel205', 'Channel228'
                    ],
                    [
                        'Channel148', 'Channel149', 'Channel150', 'Channel151',
                        'Channel152', 'Channel153', 'Channel154', 'Channel183',
                        'Channel206', 'Channel229'
                    ],
                    [
                        'Channel155', 'Channel156', 'Channel157', 'Channel158',
                        'Channel159', 'Channel160', 'Channel161', 'Channel184',
                        'Channel207', 'Channel230'
                    ]]
        self.assertEqual(expected, chan_group)

    def test_apply_dimensionality_reduction_to_feature_stack(self):
        """test Apply Dimensionality Reduction To Feature Stack"""

        # prepare inputs
        number_of_bands = 230
        image_stack = os.path.join(IOTA2DIR, "data", "230feats_test.tif")
        arr_pattern = TUR.fun_array("iota2_binary")
        arr_to_reduce = [
            arr_pattern * cpt for cpt in range(1, number_of_bands + 1)
        ]
        TUR.array_to_raster(arr_to_reduce, image_stack)
        fake_db_dir = os.path.join(IOTA2DIR, "data", "tmp", "dimRed",
                                   "before_reduction")
        ensure_dir(fake_db_dir)
        fake_db_file = os.path.join(fake_db_dir, "dim_red_samples.sqlite")
        TUV.random_features_database_generator(fake_db_file, self.field_list,
                                               20)
        # check if functions are launchable, without exceptions
        model_list = [self.output_model_file_name] * len(
            DR.build_channel_groups("sensor_date",
                                    self.cfg.getParam("chain", "output_path")))
        (app, _) = DR.apply_dimensionality_reduction_to_feature_stack(
            "sensor_date", self.cfg.getParam("chain", "output_path"),
            image_stack, model_list)
        app.SetParameterString(
            "out", os.path.join(IOTA2DIR, "data", "tmp", "reducedStack.tif"))
        app.ExecuteAndWriteOutput()

    def test_apply_dimensionality_reduction_to_feature_stack_pipeline(self):
        """"""

        # prepare inputs
        import otbApplication as otb
        number_of_bands = 230
        image_stack = os.path.join(IOTA2DIR, "data", "230feats_test.tif")
        arr_pattern = TUR.fun_array("iota2_binary")
        arr_to_reduce = [
            arr_pattern * cpt for cpt in range(1, number_of_bands + 1)
        ]
        TUR.array_to_raster(arr_to_reduce, image_stack)
        fake_db_dir = os.path.join(IOTA2DIR, "data", "tmp", "dimRed",
                                   "before_reduction")
        ensure_dir(fake_db_dir)
        fake_db_file = os.path.join(fake_db_dir, "dim_red_samples.sqlite")
        TUV.random_features_database_generator(fake_db_file, self.field_list,
                                               20)
        # check if functions are launchable, without exceptions
        app = otb.Registry.CreateApplication("ExtractROI")
        app.SetParameterString("in", image_stack)
        app.Execute()
        model_list = [self.output_model_file_name] * len(
            DR.build_channel_groups("sensor_date",
                                    self.cfg.getParam("chain", "output_path")))
        (appdr, _) = DR.apply_dimensionality_reduction_to_feature_stack(
            "sensor_date", self.cfg.getParam("chain", "output_path"), app,
            model_list)
        appdr.SetParameterString(
            "out",
            os.path.join(IOTA2DIR, "data", "tmp", "reducedStackPipeline.tif"))
        appdr.ExecuteAndWriteOutput()


if __name__ == '__main__':
    unittest.main()
