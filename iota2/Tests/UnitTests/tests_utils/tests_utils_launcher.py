#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
import os
from subprocess import Popen, PIPE, CalledProcessError


def run_cmd(cmd: str) -> int:
    """run cmd in a subprocess and raise and Exception if the command fails
    """
    # p = subprocess.Popen(cmd,
    #                      env=os.environ,
    #                      shell=True,
    #                      stdout=subprocess.PIPE,
    #                      stderr=subprocess.PIPE)
    # # Get output as strings
    # out, err = p.communicate()
    # if p.returncode != 0:
    #     raise Exception(err.decode("UTF-8"))

    with Popen(cmd,
               stdout=PIPE,
               stderr=PIPE,
               bufsize=1,
               universal_newlines=True,
               env=os.environ,
               shell=True) as p:
        for line in p.stdout:
            print(line, end='')  # process line here
        err = ""
        for line in p.stderr:
            err += line

    if p.returncode != 0:
        raise Exception(err)
