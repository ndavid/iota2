#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
import os
from typing import Optional
from collections import OrderedDict
from iota2.configuration_files import read_config_file as rcf
from iota2.sequence_builders.i2_sequence_builder import i2_builder
# from iota2.configuration_files import default_config_parameters as dcp
# from iota2.configuration_files import check_config_parameters as ccp
# from iota2.Common import ServiceError as sErr


class i2_wrong_builder(i2_builder):
    """
    class use to describe steps sequence and variable to use at
    each step (config)
    """
    def __init__(self,
                 cfg,
                 config_ressources,
                 hpc_working_directory: Optional[str] = "TMPDIR"):
        super().__init__(cfg, config_ressources, hpc_working_directory)
        # steps definitions
        self.steps_group = OrderedDict()
        self.steps_group["init"] = OrderedDict()
        self.steps_group["writting"] = OrderedDict()

        # build steps
        self.steps = self.build_steps(self.cfg, config_ressources)
        self.sort_step()

        # pickle's path
        self.iota2_pickle = os.path.join(
            rcf.read_config_file(self.cfg).getParam("chain", "output_path"),
            "logs", "iota2.txt")

    def get_dir(self):
        """
        usage : return iota2_directories
        """
        directories = ['final', "features", "customF"]

        iota2_outputs_dir = rcf.read_config_file(self.cfg).getParam(
            'chain', 'output_path')

        return [os.path.join(iota2_outputs_dir, d) for d in directories]
