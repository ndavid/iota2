#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================

# python -m unittest Iota2TestsServiceCompareVectorFile

import os
import sys
import shutil
import unittest
import iota2.Tests.UnitTests.tests_utils.tests_utils_vectors as TUV
IOTA2DIR = os.environ.get("IOTA2DIR")

if not IOTA2DIR:
    raise Exception("IOTA2DIR environment variable must be set")
# if all tests pass, remove 'iota2_tests_directory' which contains all
# sub-directory tests
RM_IF_ALL_OK = True


class Iota2testServiceCompareVectorFile(unittest.TestCase):
    """
    Test class service_compare_vector_file
    """
    @classmethod
    def setUpClass(cls):

        cls.group_test_name = "Iota2TestsServiceCompareVectorFile"
        cls.iota2_tests_directory = os.path.join(IOTA2DIR, "data",
                                                 cls.group_test_name)
        cls.all_tests_ok = []
        # definition of local variables
        cls.ref_data = os.path.join(IOTA2DIR, "data", "references",
                                    "ServiceCompareVectorFile")
        # Tests directory
        cls.test_working_directory = None
        if os.path.exists(cls.iota2_tests_directory):
            shutil.rmtree(cls.iota2_tests_directory)
        os.mkdir(cls.iota2_tests_directory)

    @classmethod
    def tearDownClass(cls):
        print("{} ended".format(cls.group_test_name))
        if RM_IF_ALL_OK and all(cls.all_tests_ok):
            shutil.rmtree(cls.iota2_tests_directory)

    # before launching a test
    def setUp(self):
        """
        create test environement (directories)
        """
        # self.test_working_directory is the diretory dedicated to each tests
        # it changes for each tests

        test_name = self.id().split(".")[-1]
        self.test_working_directory = os.path.join(self.iota2_tests_directory,
                                                   test_name)
        if os.path.exists(self.test_working_directory):
            shutil.rmtree(self.test_working_directory)
        os.mkdir(self.test_working_directory)

    def list2reason(self, exc_list):
        if exc_list and exc_list[-1][0] is self:
            return exc_list[-1][1]

    # after launching a test, remove test's data if test succeed
    def tearDown(self):
        if sys.version_info > (3, 4, 0):
            result = self.defaultTestResult()
            self._feedErrorsToResult(result, self._outcome.errors)
        else:
            result = getattr(self, "_outcomeForDoCleanups",
                             self._resultForDoCleanups)
        error = self.list2reason(result.errors)
        failure = self.list2reason(result.failures)
        ok = not error and not failure

        self.all_tests_ok.append(ok)
        if ok:
            shutil.rmtree(self.test_working_directory)

    def test_SameVector(self):
        service_compare_vector_file = TUV.service_compare_vector_file()
        file1 = os.path.join(self.ref_data, "vector1.shp")
        # we check if it is the same file
        self.assertTrue(
            service_compare_vector_file.testSameShapefiles(file1, file1))

    def test_DifferentVector(self):
        service_compare_vector_file = TUV.service_compare_vector_file()
        file1 = os.path.join(self.ref_data, "vector1.shp")
        file2 = os.path.join(self.ref_data, "vector2.shp")
        # we check if differences are detected
        self.assertFalse(
            service_compare_vector_file.testSameShapefiles(file1, file2))

    def test_ErrorVector(self):
        service_compare_vector_file = TUV.service_compare_vector_file()
        file1 = os.path.join(self.ref_data, "vectorNotHere.shp")
        file2 = os.path.join(self.ref_data, "vector2.shp")
        # we check if an error is detected
        self.assertRaises(Exception,
                          service_compare_vector_file.testSameShapefiles,
                          file1, file2)
