#!/usr/bin/env python3
#-*- coding: utf-8 -*-

# =========================================================================
#   Program:   iota2
#
#   Copyright (c) CESBIO. All rights reserved.
#
#   See LICENSE for details.
#
#   This software is distributed WITHOUT ANY WARRANTY; without even
#   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the above copyright notices for more information.
#
# =========================================================================
import os
import sys
import time
import unittest
import argparse
from unittest.runner import TextTestResult
from iota2.Common.FileUtils import get_iota2_project_dir


class TimeLoggingTestResult(TextTestResult):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.test_timings = []

    def startTest(self, test):
        self._test_started_at = time.time()
        super().startTest(test)

    def addSuccess(self, test):
        elapsed = time.time() - self._test_started_at
        description = self.getDescription(test)

        self.test_timings.append((description, elapsed))
        super().addSuccess(test)

    def getTestTimings(self):
        return self.test_timings


class TimeLoggingTestRunner(unittest.TextTestRunner):
    def __init__(self, *args, **kwargs):
        super().__init__(
            resultclass=TimeLoggingTestResult,
            *args,
            **kwargs,
        )

    def run(self, test):
        result = super().run(test)
        self.stream.writeln("\033[1mTIMING TESTS REPORT\033[0m\n")
        time_sorted_results = sorted(result.getTestTimings(),
                                     key=lambda x: float(x[1]))
        for test_description, elapsed in time_sorted_results:
            self.stream.writeln(
                f"\033[1m\033[94m{elapsed:.2f} \033[0msec Tests \033[1m{test_description}\033[0m\n{'-' * 70}"
            )
        return result


def launch_test_suite(tests_suite: str) -> int:
    """launch iota2 tests, return 0 if all tests pass else 1

    Parameters
    ----------
  
    tests_suite :
        kind of tests to launch : 'unittests', 'integrationtests' or 'all'
    """
    iota2_dir = get_iota2_project_dir()
    i2_dir_env_var = os.getenv("IOTA2DIR")
    if not i2_dir_env_var:
        os.environ["IOTA2DIR"] = iota2_dir

    tests_dir = os.path.join(iota2_dir, "iota2", "Tests")
    if tests_suite.lower() == "unittests":
        tests_dir = os.path.join(tests_dir, "UnitTests")
    elif tests_suite.lower() == "integrationtests":
        tests_dir = os.path.join(tests_dir, "IntegrationTests")
    elif tests_suite.lower() == "all":
        pass
    else:
        raise ValueError(
            f"'tests_suite' must be 'unittest' or 'integrationtest' not '{tests_suite}'"
        )
    loader = unittest.TestLoader()
    # check every *Tests* files
    suite = loader.discover(tests_dir, pattern='*Tests*')
    runner = TimeLoggingTestRunner()
    results = runner.run(suite)
    return sys.exit(0) if results.wasSuccessful() else sys.exit(1)


if __name__ == "__main__":
    PARSER = argparse.ArgumentParser(description="Launch iota2 tests")
    PARSER.add_argument("-tests_suite",
                        default="unittests",
                        dest="tests_suite",
                        const="local",
                        nargs="?",
                        choices=["unittests", "integrationtests", "all"],
                        help="list of available test suite in iota2")
    ARGS = PARSER.parse_args()
    launch_test_suite(ARGS.tests_suite)
